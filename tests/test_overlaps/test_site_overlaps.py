"""Tests for the genes overlapping features
"""
from bio_misc.example_data import examples
from bio_misc.overlaps import site_overlap
from pyaddons import utils
import pytest
import os
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "return_all1,return_all2,no_alleles,results_set_idx",
    (
        (False, False, False, 0),
        (True, True, False, 1),
        (True, False, False, 2),
        (False, True, False, 3),
        (False, False, True, 4),
        (True, True, True, 5),
        (True, False, True, 6),
        (False, True, True, 7),
    )
)
def test_site_overlap(tmpdir, return_all1, return_all2, no_alleles,
                      results_set_idx):
    """Test that site overlaps are returning the correct file over different
    parameter values.
    """
    file1, file2, results = examples.get_data('site_overlaps')
    results_out = os.path.join(
        str(tmpdir), f"results{results_set_idx}.txt"
    )
    kwargs = dict(
        outfile=results_out,
        tmpdir=tmpdir,
        chr_name1='CHOMO',
        chr_name2='chr',
        start_pos1='POS',
        start_pos2='start',
        ref_allele1='A1',
        ref_allele2='ref',
        alt_allele1='A2',
        alt_allele2='alt',
        delimiter1='\t',
        delimiter2=',',
        sort1=True,
        sort2=True,
        return_all1=return_all1,
        return_all2=return_all2,
        no_alleles=no_alleles
    )

    site_overlap.site_overlaps(file1, file2, **kwargs)
    res_md5 = utils.get_file_md5(results_out)
    exp_md5 = utils.get_file_md5(results[results_set_idx])

    assert res_md5 == exp_md5, "files do not match"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_site_no_sort(tmpdir):
    """This tests that the overlaps runs ok when the sort options are turned
    off. In this instance we are only expecting a header to be returned as the
    files need sorting, so this just tests execution.
    """
    file1, file2, exp_intersect_res = examples.get_data('site_overlaps')

    kwargs = dict(
        tmpdir=tmpdir,
        chr_name1='CHOMO',
        chr_name2='chr',
        start_pos1='POS',
        start_pos2='start',
        ref_allele1='A1',
        ref_allele2='ref',
        alt_allele1='A2',
        alt_allele2='alt',
        return_all1=False,
        return_all2=False,
        delimiter1='\t',
        delimiter2=',',
        sort1=False,
        sort2=False,
        no_alleles=True
    )
    rows = [
        row for row in site_overlap.yield_site_overlaps(file1, file2, **kwargs)
    ]
    assert len(rows) == 1, "incorrect number of rows"

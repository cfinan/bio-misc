"""Test that my correlation coefficient calculations match up with the results
obtained from numpy versions.
"""
from bio_misc.ipd import common
from time import time
import pandas as pd
import numpy as np
import numpy.ma as ma


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_test_cc_data(a, b):
    all = np.vstack((a.copy(), b.copy()))
    npcc = np.corrcoef(all)
    t0 = time()
    npcc = np.corrcoef(all)
    t1 = time()
    res = npcc[:a.shape[0], a.shape[0]:a.shape[0]+b.shape[0]]
    return res, f"np.corrcoef() : {(t1 - t0)*1000} milliseconds"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_test_macc_data(a, b):
    all = ma.masked_invalid(np.vstack((a.copy(), b.copy())))
    macc = ma.corrcoef(all)
    t0 = time()
    macc = ma.corrcoef(all)
    t1 = time()
    res = macc[:a.shape[0], a.shape[0]:a.shape[0]+b.shape[0]]
    return res, f"ma.corrcoef() : {(t1 - t0)*1000} milliseconds"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_test_block_data(a, b):
    a = ma.masked_invalid(a.copy())
    b = ma.masked_invalid(b.copy())
    cc, nobs = common.block_corrcoef(
        ma.masked_invalid(a), ma.masked_invalid(b)
    )
    t0 = time()
    cc, nobs = common.block_corrcoef(
        ma.masked_invalid(a), ma.masked_invalid(b)
    )
    t1 = time()
    return cc, nobs, f"block_corrcoef() : {(t1 - t0)*1000} milliseconds"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_complete_ten(ten_by_ten_a, ten_by_ten_b):
    """Test a set of data that has no missing values at all.
    """
    print("\n**** 10x10 - complete ****")
    npcc, time = _get_test_cc_data(ten_by_ten_a, ten_by_ten_b)
    print(time)
    blcc, blnobs, time = _get_test_block_data(ten_by_ten_a, ten_by_ten_b)
    print(time)
    macc, time = _get_test_macc_data(ten_by_ten_a, ten_by_ten_b)
    print(time)
    assert np.allclose(npcc, macc)
    assert np.allclose(npcc, blcc)
    assert np.allclose(macc, blcc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_nan_ten(ten_by_ten_nan_a, ten_by_ten_nan_b):
    """Test a set of data that has no missing values at all.
    """
    print("\n**** 10x10 - NaN ****")
    npcc, time = _get_test_cc_data(ten_by_ten_nan_a, ten_by_ten_nan_b)
    print(time)
    blcc, blnobs, time = _get_test_block_data(
        ten_by_ten_nan_a, ten_by_ten_nan_b
    )
    print(time)
    macc, time = _get_test_macc_data(ten_by_ten_nan_a, ten_by_ten_nan_b)
    print(time)
    assert np.allclose(macc, blcc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_complete_thousand(hundred_by_hundred_a, hundred_by_hundred_b):
    """Test a set of data that has no missing values at all.
    """
    print("\n**** 100x100 - complete ****")
    npcc, time = _get_test_cc_data(hundred_by_hundred_a, hundred_by_hundred_b)
    print(time)
    blcc, blnobs, time = _get_test_block_data(hundred_by_hundred_a,
                                              hundred_by_hundred_b)
    print(time)
    macc, time = _get_test_macc_data(hundred_by_hundred_a,
                                     hundred_by_hundred_b)
    print(time)
    assert np.allclose(npcc, macc)
    assert np.allclose(npcc, blcc)
    assert np.allclose(macc, blcc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_nan_thousand(hundred_by_hundred_nan_a, hundred_by_hundred_nan_b):
    """Test a set of data that has no missing values at all.
    """
    print("\n**** 100x100 - NaN ****")
    npcc, time = _get_test_cc_data(hundred_by_hundred_nan_a,
                                   hundred_by_hundred_nan_b)
    print(time)
    blcc, blnobs, time = _get_test_block_data(
        hundred_by_hundred_nan_a, hundred_by_hundred_nan_b
    )
    print(time)
    macc, time = _get_test_macc_data(hundred_by_hundred_nan_a,
                                     hundred_by_hundred_nan_b)
    print(time)
    assert np.allclose(macc, blcc)

"""Tests for the ``bio_misc.ipd.corr_matrix`` module.
"""
from genomic_config import genomic_config
from bio_misc.ipd import corr_matrix, common
from itertools import combinations
import numpy as np
import pandas as pd
import pytest
import copy
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_cohort(config, pop_info, verbose=False, **kwargs):
    """Get a cohort object for the test cohort data.

    Parameters
    ----------
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the test files based
        on genomic location.
    pop_info : `dict`
        The population information for the test, must have the keys ``name``,
       ``species``, ``species``, ``assembly``, ``file_format``.

    """
    return common.Cohort(
        config, pop_info['name'], pop_info['species'], pop_info['assembly'],
        pop_info['file_format'], leading_zero=False, verbose=False, **kwargs
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _combine_variants(pop_variants):
    """Combine variant lists into a unique set of variants.

    Parameters
    ----------
    pop_variants : `list` of (`list` of ``bio_misc.ipd.common.Variant`)
        The variants to combine.

    Returns
    -------
    combined_variants : `list` of ``bio_misc.ipd.common.Variant`
        The unique combined variants.
    """
    merged = pop_variants[0].copy()
    for i in range(1, len(pop_variants)):
        merged.extend(pop_variants[i])
    prev_uni_id = ""
    variants = []
    for i in sorted(merged, key=lambda x: x.uni_id):
        i.is_unknown = False
        if i.uni_id != prev_uni_id:
            variants.append(i)
        prev_uni_id = i.uni_id
    return variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _eval_multi_pop_corr_matrix(pop_variants, expected_corr_matrix,
                                combined_matrix):
    """Evaluate the multi-population correlation matrix for expected values.

    Parameters
    ----------
    pop_variants : `list` of (`list` of ``bio_misc.ipd.common.Variant`)
        The variants to combine.
    expected_corr_matrix : `list` of `pandas.DataFrame`
        The correlation matrices from single population runs using Plink 1.9
    combined_matrix : `pandas.DataFrame`
        The combined correlation matrix generated from using two cohort
        populations, this is the matrix that is being evaluated (where
        possible) against ``expected_corr_matrix``.

    Raises
    ------
    AssertionError
        If any of the data points do not match to the precision in
        ``expected_corr_matrix``.
    """
    variants = _combine_variants(pop_variants)
    for v1, v2 in combinations(variants, 2):
        x = None
        for m in expected_corr_matrix:
            try:
                x = m.loc[v1.uni_id, v2.uni_id]
                if np.isnan(x):
                    continue
                break
            except KeyError:
                pass

        if x is None:
            # The pairing is not calculated using plink so can't be tested
            continue
        res1 = combined_matrix.loc[v1.uni_id, v2.uni_id]
        res2 = combined_matrix.loc[v2.uni_id, v1.uni_id]

        test_dp = len(str(x))
        if np.isnan(x):
            assert np.isnan(res1)
        else:
            if x < 0:
                res1 = round(res1, test_dp - 3)
            else:
                res1 = round(res1, test_dp - 2)
            assert abs(x) == abs(res1), \
                f"bad data for {v1.uni_id} vs {v2.uni_id}"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _eval_pop_corr_matrix(res, test):
    """Evaluate the correlation matrices for same values.

    Parameters
    ----------
    res  : `pandas.DataFrame`
        The result matrix (with the truth values).
    test  : `pandas.DataFrame`
        The test matrix.

    Raises
    ------
    AssertionError
        If any of the data points do not match to the precision in
        ``expected_corr_matrix``.

    Notes
    -----
    This is used instead of pandas.testing as the plink matrices are rounded
    to different dp and I want to mimic that with the matrix under test
    """
    assert test.shape == res.shape, "shape does not match"
    assert np.all(res.columns == test.columns), "columns do not match"
    assert np.all(res.index == test.index), "rows do not match"

    for i in range(res.shape[0]):
        for j in range(res.shape[1]):
            res_val = res.iloc[i, j]
            test_val = test.iloc[i, j]
            if np.isnan(res_val):
                assert np.isnan(test_val)
            else:
                test_dp = len(str(res_val))
                if res_val < 0:
                    test_val = round(test_val, test_dp - 3)
                else:
                    test_val = round(test_val, test_dp - 2)
                assert abs(test_val) == abs(res_val), \
                    f"bad data for {i} vs {j}"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_pop1_corr_matrix(ipd_config_file, pop1_vcf_data, pop1_corr_matrix,
                          pop1_variants):
    """Test the correlation matrix from the population 1 data against one that
    has been generated by plink 1.9 from the same vcf file. Due to the way the
    plink matrices are written, this only evaluates the test matrix to
    varying dp (depending on the plink result).
    """
    config = genomic_config.open(config_file=ipd_config_file)
    corr, nobs = corr_matrix.get_corr_matrix(
        pop1_variants, [_get_cohort(config, pop1_vcf_data)],
        processes=1, batch_size=1, verbose=False
    )
    plink = np.abs(pop1_corr_matrix)
    result = np.abs(corr)
    _eval_pop_corr_matrix(plink, result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_pop2_corr_matrix(ipd_config_file, pop2_vcf_data, pop2_corr_matrix,
                          pop2_variants):
    """Test the correlation matrix from the population 2 data against one that
    has been generated by plink 1.9 from the same vcf file. Due to the way the
    plink matrices are written, this only evaluates the test matrix to
    varying dp (depending on the plink result).
    """
    config = genomic_config.open(config_file=ipd_config_file)
    corr, nobs = corr_matrix.get_corr_matrix(
        pop2_variants, [_get_cohort(config, pop2_vcf_data)],
        processes=1, batch_size=1, verbose=False
    )
    plink = np.abs(pop2_corr_matrix)
    result = np.abs(corr)
    _eval_pop_corr_matrix(plink, result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_pop2_1_corr_matrix(ipd_config_file, pop1_vcf_data, pop1_corr_matrix,
                            pop1_variants, pop2_vcf_data, pop2_corr_matrix,
                            pop2_variants):
    """Test the correlation matrix built from both from the population 1 and
    population 2 data against ones that have been generated by plink 1.9 from
    the same vcf files. Due to the way the plink matrices are written, this
    only evaluates to the same dp as the plink matrix. Only combinations
    present in each matrix can be tested.
    """
    variants = _combine_variants([pop2_variants, pop1_variants])
    config = genomic_config.open(config_file=ipd_config_file)
    corr, nobs = corr_matrix.get_corr_matrix(
        variants, [
            _get_cohort(config, pop2_vcf_data),
            _get_cohort(config, pop1_vcf_data)
        ],
        processes=1, verbose=False, batch_size=1
    )

    _eval_multi_pop_corr_matrix(
        [pop2_variants, pop1_variants],
        [pop2_corr_matrix, pop1_corr_matrix],
        corr
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("batch_size", tuple(range(2, 41)))
def test_pop1_batch_corr_matrix(batch_size, ipd_config_file, pop1_vcf_data,
                                pop1_corr_matrix, pop1_variants):
    """Test the batch correlation matrix from the population 1 data against one
    that has been generated by plink 1.9 from the same vcf file. Due to the way
    the plink matrices are written, this only evaluates the test matrix to
    varying dp (depending on the plink result).
    """
    config = genomic_config.open(config_file=ipd_config_file)
    corr, nobs = corr_matrix.get_corr_matrix(
        pop1_variants, [_get_cohort(config, pop1_vcf_data)],
        processes=1, batch_size=batch_size, verbose=False
    )
    plink = np.abs(pop1_corr_matrix)
    result = np.abs(corr)
    _eval_pop_corr_matrix(plink, result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# TODO: got here
@pytest.mark.parametrize("batch_size", tuple(range(2, 41)))
def test_pop2_batch_corr_matrix(batch_size, ipd_config_file, pop2_vcf_data,
                                pop2_corr_matrix, pop2_variants):
    """Test the correlation matrix from the population 1 data against one that
    has been generated by plink 1.9 from the same vcf file.
    """
    config = genomic_config.open(config_file=ipd_config_file)
    corr, nobs = corr_matrix.get_corr_matrix(
        pop2_variants, [_get_cohort(config, pop2_vcf_data)],
        verbose=False, batch_size=batch_size
    )
    plink = np.abs(pop2_corr_matrix.round(decimals=8))
    result = np.abs(corr.round(decimals=8))
    pd.testing.assert_frame_equal(plink, result, check_exact=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("batch_size", tuple(range(2, 41)))
def test_pop2_1_batch_corr_matrix(batch_size, ipd_config_file, pop1_vcf_data,
                                  pop1_corr_matrix, pop1_variants,
                                  pop2_vcf_data, pop2_corr_matrix,
                                  pop2_variants):
    """
    """
    variants = _combine_variants([pop2_variants, pop1_variants])
    config = genomic_config.open(config_file=ipd_config_file)

    # This uses the single batch algorithm
    corr_a, nobs_a = corr_matrix.get_corr_matrix(
        variants,
        [_get_cohort(config, pop2_vcf_data),
         _get_cohort(config, pop1_vcf_data)],
        verbose=False, batch_size=1
    )
    # This uses the multi batch algorithm under test
    corr_b, nobs_b = corr_matrix.get_corr_matrix(
        variants,
        [_get_cohort(config, pop2_vcf_data),
         _get_cohort(config, pop1_vcf_data)],
        verbose=False, batch_size=batch_size
    )

    # Make sure that the results from both algorithms match
    pd.testing.assert_frame_equal(corr_a, corr_b, check_exact=False)

    # Now make sure the comparable values from the plink matrices are the same
    # as the batch marix
    _eval_multi_pop_corr_matrix(
        [pop2_variants, pop1_variants],
        [pop2_corr_matrix, pop1_corr_matrix],
        corr_b
    )
    #
    # For the number of observations we just compare both algorithms to make
    # sure they concur
    pd.testing.assert_frame_equal(nobs_a, nobs_b)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_pop1_bgen_corr_matrix(ipd_config_file, pop1_bgen_data, pop1_corr_matrix,
                               pop1_variants):
    """Test the correlation matrix from the population 1 data against one that
    has been generated by plink 1.9 from the same vcf file.
    """
    config = genomic_config.open(config_file=ipd_config_file)
    chrt1 = _get_cohort(config, pop1_bgen_data, allow_complex=True)
    # chrt1['kwargs'] = dict(allow_complex=True)

    corr, nobs = corr_matrix.get_corr_matrix(
        pop1_variants, [chrt1],
        verbose=False
    )
    plink = np.abs(pop1_corr_matrix.round(decimals=8))
    result = np.abs(corr.round(decimals=8))
    pd.testing.assert_frame_equal(plink, result, check_exact=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_pop2_bgen_corr_matrix(ipd_config_file, pop2_bgen_data, pop2_corr_matrix,
                               pop2_variants):
    """Test the correlation matrix from the population 1 data against one that
    has been generated by plink 1.9 from the same vcf file.
    """
    config = genomic_config.open(config_file=ipd_config_file)
    chrt1 = _get_cohort(config, pop2_bgen_data, allow_complex=True)
    # chrt1['kwargs'] = dict(allow_complex=True)

    corr, nobs = corr_matrix.get_corr_matrix(
        pop2_variants, [chrt1],
        verbose=False
    )
    plink = np.abs(pop2_corr_matrix.round(decimals=8))
    result = np.abs(corr.round(decimals=8))
    pd.testing.assert_frame_equal(plink, result, check_exact=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("batch_size", tuple(range(2, 41)))
def test_pop2_1_bgen_batch_corr_matrix(batch_size, ipd_config_file,
                                       pop1_bgen_data, pop1_corr_matrix,
                                       pop1_variants, pop2_bgen_data,
                                       pop2_corr_matrix, pop2_variants):
    """
    """
    variants = _combine_variants([pop2_variants, pop1_variants])
    config = genomic_config.open(config_file=ipd_config_file)

    chrt1 = _get_cohort(config, pop1_bgen_data, allow_complex=True)
    chrt2 = _get_cohort(config, pop2_bgen_data, allow_complex=True)

    # This uses the single batch algorithm
    corr_a, nobs_a = corr_matrix.get_corr_matrix(
        variants,
        [chrt2, chrt1],
        verbose=False, batch_size=1
    )
    # This uses the multi batch algorithm under test
    corr_b, nobs_b = corr_matrix.get_corr_matrix(
        variants,
        [chrt2, chrt1],
        verbose=False, batch_size=batch_size
    )

    # Make sure that the results from both algorithms match
    pd.testing.assert_frame_equal(corr_a, corr_b, check_exact=False)

    # Now make sure the comparable values from the plink matrices are the same
    # as the batch marix
    _eval_multi_pop_corr_matrix(
        [pop2_variants, pop1_variants],
        [pop2_corr_matrix, pop1_corr_matrix],
        corr_b
    )
    #
    # For the number of observations we just compare both algorithms to make
    # sure they concur
    pd.testing.assert_frame_equal(nobs_a, nobs_b)

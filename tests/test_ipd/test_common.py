"""Tests against the ``bio_misc.ipd.common`` module.
"""
from genomic_config import genomic_config
from bio_misc.ipd import readers, common
import numpy as np
import pytest
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("var_idx", tuple(range(0, 58)))
def test_vcf_reader_gt_complex(var_idx, ipd_config_file, pop3_vcf_data,
                               pop3_variants):
    """Test VCF complex genotypes (GT).
    """
    v = pop3_variants[var_idx]
    config = genomic_config.open(config_file=ipd_config_file)
    provider = config.get_cohort_region_provider(
        common.SPECIES,
        pop3_vcf_data['assembly'],
        pop3_vcf_data['name'],
        pop3_vcf_data['file_format']
    )
    with readers.VcfProviderReader(provider, dtype=np.float32) as reader:
        for ac, flipped in reader.get_variant_dosage(v['chr_name'],
                                                     v['start_pos'],
                                                     v['effect_allele'],
                                                     v['other_allele']):
            assert flipped == v['flipped']
            assert np.all(ac == v['counts'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("var_idx", tuple(range(0, 58)))
def test_bgen_reader_gt_complex(var_idx, ipd_config_file, pop3_bgen_data,
                                pop3_variants):
    """Test BGEN complex genotypes probabilities - derived from a GT field.
    """
    v = pop3_variants[var_idx]
    config = genomic_config.open(config_file=ipd_config_file)
    provider = config.get_cohort_region_provider(
        common.SPECIES,
        pop3_bgen_data['assembly'],
        pop3_bgen_data['name'],
        pop3_bgen_data['file_format']
    )
    with readers.BgenProviderReader(provider, allow_complex=True,
                                    dtype=np.float32) as reader:
        for ac, flipped in reader.get_variant_dosage(v['chr_name'],
                                                     v['start_pos'],
                                                     v['effect_allele'],
                                                     v['other_allele']):
            assert flipped == v['flipped']
            assert np.all(ac == v['counts'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize("var_idx", tuple(range(0, 18)))
def test_vcf_reader_gp_complex(var_idx, ipd_config_file, pop4_vcf_data,
                               pop4_variants):
    """Test VCF complex genotype probabilities (GP).
    """
    v = pop4_variants[var_idx]
    config = genomic_config.open(config_file=ipd_config_file)
    provider = config.get_cohort_region_provider(
        common.SPECIES,
        pop4_vcf_data['assembly'],
        pop4_vcf_data['name'],
        pop4_vcf_data['file_format']
    )
    with readers.VcfProviderReader(provider, dtype=np.float64) as reader:
        for ac, flipped in reader.get_variant_dosage(v['chr_name'],
                                                     v['start_pos'],
                                                     v['effect_allele'],
                                                     v['other_allele']):
            assert flipped == v['flipped']
            # print(np.round(ac, 3) == np.round(v['counts'], 3))
            if np.all(v['counts'].mask):
                np.all(ac.mask & v['counts'].mask)
            else:
                # assert np.isclose(np.round(ac, 3) == np.round(v['counts'], 3))
                assert np.allclose(ac, v['counts'], atol=0.001, equal_nan=True)

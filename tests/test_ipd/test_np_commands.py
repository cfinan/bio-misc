"""These are simple tests to make sure numpy commands used in the code work as
expected, they are NOT direct tests on the code base.
"""
import numpy as np
import numpy.ma as ma
import pandas as pd
import itertools as it
import pytest
import random



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_index_subset():
    """Test order when sub-setting on index, just some revision really
    """
    x = np.array([
        [0.0, 0.1, 0.2], [1.0, 1.1, 1.2], [2.0, 2.1, 2.2],
        [3.0, 3.1, 3.2], [4.0, 4.1, 4.2], [5.0, 5.1, 5.2]
    ])
    row_idx = [0, 2, 5]
    col_idx = [0, 2]
    result = np.array([[0.0, 0.2],
                       [2.0, 2.2],
                       [5.0, 5.2]])
    assert np.all(x[np.ix_(row_idx, col_idx)] == result)

    row_idx = [4, 4, 1]
    col_idx = [2, 1]
    result = np.array([[4.2, 4.1],
                       [4.2, 4.1],
                       [1.2, 1.1]])
    assert np.all(x[np.ix_(row_idx, col_idx)] == result)

    result = np.array([[0.0, 0.1],
                       [1.0, 1.1],
                       [2.0, 2.1],
                       [3.0, 3.1],
                       [4.0, 4.1],
                       [5.0, 5.1]])
    assert np.all(x[:, :2] == result)


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def test_1d_mask_create():
#     y = ma.array([1, 2, np.nan], mask=[False, False, True])
#     print(y)
#     y = ma.array(np.array([1, 2, np.nan]), mask=[False, False, True])
#     print(y)
#     x = ma.array(
#         [np.nan, 2, 1, 2, 4, 5, np.nan, 8],
#         mask=[True, False, False, False, False, False, True, False],
#         dtype=np.float16
#     )
#     print(x)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_1d_mask():
    """Test the behaviour of numpy 1d masked arrays in correlation coefficients
    """
    # Test the basic scenario, will give a 2x2 array with the
    # self correlation as the diagonal
    x = np.array([3, 2, 1])
    y = np.array([1, 2, 3])
    cc = np.corrcoef(x, y)
    assert cc[0][1] == -1
    # print(cc)

    # Test with a single nan, should produce an nan result
    x = np.array([3, 2, 1])
    y = np.array([1, 2, np.nan])
    cc = np.corrcoef(x, y)
    assert np.isnan(cc[0][1])

    # The same as above but with the nan column removed we should be back
    # to -1
    x = np.array([3, 2])
    y = np.array([1, 2])
    cc = np.corrcoef(x, y)
    assert np.round(cc[0][1], 2) == -1

    # This does not work as I expected, I thought it would ignore the masked
    # values but does not
    x = np.array([3, 2, 1])
    # Tell masked array that np.nan needs masking
    y = ma.masked_values([1, 2, np.nan], np.nan)
    # Use masked array version of corrcoef
    cc = ma.corrcoef(x, y)
    assert isinstance(cc[0][1], ma.core.MaskedConstant)

    # Create the mask differently and we are back to the expected -1
    x = np.array([3, 2, 1])
    y = ma.array([1, 2, np.nan], mask=[False, False, True])
    cc = ma.corrcoef(x, y)
    assert np.round(cc[0][1], 2) == -1

    # Now try a sequence with nons in both arrays at different
    # positions
    x = np.array([3, 2, 1, 2, 4, 5, 6, 8], dtype=np.float16)
    y = np.array([5, 7, 1, 4, 5, 9, 0, 2], dtype=np.float16)
    cc = np.corrcoef(x, y)
    assert np.round(cc[0][1], 4) == -0.1767

    # Without masking
    x = np.array([np.nan, 2, 1, 2, 4, 5, np.nan, 8], dtype=np.float16)
    y = np.array([5, 7, 1, 4, np.nan, np.nan, 0, 2], dtype=np.float16)
    cc = np.corrcoef(x, y)
    assert np.isnan(cc[0][1])

    # Now a shortened array using elements present in both, this should
    # be the same result as after masking
    x = np.array([2, 1, 2, 8], dtype=np.float16)
    y = np.array([7, 1, 4, 2], dtype=np.float16)
    cc = np.corrcoef(x, y)
    exp_mask_res = cc[0][1]
    assert np.round(exp_mask_res, 4) == -0.2558

    x = ma.array(
        [np.nan, 2, 1, 2, 4, 5, np.nan, 8],
        mask=[True, False, False, False, False, False, True, False],
        dtype=np.float16
    )
    y = ma.array(
        [5, 7, 1, 4, np.nan, np.nan, 0, 2],
        mask=[False, False, False, False, True, True, False, False],
        dtype=np.float16
    )
    cc = ma.corrcoef(x, y)
    assert np.round(cc[0][1], 4) == np.round(exp_mask_res, 4)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_combine_first():
    """Test the behaviour of pandas combine_first function for merging
    data frames.
    """
    expected = pd.DataFrame(
        {'A': [1, 0, 10, 34], 'B': [None, 4, None, 1],
         'C': [3, 6, 3, None], 'D': [None, 2, 1, 4]},
        index=['A', 'B', 'C', 'D'], dtype=float
    )
    df1 = pd.DataFrame(
        {'A': [None, 0, 10, 34], 'B': [None, 4, None, 1],
         'C': [None, 6, None, None], 'D': [None, 2, 1, 4]},
        index=['A', 'B', 'C', 'D'], dtype=float
    )
    df2 = pd.DataFrame(
        {'A': [1, 1], 'C': [3, 3]}, index=['A', 'C'], dtype=float
    )
    result = df1.combine_first(df2)
    pd.testing.assert_frame_equal(expected, result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "result",
    (
        (
            [(3, 5, 4, 2, 1), (4, 3, 5, 2, 1), (4, 5, 2, 3, 1),
             (2, 4, 5, 3, 1), (3, 4, 2, 5, 1), (2, 3, 4, 5, 1),
             (5, 3, 4, 1, 2), (3, 4, 5, 1, 2), (5, 4, 1, 3, 2),
             (4, 1, 5, 3, 2), (4, 3, 1, 5, 2), (3, 1, 4, 5, 2),
             (5, 4, 2, 1, 3), (2, 5, 4, 1, 3), (4, 5, 1, 2, 3),
             (5, 1, 4, 2, 3), (2, 4, 1, 5, 3), (4, 1, 2, 5, 3),
             (3, 5, 2, 1, 4), (2, 3, 5, 1, 4), (5, 3, 1, 2, 4),
             (3, 1, 5, 2, 4), (2, 5, 1, 3, 4), (5, 1, 2, 3, 4)]
        ),
        (
            [(6, 0, 0, 0, 0, 0), (5, 1, 0, 0, 0, 0), (4, 2, 0, 0, 0, 0),
             (3, 3, 0, 0, 0, 0), (4, 1, 1, 0, 0, 0), (3, 2, 1, 0, 0, 0),
             (2, 2, 2, 0, 0, 0), (3, 1, 1, 1, 0, 0), (2, 2, 1, 1, 0, 0),
             (2, 1, 1, 1, 1, 0), (1, 1, 1, 1, 1, 1)]
        ),
        (
            [(1, 2, 3), (1, 2, 4), (1, 3, 4), (2, 3, 4), (1, 2, 5), (1, 3, 5),
             (2, 3, 5), (1, 4, 5), (2, 4, 5), (3, 4, 5), (1, 2, 6), (1, 3, 6),
             (2, 3, 6), (1, 4, 6), (2, 4, 6), (3, 4, 6), (1, 5, 6), (2, 5, 6),
             (3, 5, 6), (4, 5, 6)]
        ),
        (
            [(1, 2), (1, 3), (2, 3), (1, 4), (2, 4), (3, 4), (1, 5), (2, 5),
             (3, 5), (4, 5), (1, 6), (2, 6), (3, 6), (4, 6), (5, 6), (1, 7),
             (2, 7), (3, 7), (4, 7), (5, 7), (6, 7), (1, 8), (2, 8), (3, 8),
             (4, 8), (5, 8), (6, 8), (7, 8)]
        )
    )
)
def test_colex_ordering(result):
    """Test the generation of colex ordered combinations.
    """
    test = result.copy()
    while test == result:
        random.shuffle(test)
    colex = sorted(test, key=lambda x: tuple(reversed(x)))
    assert test != result, "bad test"
    assert colex == result


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_slice_none():
    """Test the effect of using slice(None) is the same as :.
    """
    x = np.array(
        [[[1, 2, 3], [1, 2, 3], [1, 2, 3]], [[1, 2, 3], [1, 2, 3], [1, 2, 3]]]
    )
    y = x[:, :, :]
    assert x.shape == y.shape

    z = x[slice(None), slice(None), slice(None)]
    assert x.shape == z.shape
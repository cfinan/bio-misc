# Getting Started with bio-misc

__version__: `0.2.0a0`

The bio-misc package is essentially a collection of scripts to do various "biological" tasks, the sort of stuff that is useful post GWAS. It is not meant to be an API but I will try to write things to be generalisable and with a high-level API in mind. The scripts are of varying quality.

If it looks like anything is getting too big, it will be rolled into it's own package and I will attach a deprecation warning to them.

There is [online](https://cfinan.gitlab.io/bio-misc/) documentation for bio-misc.

## Installation instructions
At present, bio-misc is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install via conda unless you have access to gwas-norm, in which case you can use a pip install from the git repository.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge bio-misc
```

There are currently builds for Python v3.8, v3.9 for Linux-64 and Mac-osX, currently v3.10 is not supported directly as there is no py310 conda build for bgen-reader.  If one does not get updated in the near future, I will package one up.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/bio-misc.git
cd bio-misc
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9.

## Installing the webdrivers
Some of the scripts use Selanium. This requires a [browser driver](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/) to be installed (webdriver). For Firefox this is referred to as [`geckodriver`](https://github.com/mozilla/geckodriver/releases). The driver's location will need to be in your PATH in your `~/.bashrc` or `~/.bash_profile` and should be matched to your browser version. You will need webdrivers for both Chrome and Firefox.

## Command endpoints
bio_misc contains a lot of command line endpoints that are installed along with the package. Documentation for these can be found [here](https://cfinan.gitlab.io/bio-misc/scripts.html).

## Change log

### version `0.1.4a0`
* SCRIPTS - `bm-drug-target-effects`/ `bm-drug-target-summary` now handle instances where the BNF database is not given.
* SCRIPTS - `bm-drug-target-effects`/ `bm-drug-target-summary` made default database arguments None, to induce proper errors
* SCRIPTS - `bm-drug-target-effects`/ `bm-drug-target-summary` now handle uninitialised max phase for compounds, this only seemed to occur in ChEMBL 33.
* DOCS - fixed error in conda channel in README
* DOCS - added note about lack of py310 version
* DOCS - added note on required webdrivers
* TESTS - updated import tests
* BUILD - removed dead module
* BUILD - removed gwas-norm as is private and causing build to fail
* BUILD - now removed data_dict sources after use

### version `0.1.5a0`
* API - Minor code formatting changes
* API - `bio_misc.ipd.dosage_zarr.get_zarr_array` - Now has an option to return variant objects with missing genotypes.

### version `0.2.0a0`
* SCRIPTS - `bm-search-drug-effects` - Script for search for drugs and targets that have drug effect matches to specific keywords. The drug effects could be indications or side effects.
* API - `bio_misc.drug_lookups.search_effects.search_drug_effects` - Function for searching for drug effects and writing to the output file.

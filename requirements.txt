bgen-reader>=4.0.8
biopython
bump2version
bx-python
crossmap
cyvcf2==0.30.18
nltk
numba==0.56.4
numcodecs
numpy>=1.18, <1.24
pandas
py2neo
pysam
pytest
pytest-dependency
requests
scipy
selenium
sqlalchemy<1.4
tqdm
zarr>=2.12
git+https://gitlab.com/cfinan/bnf-download.git@master#egg=bnf_download
git+https://gitlab.com/cfinan/chembl-orm.git@master#egg=chembl_orm
git+https://gitlab.com/cfinan/ensembl-rest-client.git@master#egg=ensembl_rest_client
git+https://gitlab.com/cfinan/genomic-config.git@master#egg=genomic_config
git+https://gitlab.com/cfinan/merge-sort.git@master#egg=merge_sort
git+https://gitlab.com/cfinan/multi-join.git@master#egg=multi_join
git+https://gitlab.com/cfinan/paper-scraper.git@master#egg=paper_scraper
git+https://gitlab.com/cfinan/pyaddons.git@master#egg=pyaddons
git+https://gitlab.com/cfinan/sqlalchemy-config.git@master#egg=sqlalchemy_config
git+https://gitlab.com/cfinan/stdopen.git@master#egg=stdopen
git+https://gitlab.com/cfinan/umls-tools.git@master#egg=umls_tools

# This will require a username/password
# git+https://gitlab.com/cfinan/gwas-norm.git@master#egg=gwas_norm

# For SQLAlchemy-config as it is not in pypi
sqlalchemy-utils

# For UMLS-tools (used by bnf-download and ChEMBL orm) as it is not in pypi
beautifulsoup4
lxml
pymysql

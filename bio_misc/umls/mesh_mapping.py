"""Map terms to mesh using the UMLS
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import pprint as pp

_SCRIPT_NAME = "bm-mesh-mapping"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_mesh_mapping(infile, umls_conn, outfile=None, verbose=False,
                     term='mesh_term', tmp_dir=None, no_mapping=False,
                     comment=con.DEFAULT_COMMENT,
                     delimiter=con.DEFAULT_DELIMITER,
                     encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of drug summaries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_mesh_mapping(
                    infile, umls_conn, term=term, no_mapping=no_mapping,
                    comment=comment, delimiter=delimiter,
                    encoding=encoding, yield_header=True
                ),
                desc="[progress] fetching mesh mapping: ",
                unit=" mapping(s)",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_mesh_mapping(infile, umls_conn, term='mesh_term',
                       no_mapping=False, comment=con.DEFAULT_COMMENT,
                       delimiter=con.DEFAULT_DELIMITER,
                       encoding=con.DEFAULT_ENCODING,
                       yield_header=True):
    """Yield drug target summary results from an input file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.

    Yields
    ------
    output_row : `list`
        The high level target summary of for the protein ID (including rows
        that have no target mapping).

    Notes
    -----
    The drug targets summary is a high level overview of counts of specific
    attributes on a target.
    """
    umls_session = umls_conn()

    kwargs = dict(
        comment=comment,
        delimiter=delimiter,
        encoding=encoding
    )

    try:
        with utils.BaseParser(infile, **kwargs) as parser:
            term_idx = parser.get_col_idx(term)

            if yield_header is True:
                yield parser.header + parser.get_unique_col(
                    ['mesh_mapping_input_idx', 'cui', 'mesh_term_hcd',
                     'root_term']
                )

            for idx, row in enumerate(parser, 1):
                term_value = row[term_idx]
                has_mapping = False

                cuis = set()
                for i in get_cui(umls_session, term_value):
                    cuis.add(i.CUI)

                for i in cuis:
                    has_mapping = True
                    for hier in get_hier(umls_session, i):
                        root_term = get_root(umls_session, hier.HCD)
                        mapping_row = [idx, i, hier.HCD, root_term.STR]
                        yield row + mapping_row

                if has_mapping is False and no_mapping is True:
                    yield row + [idx] + ([None] * 3)
    finally:
        umls_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_cui(session, term, sab=None):
    """
    Attempt to match a term string in the UMLS

    Parameters
    ----------
    term : :obj:`str`
        A text term to search the UMLS MRCONSO table
    sab : :obj:`str` (optional)
        An optional source abbreviation (SAB) to retrict to
    """
    args = {'TERM': term}
    where = "WHERE M.STR = :TERM"

    if sab is not None:
        args['SAB'] = sab
        where = "{0} AND M.SAB = :SAB".format(where)

    sql = """SELECT M.CUI,SAB,TTY,STR,SUPPRESS
          FROM MRCONSO M
          {0}""".format(where)
    query = session.execute(
        sql, args
    )

    for i in query:
        yield i


def get_hier(session, cui):
    """
    """
    sql = text("""
    select * from MRHIER where CUI = :CUI AND SAB = "MSH"
    """)
    query = session.execute(
        sql, dict(CUI=cui)
    )
    for i in query:
        yield i


def get_root(session, hcd):
    root_hcd = hcd[:3]
    sql = text("""
    select M.CODE, M.STR, M.ISPREF
    from MRHIER H
    join MRCONSO M on H.AUI = M.AUI
    where HCD = :HCD AND H.SAB = "MSH" AND M.SAB = "MSH"
    """)

    query = session.execute(
        sql, dict(HCD=root_hcd)
    )

    return next(query)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """Main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    # Session maker
    engine = create_engine(
        args.uri
    )
    umls_conn = sessionmaker(bind=engine)

    try:
        get_mesh_mapping(
            args.infile, umls_conn, outfile=args.outfile,
            no_mapping=args.no_map, delimiter=args.delimiter,
            verbose=args.verbose, encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, term=args.term
        )
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """
    Initialise the command line arguments. We accept one argument that is
    optional. It is either the path to a database or the name of a section
    header in a configfile. If a section header in a config file then this
    should contain SQLAlchemy connection parameters

    Returns
    -------
    parser : `argpare.ArgumentParser`
        The "unparsed" argument parser
    """
    parser = argparse.ArgumentParser(
        description="Take a set of mesh terms and map them to the mesh data "
        "in the UMLS"
    )
    parser.add_argument(
        'uri', type=str, default='root',
        help="The SQLAlchemy connection URI"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '--term', type=str, default='mesh_term',
        help="The name of the term column."
    )
    parser.add_argument(
        '--no-map', action='store_true',
        help="Include non-mapping rows in the output."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""Output Boolean annotation matrices that can aid the decision of what
gene is driving an association signal (in a broad brush kind of way)
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
# from ensembl_rest_client import client, utils as rest_utils
from tqdm import tqdm
import pandas as pd
import numpy as np
import configparser
import argparse
# import csv
import os
# import sys
# import pprint as pp


LOCI_SECTION_NAME = 'loci'
FILE_KEY_NAME = 'file'
DELIMITER_KEY_NAME = 'delimiter'
COMMENT_KEY_NAME = 'comment'
ENCODING_KEY_NAME = 'encoding'
REFERENCE_KEY_NAME = 'reference'
LOCI_ID_KEY_NAME = 'loci_id'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_annotation_matrices(config, outdir=None, verbose=False, tmp_dir=None,
                            delimiter=con.DEFAULT_DELIMITER,
                            encoding=con.DEFAULT_ENCODING,
                            comment=con.DEFAULT_COMMENT):
    # The sections are the annotation files
    sections = [i for i in config.sections()]
    # pp.pprint(sections)
    default_kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    # Find and remove the loci section from all the sections
    loci_section = sections.pop(sections.index(LOCI_SECTION_NAME))
    input_file, file_kwargs, column_map = \
        parse_section(config, loci_section, **default_kwargs)

    # Get the reference column that will hold the mapping name of the
    # columns in the output grid
    reference_column = column_map.pop(REFERENCE_KEY_NAME)

    # Get the loci ID that indicates what reference column values are
    # associated with a specific locus or region
    loci_id_column = column_map.pop(LOCI_ID_KEY_NAME)

    # Now make sure that the reference ID column is present in the mapping
    if reference_column not in column_map:
        raise KeyError("the reference mapping must be present as a "
                       "column mapping: '{0}'".format(reference_column))

    loci_cache = {}

    with utils.BaseParser(input_file, **file_kwargs) as parser:
        # Locate the loci ID column
        loci_id_idx = parser.get_col_idx(loci_id_column)

        # Now convert the column mapping names into column numbers
        for k, v in column_map.items():
            column_map[k] = parser.get_col_idx(v)

        # Now cache all the loci
        for row in parser:
            try:
                loci_cache[row[loci_id_idx]].append(row)
            except KeyError:
                loci_cache[row[loci_id_idx]] = [row]

    # Now we generate the matrices for all the loci and add the is closest
    # annotation
    ref_matrix_cache, loci_matrix_cache = build_matrix_cache(
        loci_cache, sections, reference_column, column_map
    )

    for i in sections:
        if verbose is True:
            print("[info] processing {0}".format(i))

        search_file(
            config, i, ref_matrix_cache, reference_column,
            verbose=verbose, **default_kwargs
        )

    # We do not have to output to file
    if outdir is not None:
        outdir = os.path.expanduser(outdir)
        for loci, matrix in loci_matrix_cache.items():
            ofn = os.path.join(
                outdir, "{0}.annot_matrix.txt.gz".format(loci)
            )
            matrix.to_csv(ofn, sep="\t", header=True, index=True)
    return loci_matrix_cache, loci_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_section(config, section, **file_kwargs):
    """Parse a section from the config file
    """
    section = dict(config[section])

    try:
        # Make sure we have a valid file argument
        input_file = section.pop(FILE_KEY_NAME)
        open(input_file).close()
    except (KeyError, FileNotFoundError) as e:
        raise FileNotFoundError(
            "problem with file in config section '{0}': '{1}'".format(
                section, str(e)
            )
        ) from e

    # As a default use the file parameters passed to the function
    # If there are any present in the config entry then we overwrite
    for i in [DELIMITER_KEY_NAME, ENCODING_KEY_NAME, COMMENT_KEY_NAME]:
        try:
            file_kwargs[i] = section.pop(i)
        except KeyError:
            pass

    # Convert and string \t \s to real spaces and tabs
    file_kwargs[DELIMITER_KEY_NAME] = \
        get_delimiter(file_kwargs[DELIMITER_KEY_NAME])

    return input_file, file_kwargs, section


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def search_file(config, section_name, ref_matrix_cache, reference_column,
                verbose=False, **file_kwargs):
    """Search one of the external annotation files.

    Parameters
    ----------
    section : `configparser.section`
        A section from the config parser object which behaves like a dictionary
    gene_matrix_cache : `dict` of `list` of `tuple`
        The `dict` is keyed by the gene ID and the `list` values are `tuples`
        that have the  that contain that gene ID at
        ``[0]`` and any variant IDs associated with that loci at ``[1]``
    loci_file_var_id : `str` or `NoneType`, optional, default: `NoneType`
        Any variant ID columns that are present in the input loci file (and by
        extension the ``loci_cache``). If `NoneType` then no loci column is
        used.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Notes
    -----
    This updates the matrices in the ``gene_matrix_cache`` in place
    """
    input_file, file_kwargs, column_map = \
        parse_section(config, section_name, **file_kwargs)

    with utils.BaseParser(input_file, **file_kwargs) as parser:
        # Now convert the column mapping names into column numbers
        for k, v in column_map.items():
            column_map[k] = parser.get_col_idx(v)

        # Get the column number for the reference column (also remove it from
        # the column map)
        reference_idx = column_map.pop(reference_column)

        for row in tqdm(
                parser,
                desc="[progress] processing annotations",
                unit=" rows",
                disable=not verbose
        ):
            ref_id = row[reference_idx]

            try:
                ref_matrix_cache[ref_id]
            except KeyError:
                # The refernce is not present
                continue

            for matrix, map_values in ref_matrix_cache[ref_id]:
                match = True
                for col_name, col_idx in column_map.items():
                    try:
                        if row[col_idx] != map_values[col_name]:
                            match = False
                            break
                    except KeyError:
                        raise KeyError(
                            "unexpected column in data file, all data file"
                            " columns should also be referenced in the loci "
                            "file: '{0}'".format(col_name)
                        )
                if match is True:
                    matrix.loc[section_name, ref_id] = 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_matrix_cache(loci_cache, sections, reference_col, colmap):
    """Build caches of the annotation matrices that will be used to easily
    update their data.

    Parameters
    ----------
    loci_cache : `dict` of `list` of `list`
        The gene loci that we want to annotate from our external data sources.
        The lowest level list is a row in the loci file (i.e. a gene in the
        loci). The mid level list represents the loci and the `dict` is keys on
        the loci ID.
    sections : `list` of `str`
        The section headings in the external data config file
    reference_col : `str`
        The name of the reference column mapping (i.e. must be a column in
        colmap).
    colmap : `dict`
        The keys are column mappings that may be present in other data files
        the values are the actual column names for those columns in the other
        data files.

    Returns
    -------
    ref_matrix_cache : `dict` of `list` of `tuple`
        The `dict` is keyed by the refernce ID and the `list` values are
        `tuples` that have the annotation matrix at [0] and a dict of other
        data values that can be compared to the datasets containing the
        equivalent data values at [1].
    loci_matrix_cache : `dict`
        The keys for the `dict` are the loci ID and the value is the
        `pandas.DataFrame` for the matrix.

    Notes
    -----
    So essentially the same matrices are represented in two different data
    structures.
    """
    colmap = colmap.copy()
    reference_idx = colmap.pop(reference_col)
    ref_matrix_cache = {}
    loci_matrix_cache = {}

    # loci is the loci ID and genes are a list of genes (`list`) in the loci
    for loci_id, loci_content in loci_cache.items():
        # Now get all the reference IDs that will be used as matrix columns
        all_ref_ids = [i[reference_idx] for i in loci_content]

        # Generate the matrix and assign the closest gene
        annot_matrix = get_empty_matrix(sections, all_ref_ids)

        # Update the loci cache
        loci_matrix_cache[loci_id] = annot_matrix

        mapping_values = dict(
            [(k, loci_content[0][v]) for k, v in colmap.items()]
        )

        # Now update the gene matrix cache and use the var_id if we have it
        for i in all_ref_ids:
            matrix_data = (annot_matrix, mapping_values)
            try:
                ref_matrix_cache[i].append(matrix_data)
            except KeyError:
                ref_matrix_cache[i] = [matrix_data]
    return ref_matrix_cache, loci_matrix_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_empty_matrix(rows, columns):
    """
    """
    return pd.DataFrame(
        np.zeros(shape=(len(rows), len(columns)), dtype='int32'),
        columns=columns, index=rows
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def match_gene(section_name, gene_matrix_cache, row, gene_id_idx, var_id_idx):
    gene_id = row[gene_id_idx]
    try:
        for matrix, loci_var_id in gene_matrix_cache[gene_id]:
            matrix.loc[section_name, gene_id] = 1
    except KeyError:
        pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def match_gene_variant(section_name, gene_matrix_cache, row, gene_id_idx,
                       var_id_idx):
    gene_id = row[gene_id_idx]
    var_id = row[var_id_idx]
    # print(gene_id)
    # print(var_id)
    try:
        for matrix, loci_var_id in gene_matrix_cache[gene_id]:
            if loci_var_id == var_id:
                matrix.loc[section_name, gene_id] = 1
    except KeyError:
        pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_delimiter(delimiter):
    """
    """
    if delimiter == r'\t':
        return "\t"
    elif delimiter == r'\s':
        return " "
    return delimiter


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog('bm-annotation-matrix', pkg_name, __version__)
    m.msg_args(args)

    config = configparser.ConfigParser(interpolation=None)
    config.read(args.ini_file)

    get_annotation_matrices(
        config, args.outdir, delimiter=args.delimiter, verbose=args.verbose,
        encoding=args.encoding, comment=args.comment, tmp_dir=args.tmp_dir
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Output Boolean annotation matrices for nearest genes "
        "surrounding an association signal"
    )
    # parser.add_argument(
    #     'loci_file', type=str,
    #     help="The nearest genes file, as output by genes-within-flank. This "
    #     "should have a gene-id column (that should also be in the other "
    #     "annotations), a distance rank column, a loci ID column and optionally"
    #     " a phenotype column. The names of these columns can be specified by"
    #     " arguments to this script."
    # )
    parser.add_argument(
        'ini_file', type=str,
        help="An input .ini file, This should describe the various "
        "annotation files."
    )
    parser.add_argument(
        '-o', '--outdir', type=str, default=os.getcwd(),
        help="An output directory, where the annotation matrices are output"
    )
    arguments.add_file_arguments(parser)
    # arguments.add_id_column_arguments(parser, uniprot_id=False)
    arguments.add_verbose_arguments(parser)
    # parser.add_argument(
    #     '-L', '--loci-id', type=str, default='loci_id',
    #     help="The Ensembl REST endpoint to use. (default: loci_id)"
    # )
    # parser.add_argument(
    #     '-V', '--variant-id', type=str, default=None,
    #     help="The variant identifier. This allows for variant and gene matches"
    #     "to occur, if not provided only variant matches will happen"
    #     "(default: None)"
    # )
    # parser.add_argument(
    #     '-D', '--dist-rank', type=str, default='distance_rank',
    #     help="The number of nearest genes to output (default: distance_rank)."
    # )
    # parser.add_argument(
    #     '-P', '--phenotype', type=str, default=None,
    #     help="The name of the phenotype column (default: None)."
    # )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

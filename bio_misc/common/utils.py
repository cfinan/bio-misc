"""Utility function that are used by several modules
"""
from bio_misc.common import constants as con
from contextlib import contextmanager
import sys
import inspect
import binascii
import shutil
import tempfile
import os
import gzip
import csv
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Msg(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, file=sys.stderr, verbose=True, prefix=None):
        self._prefix = prefix or ''
        self.set_verbose(verbose=verbose, file=file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg_prog(self, prog_name, package, version):
        """
        """
        self.msg(
            " {0} ({1}: v{2}) ===".format(prog_name, package, version),
            prefix="==="
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg_args(self, args, **kwargs):
        """
        """
        # Loop through the objects attributes
        for i in inspect.getmembers(args):
            # Ignores anything starting with underscore
            # (that is, private and protected attributes)
            if not i[0].startswith('_'):
                # Ignores methods
                if not inspect.ismethod(i[1]):
                    attribute = str(i[1])
                    modifier = "value"
                    # For lists, dicts and tuples we report the length
                    if isinstance(i[1], list) is True or\
                       isinstance(i[1], dict) is True or\
                       isinstance(i[1], tuple) is True:
                        attribute = str(len(i[1]))
                        modifier = "length"
                    # Output the message, here we use clean password of lazy
                    # SQLAlchemy to make sure any passwords are stripped
                    # from connection URLS
                    self.msg("%s %s: %s" % (i[0],
                                            modifier,
                                            attribute), **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_verbose(self, verbose=True, file=sys.stderr):
        self._output = file
        self.msg = self._msg
        if verbose is False:
            self.msg = self._null_msg

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _msg(self, msg, prefix=''):
        print(
            "{0}{1}".format((prefix or self._prefix), msg), file=self._output
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _null_msg(self, *args, **kwargs):
        pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseParser(object):
    """Handle basic parsing from flat files.

    Parameters
    ----------
    infile : `str`
        The path to the input file.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input file.
    encoding : `str`, optional, default: `utf-8`
        The encoding of the input file.
    comment : `str`, optional, default: `#`
        The comment characters in the input file.
    none_values : `list` of `str`, optional, default: `['', 'NaN', 'NA']`
        Values in the input file that are considered empty.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, delimiter=con.DEFAULT_DELIMITER,
                 encoding=con.DEFAULT_ENCODING, comment=con.DEFAULT_COMMENT,
                 none_values=['', 'NaN', 'NA']):
        self.infile = infile
        self.delimiter = delimiter
        self.encoding = encoding
        self.comment = comment
        self.none_values = none_values
        self.header = []
        self.header_comments = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager and open the file.
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager and close the file.
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """Initialise the iterator.
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Get the next row in the input file.

        Raises
        ------
        IndexError
            If the header row list of the input file is a different length to
            the current row that has been extracted.
        """
        try:
            row = [i.strip() for i in next(self.reader)]
            row = [i if i not in self.none_values else None for i in row]
        except AttributeError as e:
            raise IOError("have you opened the file?") from e

        if len(row) != len(self.header):
            raise IndexError(
                "row length != header length: {0} vs. {1}".format(
                    len(row), len(self.header)
                )
            )
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the input file. This also initialises a ``csv.reader`` object.
        """
        open_method = open
        if self.infile not in ['-', None] and is_gzip(self.infile) is True:
            open_method = gzip.open
        self.file_obj = input_open(
            self.infile, binary=False, method=open_method,
            encoding=self.encoding
        )
        self.reader = csv.reader(
            self.file_obj, delimiter=self.delimiter
        )

        # Locate the header and skip any comments
        self._read_header()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _read_header(self):
        """Read and extract the header row.
        """
        while True:
            try:
                row = next(self.reader)
            except StopIteration as e:
                raise StopIteration("could not find header") from e
            if row[0].startswith(self.comment):
                self.header_comments.append(row)
            else:
                self.header = [i.strip() for i in row]
                break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the input file
        """
        try:
            self.file_obj.close()
        except AttributeError as e:
            raise IOError("file not open?") from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_col_idx(self, column):
        """Return the column number for a column name.

        Parameters
        ----------
        column : `str`
            The column name to get the column number for.

        Returns
        -------
        column_idx : `int`
            The 0-based column number for a column with ``column`` name.

        Raises
        ------
        ValueError
            If the column is not in the header.
        IOError
            If the file is not open.
        """
        try:
            return self.header.index(column)
        except ValueError as e:
            raise ValueError("column not in header: {0}".format(column)) from e
        except AttributeError as e:
            raise IOError("have you opened the file?") from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_unique_col(self, columns):
        """When given a bunch of columns, ensure they are uniquely named with
        respect to the file header.

        Parameters
        ----------
        columns : `list` of `str`
            The columns to check against the header of the file

        Returns
        -------
        uniq_col_names  : `list` of `str`
            The columns but with guaranteed unique names with respect to the
            file header.

        Notes
        -----
        If and of the columns that have been passed overlap the header of the
        file, then sequential number suffixes are added on until the column
        name is not represented in the header. The suffex numbering starts
        at 1.
        """
        uniq_cols = []
        for i in columns:
            idx = 1
            while True:
                if i not in self.header:
                    uniq_cols.append(i)
                    break
                i = "{0}_{1}".format(i, idx)
                idx += 1
        return uniq_cols


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CoordsParser(BaseParser):
    """Handle basic parsing from flat files giving specific access to genomic
    coordinates in the file.

    Parameters
    ----------
    *args
        Arguments to the ``bio_misc.common.utils.BaseParser`` class.
    chr_name : `str`,  optional, default: `chr_name`
        The name for the chromosome column.
    start_pos : `str`, optional, default: `start_pos`
        The name for the start position column.
    end_pos : `str`, optional, default: `NoneType`
        The name for the end position column. If this is and ``ref_allele``
        are not given, then end position will default to start position.
    ref_allele : `str`, optional, default: `NoneType`
        The name for the ref allele column. If this is given and end_pos is
        ``NoneType``, then the end position is calculated from the start
        position and the length of the reference allele column. Please note
        the reference allele column is not checked for being a DNA string.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                 end_pos=None, ref_allele=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.chr_name = chr_name
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.ref_allele = ref_allele

        # The default function will return the end position (or start as end)
        self.end_func = self._get_end
        # If there is no end position and no ref allele column, then start
        # position column becomes the end position polumn
        if self.end_pos is None and self.ref_allele is None:
            self.end_pos = self.start_pos
        elif self.ref_allele is not None:
            # If the ref allele has been defined then we use it along with
            # start position to get the end
            # We set the end pos to the start pos so it does not fail column
            # checking but we do not use it as such
            self.end_pos = self.start_pos
            self.end_func = self._get_ref_end

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Extract the next row from the input file.
        """
        row = super().__next__()
        for i in [self.start_pos_idx, self.end_pos_idx]:
            try:
                row[i] = int(row[i])
            except (TypeError, ValueError):
                row[i] = None
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the file
        """
        super().open()
        self.start_pos_idx = self.get_col_idx(self.start_pos)
        self.end_pos_idx = self.get_col_idx(self.end_pos)
        self.chr_name_idx = self.get_col_idx(self.chr_name)

        # if it has been defined then set the column indec for the reference
        # allele
        self._ref_allele_idx = None
        if self.ref_allele is not None:
            self._ref_allele_idx = self.get_col_idx(self.ref_allele)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords(self, row):
        """Get a tuple of the coordinates

        Parameters
        ----------
        row : `list`
            A row from the input file to extract the coordinates from.

        Returns
        -------
        chr_name : `str`
            The chromosome name.
        start_pos : `int`
            The start position.
        end_pos : `int`
            The end position.
        """
        return row[self.chr_name_idx], row[self.start_pos_idx], \
            self.end_func(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_end(self, row):
        """Get the end position as set in the end position index attribute
        (either the end position column or the start position column (acting
        as end)).

        Returns
        -------
        end_pos : `int`
            The end position data.
        """
        return row[self.end_pos_idx]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_ref_end(self, row):
        """Get the end position from the start position and the length of the
        reference allele.

        Returns
        -------
        end_pos : `int`
            The end position data.
        """
        return row[self.start_pos_idx] + len(row[self._ref_allele_idx]) - 1


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasParser(BaseParser):
    """Handle basic parsing from a GWAS flat files (no p-value)

    Parameters
    ----------
    *args
        Arguments to the `bio_misc.common.utils.BaseParser`
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional: default: `NoneType`
        The name of the end position column in the header. If `NoneType` it
        defaults to the same as the start position.
    effect_allele : `str`, optional: default: `bio_misc.common.constants.EFFECT_ALLELE`
        The name of the effect allele in the header.
    effect_type : `str`, optional: default: `beta`
        The effect type of the effect size in the file. Values should be
        ``beta``, ``log_or`` or ``or``.
    other_allele : `str`, optional: default: `bio_misc.common.constants.OTHER_ALLELE`
        The name of the other (non effect allele) in the header.
    effect_size : `str`, optional: default: `bio_misc.common.constants.EFFECT_SIZE`
        The name of the effect size column in the header.
    **kwargs
        Keyword arguments to the `bio_misc.common.utils.BaseParser`
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                 end_pos=None, effect_allele=con.EFFECT_ALLELE,
                 effect_type="beta", other_allele=con.OTHER_ALLELE,
                 effect_size=con.EFFECT_SIZE, **kwargs):
        super().__init__(*args, **kwargs)
        self.chr_name = chr_name
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.effect_allele = effect_allele
        self.other_allele = other_allele
        self.effect_type = effect_type
        self.effect_size = effect_size
        if self.end_pos is None:
            self.end_pos = self.start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Get the next row from the file.

        Returns
        -------
        row : `list` of (`str` and `int` and `float`)
            The next row in the file being parsed.

        Notes
        -----
        Empty columns are set to `NoneType` and the start position/end_position
        and set to integers. The effect size is set to a float and the alleles
        are set to uppercase.
        """
        row = super().__next__()
        for i in [self.start_pos_idx, self.end_pos_idx]:
            try:
                row[i] = int(row[i])
            except TypeError:
                row[i] = None

        try:
            row[self.effect_size_idx] = float(row[self.effect_size_idx])
        except TypeError:
            row[self.effect_size_idx] = None

        for i in [self.effect_allele_idx, self.other_allele_idx]:
            try:
                row[i] = row[i].upper()
            except TypeError:
                row[i] = None

        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the file for parsing and ID the "GWAS" columns in the header.

        Raises
        ------
        ValueError
            If any of the expected GWAS column names can't be found in the
            header.
        """
        super().open()
        self.start_pos_idx = self.get_col_idx(self.start_pos)
        self.end_pos_idx = self.get_col_idx(self.end_pos)
        self.chr_name_idx = self.get_col_idx(self.chr_name)
        self.effect_size_idx = self.get_col_idx(self.effect_size)
        self.effect_allele_idx = self.get_col_idx(self.effect_allele)
        self.other_allele_idx = self.get_col_idx(self.other_allele)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_site(self, row):
        """Get the basic GWAS information from a parsed row.

        Parameters
        ----------
        row : `list` of (`str` and `int` and `float`)
            A parsed row as returned by `__next__`.

        Returns
        -------
        chr_name : `str`
            The chromosome name in the row.
        start_pos : `int`
            The start position of the row.
        end_pos : `int`
            The end position of the row. If not in the file this will be the
            same as the start position.
        effect_allele : `str`
            The effect allele value for the row.
        other_allele : `str`
            The other allele value for the row.
        effect_size : `float`
            The effect size value for the row.
        """
        return (
            row[self.chr_name_idx], row[self.start_pos_idx],
            row[self.end_pos_idx], row[self.effect_allele_idx],
            row[self.other_allele_idx], row[self.effect_size_idx]
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def stdopen(filename, mode='rt', method=open, use_tmp=False, tmp_dir=None,
            **kwargs):
    """Provide either an opened file or STDIN/STDOUT if filename is not a file.

    Parameters
    ----------
    filename : `str` or `NoneType`
        The filename to open. If ``-``, ``''`` or ``NoneType`` then either
        `sys.stdin` or `sys.stdout` is yielded (depending on ``mode``).
        Otherwise the file is opened with ``method``.
    mode : `str`
        Should be the usual ``w/wt/wb/r/rt/rb``. A ``''`` is interpreted as
        read as is ``NoneType``.
    method : `func`, optional, default: `open`
        The open method to use if iflename is not using ``STDIN`` or
        ``STDOUT``.
    **kwargs
        Any other kwargs passed to ``method``.

    Yields
    ------
    fobj : `File` or `sys.stdin` or `sys.stdout`
        A place to read or write depending on ``filename`` and ``mode``

    Raises
    ------
    ValueError
        If the more can't be identified.
    """
    # Reading
    if mode == '' or 'r' in mode or mode is None:
        binary = False
        if 'b' in mode:
            binary = True
        fobj = input_open(filename, binary=binary, method=method, **kwargs)
        yield fobj
        fobj.close()
    elif 'w' in mode:
        # Writing to a file
        if filename not in [sys.stdout, '-', '', None]:
            if use_tmp is True:
                tmpout = get_tmp_file(dir=tmp_dir)

                try:
                    fobj = method(tmpout, mode, **kwargs)
                    yield fobj
                except Exception:
                    fobj.close()
                    os.unlink(tmpout)
                    raise
                fobj.close()
                shutil.move(tmpout, filename)
            else:
                fobj = method(filename, mode, **kwargs)
                yield fobj
                fobj.close()
        else:
            # Writing to STDOUT
            if 'b' in mode:
                yield sys.stdout.buffer
            else:
                try:
                    sys.stdout.reconfigure(encoding=kwargs['encoding'])
                except KeyError:
                    pass
                yield sys.stdout
    else:
        raise ValueError("unknown mode: {0}".format(mode))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def input_open(filename, binary=False, method=open, **kwargs):
    """
    Provide either an opened file or STDIN/STDOUT if filename is not a file.

    Parameters
    ----------
    filename : str or :obj:`sys.stdin` or NoneType
        The filename to open. If `sys.stdin`, '-', '' or `NoneType` then
        `sys.stdin` is yielded otherwise the file is opened with `method`
    binary : `bool`, optional, default: `False`
        Should the file or STDIN be used in binary mode.
    method : `function`, optional, default: `open`
        The open method to use (uses the standard `open` as a default) but the
        user can supplied things like `gzip.open` here if required.
    **kwargs
        Any other kwargs passed to ``method``.

    Yields
    ------
    fobj : :obj:`File` or `sys.stdin`
        A place to read the file from
    """
    # Want to read in from a file and not STDIN
    if filename not in [sys.stdin, '-', '', None]:
        mode = 'rt'
        if binary is True:
            mode = 'rb'

        fobj = method(filename, mode, **kwargs)
        return fobj
    else:
        stdin_obj = sys.stdin
        if binary is True:
            stdin_obj = sys.stdin.buffer

        try:
            # If the encoding has been supplied then modify STDIN to accept the
            # new encoding
            stdin_obj.reconfigure(encoding=kwargs['encoding'])
        except KeyError:
            pass
    return stdin_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tmp_file(**kwargs):
    """
    Initialise a temp file to work with. This differs from `tempfile.mkstemp`
    as the temp file is closed and only the file name is returned.

    Parameters
    ----------
    **kwargs
        Any arguments usually passed to `tempfile.mkstemp`

    Returns
    -------
    temp_file_name : `str`
        The name of a temp file that has been created with the requested
        parameters.
    """
    tmp_file_obj, tmp_file_name = tempfile.mkstemp(**kwargs)
    os.close(tmp_file_obj)
    return tmp_file_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_gzip(file_name):
    """
    Determine if a file is gzipped by looking at the first few bytes in the
    file

    Parameters
    ----------
    file_name : :obj:`str`
        A route to the file that needs checking

    Returns
    -------
    is_gzipped : bool
        `True` if it is gzipped `False` if not
    """
    with open(file_name, 'rb') as test:
        # We read the first two bytes and we will look at their values
        # to see if they are the gzip characters
        testchr = test.read(2)
        is_gzipped = False

        # Test for the gzipped characters
        if binascii.b2a_hex(testchr).decode() == "1f8b":
            # If it is gziped, then close it and reopen as a gzipped file
            is_gzipped = True

    return is_gzipped


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_unique_entries(file_name, columns, parser=BaseParser, **kwargs):
    """Parse through a file and get the unique entries from the file

    Parameters
    ----------
    file_name : `str`
        The path to the file you want to parse.
    columns : `list` of `str` or `str`
        The columns that you want to extract the unique data for.
    parser : `BaseParser`, optional, default: `BaseParser`
        A parser object to use. Must inherit from `BaseParser`
    **kwargs
        Keyword arguments to the parser.

    Returns
    -------
    unique : `list` of `tuple` or `list`
        If multiple columns are given then the list will contain tuples of data
        values. The length of the tuple will equal the length of the provided
        column list. If columns is a string then the list will contain atomic
        values.

    Notes
    -----
    if columns is a string then all `NoneType` values will be filtered from
    the returned list. This does not happen if columns is a list.
    """
    unique = []
    with BaseParser(file_name, **kwargs) as parser:

        if isinstance(columns, (list, tuple)):
            col_idx = [parser.get_col_idx(i) for i in columns]
            for row in parser:
                col_data = tuple([row[i] for i in col_idx])

                if col_data not in unique:
                    unique.append(col_data)
        elif isinstance(columns, str):
            col_idx = parser.get_col_idx(columns)
            for row in parser:
                col_data = row[col_idx]

                if col_data is not None and col_data not in unique:
                    unique.append(col_data)

    return unique

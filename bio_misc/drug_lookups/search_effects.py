"""Search for drugs and targets that match defined drug effect search terms.
Searches can be performed over ChEMBL and/or the BNF, for ChEMBL, drug
indications are searched. For the BNF, drug indications, side effects,
contra-indications and cautions are searched.

If searching ChEMBL, you must have created some search index targets. Please
see the `chembl-orm <https://gitlab.com/cfinan/chembl-orm>`_ package for
details.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from pyaddons import utils, log
from bnf_download import queries as bq
from sqlalchemy_config import config as cfg
from chembl_orm import queries as cq
from tqdm import tqdm
import stdopen
import argparse
import csv
import os
import sys
# import pprint as pp


_PROG_NAME = 'bm-search-drug-effects'
"""The name of the script that is implemented in this module (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""

# deal with long fields in csv
csv.field_size_limit(sys.maxsize)

DEFAULT_PREFIX = 'db.'
"""The default prefix for SQLAlchemy parameters that are defined in the config
file (`str`)
"""
DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
"""The default config file location for SQLAlchemy parameters (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point. For API access see
    ``bio_misc.drug_lookups.search_effects.search_drug_effects``
    """
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        args.search_terms.extend(
            [row.strip() for row in open(args.search_file, 'rt')]
        )
    except FileNotFoundError:
        pass

    chembl_sm = None
    if args.chembl is not None:
        # Get a sessionmaker to create sessions to interact with the database
        chembl_sm = cfg.get_new_sessionmaker(
            args.chembl,
            conn_prefix=DEFAULT_PREFIX,
            config_arg=args.config,
            config_default=DEFAULT_CONFIG,
            exists=True
        )

    bnf_sm = None
    if args.bnf is not None:
        # Get a sessionmaker to create sessions to interact with the database
        bnf_sm = cfg.get_new_sessionmaker(
            args.bnf,
            conn_prefix=DEFAULT_PREFIX,
            config_arg=args.config,
            config_default=DEFAULT_CONFIG,
            exists=True
        )

    prog_verbose = log.progress_verbose(verbose=args.verbose)

    try:
        logger.info("searching for drug effects...")
        search_drug_effects(
            args.search_terms, chembl_conn=chembl_sm, bnf_conn=bnf_sm,
            outfile=args.outfile, verbose=prog_verbose,
            tmpdir=args.tmp, no_match=True, targets=not args.no_targets
        )
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'search_terms', type=str, nargs="*",
        help="Any terms that you want to search in the drug effects. If you"
        " have lots of terms, you can use the --search-file option. Note"
        " that terms are aggregated over this argument and the search file"
        " argument."
    )
    parser.add_argument(
        '--search-file', type=str,
        help="A file containing one or more search terms to search against"
        " the drug effects. There should be one term per line."
    )
    parser.add_argument(
        '-C', '--chembl', type=str,
        help="The connection for the ChEMBL database. This should be an "
        "SQLAlchemy connection URL, filename if using SQLite or config "
        "file section if you do not want to put full connection parameters on"
        " the cmd-line. Use the config file (--config) to supply the location"
        " of any config files"
    )
    parser.add_argument(
        '-B', '--bnf', type=str,
        help="The connection for the BNF database. This should be an "
        "SQLAlchemy connection URL, filename if using SQLite or config "
        "file section if you do not want to put full connection parameters on"
        " the cmd-line. Use the config file (--config) to supply the location"
        " of any config files"
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="The location of the output file, if not supplied then output"
        " is to STDOUT"
    )
    parser.add_argument(
        '-c', '--config', type=str,
        default="~/{0}".format(os.path.basename(DEFAULT_CONFIG)),
        help="The location of the config file"
    )
    parser.add_argument(
        '-T', '--tmp', type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose', default=0, action="count",
        help="give more output, ``--vv`` turns on progress monitoring"
    )
    parser.add_argument(
        '-N', '--no-targets', action="store_true",
        help="Do not output any drug-target information, this will limit the"
        " amount of pseudo-replication"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()
    args.config = utils.get_full_path(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def search_drug_effects(search_terms, chembl_conn=None, bnf_conn=None,
                        outfile=None, verbose=False, tmpdir=None,
                        no_match=False, encoding='UTF8', delimiter="\t",
                        targets=True):
    """The main high level API function that takes an input file and generates
    and output file of drug summaries.

    Parameters
    ----------
    search_terms : `list` of `str`
        The terms to search against the BNF and/or ChEMBL.
    chembl_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    tmpdir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    no_match : `bool`, optional, default: `False`
        Report all the entries that have no matching drug effect.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    targets : `bool`, optional, default: `True`
        Include target information in addition to drug information.

    See also
    --------
    :ref:`documentation page <search_effects>`
    """
    if chembl_conn is None and bnf_conn is None:
        raise ValueError(
            "You must define either a ChEMBL connection or a BNF connection"
            " (or both)"
        )

    if len(search_terms) == 0:
        raise ValueError("There are no search terms")

    tqdm_kwargs = dict(
        desc="[info] searching for drug effect terms...",
        unit="term(s)",
        disable=not verbose
    )

    try:
        # If a BNF connection is supplied then set up up for queries
        bsession = None
        bsearch = None
        bsearch_func = dummy_search
        if bnf_conn is not None:
            bsession = bnf_conn()
            bsearch = bq.BnfQuery(bsession)
            bsearch_func = bnf_search

        # If a ChEMBL connection is supplied then set up up for queries
        csearch = None
        csearch_func = dummy_search
        if chembl_conn is not None:
            csearch = cq.ChemblQuery(chembl_conn)
            csearch.open()
            csearch_func = chembl_search

        with stdopen.open(outfile, mode='wt', use_tmp=True, tmpdir=tmpdir,
                          encoding=encoding) as out:
            writer = csv.writer(out, delimiter=delimiter,
                                lineterminator=os.linesep)
            header = [
                'input_idx',
                'data_source',
                'input_term',
                'search_weight',
                'match_weight',
                'match_score',
                'match_term',
                'drug_effect_id',
                'drug_effect_type',
                'drug_effect_freq',
                'drug_effect_name',
                'drug_name',
                'max_phase_for_indication',
                'drug_url'
            ]

            if targets is True:
                header += [
                    'drug_component_name',
                    'full_mapping',
                    # 'mapping_synonym',
                    'compound_pref_name',
                    'compound_chembl_id',
                    'molecule_type',
                    # 'target_mapping_relationship',
                    'mechanism_of_action',
                    'target_chembl_id',
                    'target_type',
                    'target_pref_name',
                    'organism',
                    'action_type',
                    'target_component_database',
                    'target_accession',
                    'target_description'
                ]
            writer.writerow(header)
            idx = 1
            for row in tqdm(search_terms, **tqdm_kwargs):
                # Search for the BNF drug effects
                for outrow in bsearch_func(bsearch, row, targets=targets):
                    writer.writerow([idx] + outrow)

                # Search for the ChEMBL indication
                for outrow in csearch_func(csearch, row, targets=targets):
                    writer.writerow([idx] + outrow)
                idx += 1
    finally:
        for i in [bsession, csearch]:
            try:
                bsession.close()
            except AttributeError:
                pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dummy_search(*args, **kwargs):
    """The dummy search does not actually do anything. It is used to issue
    queries for any of the BNF/ChEMBL resources that have not been supplied.

    Parameters
    ----------
    *args
        Any positional arguments (ignored).
    **kwargs
        Any keyword arguments (ignored).

    Returns
    -------
    no_hits : `list`
        A list of length 0.
    """
    return []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_match_words(text, m):
    """Extract the matching word positions from the query text.

    Parameters
    ----------
    text : `str`
        The query text.
    m  : `list`
        The matching search result.

    Returns
    -------
    matching_words : `list` of `str`
        The words from the search result.
    """
    return [
        text[pos[0]:pos[1]]
        for pos, rank, token in sorted(m[3], key=lambda x: x[0])
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_drug_effect_mapping(m):
    """Extract the term mapping record from a query result.

    Parameters
    ----------
    m  : `list`
        The matching search result.

    Returns
    -------
    drug_effect : `bnf_download.orm.TermIndexMap` or \
    `chembl_orm.orm.TermIndexMap`
        The term mapping result.

    Raises
    ------
    """
    return m[4][0][1]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_drug_effect(drug_effect):
    """Test that a single drug effect (indication in ChEMBL) is returned.

    Parameters
    ----------
    drug_effect  : `list`
        The drug affects mapped to the index, although this is a list is should
        have length 1.

    Returns
    -------
    drug_effect : `bnf_download.orm.DrugEffect` or \
    `chembl_orm.orm.DrugIndication`
        The database drug effect result.

    Raises
    ------
    ValueError
        If there is >1 drug effect result.
    """
    if len(drug_effect) > 1:
        raise ValueError(
            f"bad drug effect: {len(drug_effect)}"
        )
    return drug_effect[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_empty_row(text, targets):
    """Return an empty row of the correct length to be output. The length of
    this will change depending on the targets argument.

    Parameters
    ----------
    text : `str`
        The text that was used to search and come up with no results.
    targets : `bool`
        Has the user requested target information, if so return a longer line.

    Returns
    -------
    empty_row : `list`
        The search text is at element 0 and the remainder of the elements are
        populated with `NoneType`.
    """
    blank = [text] + ([None] * 11)
    if targets is False:
        return blank
    else:
        return blank + ([None] * 16)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def chembl_search(search, text, targets=True, no_match=True):
    """Search for ChEMBL drug indication matches to the search text.

    Parameters
    ----------
    search : `chembl_orm.queries.ChemblQuery`
        A ChEMBL query object.
    text : `str`
        The search text.
    targets : `bool`, optional, default: `True`
        Include target information in the search results.
    no_match : `bool`, optional, default: `True`
        Include empty rows for text searches that had no results.

    Yields
    ------
    result_row : `list`
        A result row. If targets is False, then this will be the indication,
        compound information that matches the search text. If targets is True,
        then some pseudo replication will occur as for each matching
        indication/compound, all target information will also be yielded.
    """
    found = False

    for m in search.get_drugs_for_indication(text):
        found = True
        match_words = _get_match_words(text, m)
        drug_effect_map = _get_drug_effect_mapping(m)
        drug_effect = _test_drug_effect(drug_effect_map.indication)
        drug = drug_effect.molecule_dictionary

        base_row = [
            'chembl', text, m[5], m[6], m[0], '|'.join(match_words),
            drug_effect_map.term_id,
            'indication',
            'all',
            drug_effect.mesh_heading,
            drug.pref_name,
            drug_effect.max_phase_for_ind,
            None
        ]

        if targets is False:
            yield base_row
        else:
            for dm in drug.drug_mechanism:
                try:
                    td = dm.target_dictionary
                    for tc in td.target_components:
                        cs = tc.component_sequences
                        yield base_row + [
                            None,
                            1,
                            # None,
                            drug.pref_name,
                            drug.chembl_id,
                            drug.molecule_type,
                            # 'exact',
                            dm.mechanism_of_action,
                            td.chembl_id,
                            td.target_type,
                            td.pref_name,
                            td.organism,
                            dm.action_type,
                            cs.db_source,
                            cs.description,
                            None
                        ]
                except AttributeError:
                    # For some reason we have drug mechanisms with no targets
                    if td is not None:
                        raise

    if found is False and no_match is True:
        yield _get_empty_row(text, targets)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def bnf_search(search, text, targets=True, no_match=True):
    """Search for BNF drug effect matches to the search text.

    Parameters
    ----------
    search : `bnf_download.queries.BnfQuery`
        A BNF query object.
    text : `str`
        The search text.
    targets : `bool`, optional, default: `True`
        Include target information in the search results.
    no_match : `bool`, optional, default: `True`
        Include empty rows for text searches that had no results.

    Yields
    ------
    result_row : `list`
        A result row. If targets is False, then this will be the indication,
        compound information that matches the search text. If targets is True,
        then some pseudo replication will occur as for each matching
        indication/compound, all target information will also be yielded.
    """
    found = False
    for m in search.get_drugs_with_effect(text):
        found = True
        match_words = _get_match_words(text, m)
        drug_effect_map = _get_drug_effect_mapping(m)
        drug_effect = _test_drug_effect(drug_effect_map.drug_effect)
        drug = drug_effect.drug

        base_row = [
            'bnf', text, m[5], m[6], m[0], '|'.join(match_words),
            drug_effect_map.term_id,
            drug_effect.drug_effect_type,
            drug_effect.drug_effect_freq,
            drug_effect.drug_effect_name,
            drug.drug_name,
            4,
            drug.drug_url
        ]
        if targets is False:
            yield base_row
        else:
            for c in drug.drug_component:
                for cm in c.chembl_mapping:
                    for ctc in cm.chembl_target_components:
                        yield base_row + [
                            c.drug_component_name,
                            int(cm.full_mapping),
                            # cm.mapping_synonym,
                            cm.compound_pref_name,
                            cm.compound_chembl_id,
                            cm.molecule_type,
                            # ctc.target_mapping_relationship,
                            ctc.mechanism_of_action,
                            ctc.target_chembl_id,
                            ctc.target_type,
                            ctc.target_pref_name,
                            ctc.organism,
                            ctc.action_type,
                            ctc.target_component_database,
                            ctc.target_accession,
                            ctc.target_description
                        ]

    if found is False and no_match is True:
        yield _get_empty_row(text, targets)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""General queries to fetch information from ChEMBL.

Notes
-----
Although these use SQLAlchemy they do not use it properly and the queries are
all based around SQLite. I will adapt this to reflect the ChEMBL schema and
query the database properly.
"""
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text
import pprint as pp
import os

COMPONENTS = 'components'
INDICATIONS = 'chembl_indications'
ASSAYS = 'assays'
COMPOUNDS = 'compounds'
ATC = 'atc'

# ALLOSTERIC ANTAGONIST
# BINDING AGENT
# BLOCKER
# CHELATING AGENT
# CROSS-LINKING AGENT
# DISRUPTING AGENT
# HYDROLYTIC ENZYME
# INVERSE AGONIST
# MODULATOR
# NEGATIVE ALLOSTERIC MODULATOR
# NEGATIVE MODULATOR
# OPENER
# OTHER
# OXIDATIVE ENZYME
# POSITIVE ALLOSTERIC MODULATOR
# POSITIVE MODULATOR
# PROTEOLYTIC ENZYME
# REDUCING AGENT
# SEQUESTERING AGENT
# STABILISER
# SUBSTRATE

INHIBIT_MECH = [
    'ANTAGONIST',
    'ANTISENCE INHIBITOR',
    'DEGRADER',
    'INHIBITOR',
    'RNAI INHIBITOR'
]
"""Inhibition mechanisms (`list` of `str`)
"""

ACTIVE_MECH = [
  'ACTIVATOR',
  'AGONIST',
  'PARTIAL AGONIST'
]
"""Activation mechanisms (`list` of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def chembl_sqlite_connect(db):
    """Connect to a SQLite database using SQLAlchemy.

    Parameters
    ----------
    db : `str`
        A path to an SQLite database

    Returns
    -------
    session_maker : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions:
        ``session = session_maker()``
    """
    engine = create_engine(
        'sqlite:////{0}'.format(
            os.path.realpath(os.path.expanduser(db))
        )
    )
    return sessionmaker(bind=engine)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_protein_info(session, prot_id):
    """Get a target summary for a given uniprot ID

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The sesion object to connect to ChEMBL
    prot_id : `str`
        The protein ID to map to ChEMBL target identifiers. In reality this
        does not have to be a protein ID it could be anything that is a target
        component in ChEMBL. There are no restrictions on the query
    """
    targets = get_chembl_target_for_component(session, prot_id)
    summary = []
    for i in targets:
        target = dict(i)
        target[COMPONENTS] = []
        for component in get_target_components(session, i.target_chembl_id):
            target[COMPONENTS].append(dict(component))

        target[INDICATIONS] = \
            [
                dict(ind) for ind in get_target_indications(
                    session, i.target_chembl_id
                )
            ]

        assays = get_assays_for_target(session, i.target_chembl_id)

        target[ASSAYS] = assays
        compounds = set([a.compound_chembl_id for a in assays])

        target[COMPOUNDS] = []
        for c in compounds:
            compound_summary = dict(get_compound_summary(session, c))
            compound_summary[ASSAYS] = []
            compound_summary[ATC] = \
                [dict(atc) for atc in get_compound_atc(session, c)]
            target[COMPOUNDS].append(compound_summary)
        summary.append(target)
    return summary


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_targets(chembl_conn, organism="Homo sapiens"):
    """Get all the targets that have a defined mechanism in ChEMBL.

    Parameters
    ----------
    chembl_conn : `sqlalchemy.Sessionmaker`
        An SQL alchemy session maker to create a session.
    organism : `str`, optional, default: `Homo sapiens`
        The organism that should contain the target.

    Yields
    ------
    row : `tuple`
        An SQLAlchemy namedtuple where the names are the column names.
    """
    session = chembl_conn()

    sql = text(
        """
        SELECT td.chembl_id as target_chembl_id,
               td.pref_name as target_pref_name,
               td.target_type,
               td.organism
        FROM drug_mechanism dm
        JOIN target_dictionary td on dm.tid = td.tid
        WHERE td.organism = :ORGANISM
        GROUP BY target_chembl_id
        """
    )

    query = session.execute(
        sql, dict(ORGANISM=organism)
    )

    for i in query:
        yield i


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_chembl_target_for_component(session, accession):
    """Get all the chembl target IDs for a component accession.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database.
    accession : `str`
        A target component accession, typically a uniprot identifier.

    Returns
    -------
    chembl_ids :`list` of `tuple`
        The chembl identifiers mapping to the uniprot ID. Each tuple has the
        structure of [0] chembl_id [1] species.

    Notes
    -----
    Currently the queries are text and optimised for SQLite. The component
    accession is typically a uniprot ID.
    """
    sql = text(
        """
        select td.chembl_id as target_chembl_id,
               td.target_type,
               td.pref_name as target_pref_name,
               td.organism as target_organism,
               td.tax_id as target_taxonomic_id,
               td.species_group_flag as target_species_group_flag
        from component_sequences cs
        join target_components tc
        on tc.component_id = cs.component_id
        join target_dictionary td on tc.tid = td.tid
        where accession = :ACCESSION
        """
    )
    query = session.execute(sql, {'ACCESSION': accession})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_components(session, chembl_id):
    """Get all the chembl target IDs for a component accession.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database.
    accession : `str`
        A target component accession, typically a uniprot identifier.

    Returns
    -------
    chembl_ids :`list` of `tuple`
        The chembl identifiers mapping to the uniprot ID. Each tuple has the
        structure of [0] chembl_id [1] species.

    Notes
    -----
    Currently the queries are text and optimised for SQLite. The component
    accession is typically a uniprot ID.
    """
    sql = text(
        """
        select td.chembl_id as target_chembl_id,
               cs.accession as component_accession,
               tc.homologue as component_is_homologue,
               cs.component_type,
               cs.sequence as component_sequence,
               cs.description as component_desc,
               cs.organism as component_organism,
               cs.tax_id as component_taxonomic_id,
               cs.db_source as comonent_source,
               cs.db_version as component_source_version
        from target_components tc
        join component_sequences cs
        on tc.component_id = cs.component_id
        join target_dictionary td
        on tc.tid = td.tid
        where td.chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_indications(session, chembl_id):
    """
    """
    sql = text(
        """
        select td.chembl_id as target_chembl_id,
        td.pref_name as target_pref_name,
        md.chembl_id as compound_chembl_id,
        md.pref_name as compound_pref_name,
        md.molecule_type,
        dm.mechanism_of_action,
        dm.action_type,
        dm.direct_interaction,
        dm.molecular_mechanism,
        dm.disease_efficacy,
        dm.mechanism_comment,
        dm.selectivity_comment,
        dm.binding_site_comment,
        di.max_phase_for_ind,
        di.mesh_id,
        di.mesh_heading,
        di.efo_id,
        di.efo_term
        from target_dictionary td
        join drug_mechanism dm on td.tid = dm.tid
        join molecule_dictionary md on dm.molregno = md.molregno
        left join drug_indication di on dm.molregno = di.molregno
        where td.chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_assays_for_target(session, chembl_id):
    """Get the activity data for a chembl target.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database
    chembl_id : `str`
        A chembl identifier for the target.

    Returns
    -------
    activity_data
    """
    sql = text(
        """
        select  td.chembl_id as target_chembl_id
                , md.chembl_id as compound_chembl_id
                , s.chembl_id as assay_chembl_id
                , s.description as assay_description
                , ast.*
                , s.assay_test_type
                , s.assay_category
                , s.assay_organism
                , s.assay_strain
                , s.assay_tissue
                , s.assay_cell_type
                , rt.*
                , s.confidence_score
                , s.curated_by
                , a.standard_type
                , a.standard_relation
                , a.standard_value
                , a.standard_units
                , a.standard_flag
                , a.pchembl_value
                , a.activity_comment
                , a.standard_text_value
        from molecule_dictionary md
        join activities a
        on a.molregno = md.molregno
        join assays s
        on s.assay_id = a.assay_id
        join relationship_type rt
        on rt.relationship_type = s.relationship_type
        join assay_type ast
        on ast.assay_type = s.assay_type
        join target_dictionary td
        on td.tid = s.tid
        where td.chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_compound_summary(session, chembl_id):
    """
    """
    sql = text(
        """
        select  md.chembl_id
                , md.pref_name
                , md.indication_class
                , md.max_phase
                , md.therapeutic_flag
                , md.molecule_type
                , md.first_approval
                , md.black_box_warning
                , md.withdrawn_flag
                , md.oral
                , md.parenteral
                , md.topical
                , cp.num_lipinski_ro5_violations
                , cp.ro3_pass
                , md.usan_year
        from molecule_dictionary md
        left join compound_properties cp
        on md.molregno = cp.molregno
        where chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    res = query.fetchall()
    if len(res) > 1:
        pp.pprint(res)
        raise IndexError("too many results")
    return res[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_compound_atc(session, chembl_id):
    """
    """
    sql = text(
        """
        select  md.chembl_id
        , ac.who_name
        , mac.level5
        , ac.level1_description
        , ac.level2_description
        , ac.level3_description
        , ac.level4_description
        from molecule_dictionary md
        join molecule_atc_classification mac
        on md.molregno = mac.molregno
        join atc_classification ac
        on mac.level5 = ac.level5
        where chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()

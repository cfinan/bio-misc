"""Output a high level summary for ChEMBL targets that contain the search
protein. Search proteins are provided in an input file that can be any
arbitrary flat-file format.

It aims to document the numbers of unique compounds and indications stratified
by various criteria. This can be helpful as a first pass filtering step to find
potentially interesting targets. The majority of the data is queried from
ChEMBL. So all the compound data associated with a target is taken from the
``assays``, ``activities`` and ``molecule_dictionary`` tables. If a target has
been considered therapeutically it will also have counts from the
``drug_mechanism`` table, targets in here may be from any clinical or
pre-clinical phase. It also uses ChEMBL to produce counts of compound
occurrences by level 1 ATC code. The indication counts are from the
``drug_mechanism`` table and also from a separate
`BNF <https://bnf.nice.org.uk/>`_ database.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.drug_lookups import (
    chembl_queries as cq,
    bnf_queries as bq
)
from tqdm import tqdm
import argparse
import csv
import os
import sys
import re
import pprint as pp


_SCRIPT_NAME = 'bm-drug-target-summary'
"""The name of the script that is implemented in this module (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""
_SUMMARY_CACHE = {}
"""A module level cache for the summary queries to prevent excessive
(expensive) database queries (`dict`)
"""

ATC_LEVEL1 = [
    'ALIMENTARY TRACT AND METABOLISM',
    'BLOOD AND BLOOD FORMING ORGANS',
    'CARDIOVASCULAR SYSTEM',
    'DERMATOLOGICALS',
    'GENITO URINARY SYSTEM AND SEX HORMONES',
    "SYSTEMIC HORMONAL PREPARATIONS, EXCL. SEX HORMONES AND INSULINS",
    'ANTIINFECTIVES FOR SYSTEMIC USE',
    'ANTINEOPLASTIC AND IMMUNOMODULATING AGENTS',
    'MUSCULO-SKELETAL SYSTEM',
    'NERVOUS SYSTEM',
    "ANTIPARASITIC PRODUCTS, INSECTICIDES AND REPELLENTS",
    'RESPIRATORY SYSTEM',
    'SENSORY ORGANS',
    'VARIOUS',
]
ATC_LEVEL1_MAP = dict(
    [(i, "target_atc_{}".format(re.sub(r'\W+', '_', i.lower())))
     for i in ATC_LEVEL1]
)
ATC_FIELDS = list(ATC_LEVEL1_MAP.values())
PHASE_BASE_STR = 'target_n_phase_{0}_compounds'

# Some column names in the summary
QUERY_PROTEIN = 'query_protein_id'
CHEMBL_ID = 'target_chembl_id'
PREF_NAME = 'target_pref_name'
ORGANISM = 'target_organism'
TYPE = 'target_type'
N_COMPONENTS = 'target_n_components'
N_CHEMBL_INDICATIONS = 'target_n_chembl_indications'
N_BNF_INDICATIONS = 'target_n_bnf_indications'
N_BNF_SIDE_EFFECTS = 'target_n_bnf_side_effects'
MAX_PHASE = 'target_max_compound_phase'
N_BNF_INDICATION_COMPOUNDS = 'target_n_bnf_indication_compounds'
N_BNF_SIDE_EFFECT_COMPOUNDS = 'target_n_bnf_side_effect_compounds'
N_COMPOUNDS = 'target_n_compounds'
N_MECHANISMS = 'target_n_drug_mechanism_compounds'
N_INHIBIT = 'target_n_inhibitory_mechanisms'
N_ACTIVATE = 'target_n_activation_mechanisms'
N_ASSAYS = 'target_n_assays'
N_ACTIVITY = 'target_n_activities'
N_THERAPY = 'target_n_therapeutic_flag_compounds'
N_RO5 = 'target_n_ro5_zero_violations_compounds'
N_RO3 = 'target_n_ro3_pass_compounds'
N_USAN = 'target_n_usan_compounds'
N_ATC = 'target_n_atc_compounds'
N_BLACKBOX = 'target_n_blackbox_warning_compounds'
N_WITHDRAWN = 'target_n_withdrawn_compounds'
N_PHASE_0 = PHASE_BASE_STR.format(0)
N_PHASE_1 = PHASE_BASE_STR.format(1)
N_PHASE_2 = PHASE_BASE_STR.format(2)
N_PHASE_3 = PHASE_BASE_STR.format(3)
N_PHASE_4 = PHASE_BASE_STR.format(4)

HEADER = [
    QUERY_PROTEIN,
    CHEMBL_ID,
    PREF_NAME,
    ORGANISM,
    TYPE,
    N_COMPONENTS,
    N_CHEMBL_INDICATIONS,
    N_BNF_INDICATIONS,
    N_BNF_SIDE_EFFECTS,
    MAX_PHASE,
    N_BNF_INDICATION_COMPOUNDS,
    N_BNF_SIDE_EFFECT_COMPOUNDS,
    N_COMPOUNDS,
    N_MECHANISMS,
    N_INHIBIT,
    N_ACTIVATE,
    N_ASSAYS,
    N_ACTIVITY,
    N_THERAPY,
    N_RO5,
    N_RO3,
    N_USAN,
    N_ATC,
    N_BLACKBOX,
    N_WITHDRAWN,
    N_PHASE_1,
    N_PHASE_2,
    N_PHASE_3,
    N_PHASE_4
] + list(ATC_LEVEL1_MAP.values())


# Other fields in the summary info but not in the output
BNF_EFFECTS = 'bnf_effects'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    # Session maker
    chembl_conn = cq.chembl_sqlite_connect(args.chembl)

    bnf_conn = None
    if args.bnf is not None:
        bnf_conn = bq.bnf_sqlite_connect(args.bnf)

    try:
        get_drug_target_summary(
            args.infile, chembl_conn, bnf_conn=bnf_conn, outfile=args.outfile,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment, tmp_dir=args.tmp_dir,
            prot_id=args.prot_id, no_target=args.missing
        )
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    arguments.add_drug_data_arguments(parser)
    arguments.add_missing_args(
        parser,
        help="Include proteins that have no ChEMBL target in the output"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()

    if args.chembl is None:
        raise ValueError("Must pass a ChEMBL database argument")

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_drug_target_summary(infile, chembl_conn, bnf_conn=None, outfile=None,
                            verbose=False, prot_id=con.UNIPROT_PROT_ID,
                            tmp_dir=None, comment=con.DEFAULT_COMMENT,
                            no_target=False, delimiter=con.DEFAULT_DELIMITER,
                            encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of drug summaries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.

    See also
    --------
    :ref:`documentation page <dt_summary>`
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_drug_target_summary(
                    infile, chembl_conn, bnf_conn=bnf_conn, prot_id=prot_id,
                    no_target=no_target, comment=comment, delimiter=delimiter,
                    encoding=encoding, yield_header=True
                ),
                desc="[progress] fetching target summaries: ",
                unit=" summary",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_drug_target_summary(infile, chembl_conn, bnf_conn=None,
                              prot_id=con.UNIPROT_PROT_ID,
                              no_target=False, comment=con.DEFAULT_COMMENT,
                              delimiter=con.DEFAULT_DELIMITER,
                              encoding=con.DEFAULT_ENCODING,
                              yield_header=True):
    """Yield drug target summary results from an input file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chembl_conn : `sqlalchemy.orm.sessionmaker`
        An object that can be used to create SQLAlchemy Sessions against
        ChEMBL: ``session = session_maker()``
    bnf_conn : `sqlalchemy.orm.sessionmaker`, optional, default: `NoneType`
        An object that can be used to create SQLAlchemy Sessions against
        the BNF: ``session = session_maker()``
    prot_id : `str`, optional, default: `con.UNIPROT_PROT_ID`
        The name of the protein ID column in the input file.
    no_target : `bool`, optional, default: `False`
        Report all the entries that have no ChEMBL target in addition to those
        that do (not implemented yet but this option is on as default).
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.

    Yields
    ------
    output_row : `list`
        The high level target summary of for the protein ID (including rows
        that have no target mapping).

    Notes
    -----
    The drug targets summary is a high level overview of counts of specific
    attributes on a target.

    See also
    --------
    :ref:`documentation page <dt_summary>`
    """
    chembl_session = chembl_conn()

    try:
        bnf_session = bnf_conn()
    except TypeError:
        # Probably NoneType, which is OK, if not then we raise
        if bnf_conn is not None:
            raise
        bnf_session = None

    kwargs = dict(
        comment=comment,
        delimiter=delimiter,
        encoding=encoding
    )

    try:
        with utils.BaseParser(infile, **kwargs) as parser:
            prot_id_idx = parser.get_col_idx(prot_id)

            if yield_header is True:
                yield parser.header + parser.get_unique_col(
                    ['target_summary_input_idx'] + HEADER
                )

            for idx, row in enumerate(parser, 1):
                prot_id_value = row[prot_id_idx]
                match = False
                if prot_id is not None:
                    for summary_record in get_target_summary(
                        chembl_session, bnf_session, prot_id_value
                    ):
                        match = True
                        summary_row = []
                        for col in HEADER:
                            summary_row.append(summary_record[col])
                        yield row + [idx] + summary_row
                if match is False and no_target is True:
                    summary_row = []
                    # If we want to output no matches
                    for col in HEADER:
                        summary_row.append(None)
                    yield row + [idx] + summary_row

    finally:
        chembl_session.close()

        if bnf_session is not None:
            bnf_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_summary(chembl_session, bnf_session, prot_id):
    """Get a target summary for a given uniprot ID

    Parameters
    ----------
    chembl_session : `sqlalchemy.Session`
        The sesion object to query against ChEMBL.
    bnf_session : `sqlalchemy.Session` or `NoneType`
        The session object to query against the BNF. If not available then pass
        `NoneType`.
    prot_id : `str`
        The protein ID to map to ChEMBL target identifiers. In reality this
        does not have to be a protein ID it could be anything that is a target
        component in ChEMBL. There are no restrictions on the query

    Returns
    -------
    target_summary : `dict`
        The full summary information on the target.
    """
    try:
        # First does a summary appear in the module level cache?
        return _SUMMARY_CACHE[prot_id]
    except KeyError:
        pass

    chembl_summary = cq.get_protein_info(chembl_session, prot_id)

    summaries = []
    for i in chembl_summary:
        if bnf_session is not None:
            i[BNF_EFFECTS] = bq.get_target_effects(
                bnf_session, i[CHEMBL_ID]
            )
        else:
            i[BNF_EFFECTS] = []

        summary_record = {
            QUERY_PROTEIN: prot_id,
            CHEMBL_ID: i[CHEMBL_ID],
            PREF_NAME: i[PREF_NAME],
            ORGANISM: i[ORGANISM],
            TYPE: i[TYPE],
            N_COMPONENTS: len(i[cq.COMPONENTS])
        }
        _chembl_assays(i, summary_record)
        _compound_counts(i, summary_record)
        _compound_phases(i, summary_record)
        _compound_atc(i, summary_record)
        _chembl_indicaions(i, summary_record)
        _bnf_indicaions(i, summary_record)
        _bnf_side_effects(i, summary_record)
        summaries.append(summary_record)

    # Update the cache
    _SUMMARY_CACHE[prot_id] = summaries
    return summaries


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _chembl_assays(target_summary, summary_record):
    """Extract the number of assays and number of activity assays from the
    target summary.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Notes
    -----
    This adds ``target_n_assays`` and ``target_n_activities`` data to the
    ``summary_record``
    """
    summary_record[N_ASSAYS] = len(target_summary[cq.ASSAYS])
    summary_record[N_ACTIVITY] = len(
        [i for i in target_summary[cq.ASSAYS] if i['assay_type'] != 'A']
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _chembl_indicaions(target_summary, summary_record):
    """Summarise the chembl indication data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. This has the following
        updates: The number of mechanisms associated with the target, the
        number of ChEMBL indications associated with the target, the number of
        drugs with inhibitory mechanisms against the target and the number of
        drugs with activation mechanisms against the target. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds the following keys: ``target_n_drug_mechanism_compounds``,
    ``target_n_inhibitory_mechanisms``, ``target_n_activation_mechanisms`` and
    ``target_n_chembl_indications``.
    """
    compounds = set()
    indications = set()
    mechanisms = dict()
    for i in target_summary[cq.INDICATIONS]:
        # pp.pprint(i)
        # raise NotImplementedError("implement ChEMBL indication summary")
        _add_to_set(compounds, i['compound_chembl_id'])
        _add_to_set(indications, i['mesh_heading'])

        try:
            mechanisms[i['action_type']] += 1
        except KeyError:
            mechanisms[i['action_type']] = 1
        # raise NotImplementedError("implement ChEMBL indication summary")

    summary_record[N_MECHANISMS] = len(compounds)
    summary_record[N_CHEMBL_INDICATIONS] = len(indications)
    summary_record[N_INHIBIT] = 0
    summary_record[N_ACTIVATE] = 0

    for i in cq.INHIBIT_MECH:
        try:
            summary_record[N_INHIBIT] += mechanisms[i]
        except KeyError:
            pass

    for i in cq.ACTIVE_MECH:
        try:
            summary_record[N_ACTIVATE] += mechanisms[i]
        except KeyError:
            pass

    return summary_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _bnf_indicaions(target_summary, summary_record):
    """Summarise the BNF indication data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds the following keys: ``target_n_bnf_indication_compounds``,
    ``target_n_bnf_indications``.
    """
    compounds = set()
    indications = set()
    for i in target_summary[BNF_EFFECTS]:
        if i['drug_effect_type'] == 'indication':
            _add_to_set(compounds, i['compound_chembl_id'])
            _add_to_set(indications, i['drug_effect_name'])
    summary_record[N_BNF_INDICATION_COMPOUNDS] = len(compounds)
    summary_record[N_BNF_INDICATIONS] = len(indications)

    return summary_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _bnf_side_effects(target_summary, summary_record):
    """Summarise the BNF side effect data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds the following keys: ``target_n_bnf_side_effects``,
    ``target_n_bnf_side_effect_compounds``
    """
    compounds = set()
    side_effects = set()
    for i in target_summary[BNF_EFFECTS]:
        if i['drug_effect_type'] == 'side_effect':
            _add_to_set(compounds, i['compound_chembl_id'])
            _add_to_set(side_effects, i['drug_effect_name'])
    summary_record[N_BNF_SIDE_EFFECT_COMPOUNDS] = len(compounds)
    summary_record[N_BNF_SIDE_EFFECTS] = len(side_effects)

    return summary_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _add_to_set(the_set, to_add):
    """Add an element to the set only if it is defined. Being defined is
    defined as not being ``NoneType`` and not being ''.

    Parameters
    ----------
    the_set : `set`
        The set to add to
    to_add : `any`
        The object to add to the set if it is defined.
    """
    if to_add is not None and to_add != '':
        the_set.add(to_add)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _compound_phases(target_summary, summary_record):
    """Summarise the ChEMBL compound max phase data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds the following keys: ``target_max_compound_phase``,
    ``target_n_phase_0_compounds``, ``target_n_phase_1_compounds``,
    ``target_n_phase_2_compounds``, ``target_n_phase_3_compounds``,
    ``target_n_phase_4_compounds``.
    """
    phases = {}
    summary_record[MAX_PHASE] = 0
    for c in target_summary[cq.COMPOUNDS]:
        try:
            summary_record[MAX_PHASE] = \
                max(summary_record[MAX_PHASE],
                    c['max_phase'])
        except KeyError:
            summary_record[MAX_PHASE] = c['max_phase']
        except TypeError:
            # In some recent ChEMBL versions the max phase has been
            # None, so we set it to 0 in that case
            if c['max_phase'] is not None:
                # Legit error
                raise
            summary_record.setdefault(MAX_PHASE, 0)
            c['max_phase'] = 0

        try:
            phases[c['max_phase']] += 1
        except KeyError:
            phases[c['max_phase']] = 1

    for p in range(5):
        key = PHASE_BASE_STR.format(p)
        try:
            summary_record[key] = phases[p]
        except KeyError:
            summary_record[key] = 0

    return summary_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _compound_counts(target_summary, summary_record):
    """Summarise the ChEMBL compound data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds the following keys: ``target_n_ro3_pass_compounds``,
    ``target_n_ro5_zero_violations_compounds``, ``target_n_usan_compounds``,
    ``target_n_atc_compounds``, ``target_n_therapeutic_flag_compounds``,
    ``target_n_blackbox_warning_compounds``, ``target_n_withdrawn_compounds``,
    ``target_n_compounds``.
    """
    summary_record[N_RO5] = 0
    summary_record[N_RO3] = 0
    summary_record[N_USAN] = 0
    summary_record[N_ATC] = 0
    summary_record[N_THERAPY] = 0
    summary_record[N_BLACKBOX] = 0
    summary_record[N_WITHDRAWN] = 0
    idx = 0
    for idx, c in enumerate(target_summary[cq.COMPOUNDS], 1):
        summary_record[N_RO5] += \
            int(c['num_lipinski_ro5_violations'] == 0)
        summary_record[N_RO3] += int(c['ro3_pass'] == 'Y')
        summary_record[N_USAN] += int(c['usan_year'] is not None)
        summary_record[N_ATC] += int(bool(len(c['atc'])))
        summary_record[N_THERAPY] += \
            c['therapeutic_flag']
        summary_record[N_BLACKBOX] += \
            c['black_box_warning']
        summary_record[N_WITHDRAWN] += c['withdrawn_flag']
    summary_record[N_COMPOUNDS] = idx
    return summary_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _compound_atc(target_summary, summary_record):
    """Summarise the ChEMBL ATC data.

    Parameters
    ----------
    target_summary : `dict`
        The full data on the target to extract the summary information from.
    summary_record : `dict`
        A flat dictionary of summary target information. The array assay
        information will be added to this.

    Returns
    -------
    summary_record : `dict`
        A flat dictionary of summary target information. The return is not
        necessary as the additions are happening in place.

    Notes
    -----
    This adds: ``target_n_atc_compounds`` and a key for the number of compounds
    in each level 1 ATC code.
    """
    natc = 0
    for a in ATC_FIELDS:
        summary_record[a] = 0

    for c in target_summary[cq.COMPOUNDS]:
        for a in c['atc']:
            natc += 1
            summary_record[ATC_LEVEL1_MAP[a['level1_description']]] += 1
    summary_record[N_ATC] = natc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

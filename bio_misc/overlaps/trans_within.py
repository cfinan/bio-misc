"""Define the canonical transcripts that overlap features defined within an
input file (or ``STDIN``)+ an optional flanking region applied to the feature
start+end. The most common use case would be to look at the genes that overlap
within a defined flank upstream and downstream of a GWAS association.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from ensembl_rest_client import client, utils as rest_utils
from tqdm import tqdm
import argparse
import csv
import os
# import sys
# import pprint as pp


_PROG_NAME = 'trans-within-flank'
"""The program name (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""
_DEFAULT_REST = 'https://rest.ensembl.org'
"""The default rest URL (`str`)
"""
_DEFAULT_FLANK = 250000
"""The default flank in base pairs (`int`)
"""
_HEADER = [
    ('requested_region_length', 'requested_region_length'),
    ('returned_region_length', 'region_length'),
    ('ensembl_gene_id', 'gene_id'),
    ('external_gene_id', 'display_name'),
    ('ensembl_trans_id', 'id'),
    ('external_trans_id', 'external_name'),
    ('trans_chr_name', 'seq_region_name'),
    ('trans_start_pos', 'start'),
    ('trans_end_pos', 'end'),
    ('trans_strand', 'strand'),
    ('trans_biotype', 'biotype'),
    ('trans_description', 'description'),
    ('trans_distance_rank_from_input', 'distance_rank'),
    ('trans_distance_rank_biotype_from_input', 'distance_rank_biotype'),
    ('trans_min_distance_from_input', 'min_dist'),
    ('trans_overlap_with_input', 'overlap'),
    ('trans_assembly', 'assembly_name'),
    ('trans_version', 'version'),
    ('trans_source', 'source')
]
"""The output header column names ``[0]`` and mappings into the keys of the
``dict`` returned from the REST query ``[1]`` in the ``tuple``
(`list` (`str`, `str`)).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_trans_within(infile, rest_client, species, outfile=None,
                     chr_name=con.CHR_NAME, start_pos=con.START_POS,
                     end_pos=None, flank=0, delimiter=con.DEFAULT_DELIMITER,
                     verbose=False, encoding=con.DEFAULT_ENCODING,
                     tmp_dir=None, comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    of input file columns and flanking regions.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client to use to query for genes.
    species : `str`
        The species that the rest client should use when querying for genes.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    flank : `int`, optional, default: `0`
        The flanking region to use for overlapping genes. If set to ``0`` (the
        default), then only genes overlapping the ``start_pos``/``end_pos`` in
        the input file are extracted.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_trans_within(infile, rest_client, species,
                                   chr_name=chr_name, start_pos=start_pos,
                                   end_pos=start_pos, flank=flank,
                                   encoding=encoding, delimiter=delimiter,
                                   comment=comment),
                desc="[progress] fetching nearest transcripts: ",
                unit=" returned transcripts"
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_trans_within(infile, rest_client, species, chr_name=con.CHR_NAME,
                       start_pos=con.START_POS, end_pos=None, flank=0,
                       encoding=con.DEFAULT_ENCODING, yield_header=True,
                       comment=con.DEFAULT_COMMENT,
                       delimiter=con.DEFAULT_DELIMITER):
    """Yield rows of genes that overlap the feature + any flank.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client to use to query for genes.
    species : `str`
        The species that the rest client should use when querying for genes.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    end_pos : `str` or `NoneType`, optional, default: `NoneType`
        The end position column in the input file, if not present then the
        start position is used for end position.
    flank : `int`, optional, default: `0`
        The flanking region to use for overlapping genes. If set to ``0`` (the
        default), then only genes overlapping the ``start_pos``/``end_pos`` in
        the input file are extracted.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        Should the first row that is yielded from this function be the header
        for the output file.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    """
    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        end_pos=end_pos,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )
    with utils.CoordsParser(infile, **kwargs) as parser:
        if yield_header is True:
            yield parser.header + ['genes_within_input_idx', 'flank'] + \
                [i[0] for i in _HEADER]
        for idx, row in enumerate(parser, 1):
            chr_name, start_pos, end_pos = parser.get_coords(row)
            nearest_genes = rest_utils.get_genes_within(
                rest_client, species, chr_name, start_pos - flank,
                end_pos=end_pos + flank, use_center=False,
                biotypes=None, ref_point_start=start_pos,
                ref_point_end=end_pos, use_trans=True
            )
            # pp.pprint(nearest_genes)
            for i in nearest_genes:
                yield row + [idx, flank] + process_record(i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_record(record):
    """Process a record that has been returned from the ensembl rest client

    Parameters
    ----------
    record : `dict`
        An output from the ensembl rest query.

    Returns
    -------
    processed_record : `list`
        A record that can be written to the output file, the order of the
        columns is as in the header.
    """
    processed_record = []
    for header_name, rest_name in _HEADER:
        try:
            processed_record.append(record[rest_name])
        except KeyError:
            if header_name == 'ensembl_gene_id':
                processed_record.append(record['gene_data']['id'])
            elif header_name == 'external_gene_id':
                processed_record.append(record['gene_data']['display_name'])
            elif rest_name == 'external_gene_id':
                processed_record.append(record['gene_id'])
            elif rest_name == 'distance_rank':
                processed_record.append(1)
            else:
                processed_record.append(None)
    return processed_record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_PROG_NAME, pkg_name, __version__)
    m.msg_args(args)

    rest = client.Rest(url=args.rest)

    get_trans_within(
        args.infile, rest, args.species, outfile=args.outfile,
        chr_name=args.chr_name, start_pos=args.start_pos, end_pos=args.end_pos,
        flank=args.flank, delimiter=args.delimiter, verbose=args.verbose,
        encoding=args.encoding, comment=args.comment, tmp_dir=args.tmp_dir
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_rest_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_genomic_coords_arguments(parser)
    parser.add_argument(
        '-f', '--flank', type=int, default=_DEFAULT_FLANK,
        help="The flanking size around each input region (default: "
        "{}).".format(_DEFAULT_FLANK)
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    _main()

"""Compare two files and pull out overlapping sites between them. This is
 designed to pull out matching variant sites, with the same chromosome, start
 position and alleles (in any order)). This is not designed to match on ranges.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import (
    __version__,
    __name__ as pkg_name
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from pyaddons import log
from merge_sort import chunks, merge
from multi_join import join
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import pprint as pp


_DESC = __doc__
"""The script description (`str`)
"""
_PROG_NAME = 'bm-site-overlap'
"""The name of the script (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        site_overlaps(args.infile1, args.infile2, outfile=args.outfile,
                      verbose=args.verbose, tmpdir=args.tmp_dir,
                      chr_name1=args.chr_name1, chr_name2=args.chr_name2,
                      start_pos1=args.start_pos1, start_pos2=args.start_pos2,
                      ref_allele1=args.ref_allele1,
                      ref_allele2=args.ref_allele2,
                      alt_allele1=args.alt_allele1,
                      alt_allele2=args.alt_allele2,
                      sort1=args.sort1, sort2=args.sort2,
                      return_all1=args.return_all1,
                      return_all2=args.return_all2,
                      delimiter1=args.delimiter1,
                      delimiter2=args.delimiter2,
                      encoding1=args.encoding1,
                      encoding2=args.encoding2,
                      comment1=args.comment1,
                      comment2=args.comment2,
                      no_alleles=args.no_alleles)
        log.log_end(logger)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Extract overlapping variant sites between two files. "
        "*** Note, this implementation us currently broken as the join module"
        " needs updating ***"
    )
    parser.add_argument(
        'infile1', type=str,
        help="The first input file."
    )
    parser.add_argument(
        'infile2', type=str,
        help="The second input file."
    )
    parser.add_argument(
        '-d', '--delimiter', nargs=2, type=str,
        default=[con.DEFAULT_DELIMITER, con.DEFAULT_DELIMITER],
        help="The delimiter of the input files: <file 1 delimiter> "
        "<file 2 delimiter> (default: {0},{1}).".format(
                con.DEFAULT_DELIMITER, con.DEFAULT_DELIMITER
            )
        )
    parser.add_argument(
        '-C', '--comment', type=str, nargs=2,
        default=[con.DEFAULT_COMMENT, con.DEFAULT_COMMENT],
        help="The comment character in the input files. <file 1 comment> "
        "<file 2 comment> (default: {0},{1})".format(
            con.DEFAULT_COMMENT, con.DEFAULT_COMMENT
        )
    )
    parser.add_argument(
        '-E', '--encoding', type=str, nargs=2,
        default=[con.DEFAULT_ENCODING, con.DEFAULT_ENCODING],
        help="The default encoding of the file <file 1 encoding> "
        "<file 2 encoding> (default: {0},{1}).".format(
            con.DEFAULT_ENCODING, con.DEFAULT_ENCODING
        )
    )
    parser.add_argument(
        '-c', '--chr-name', type=str, nargs=2,
        default=[con.CHR_NAME, con.CHR_NAME],
        help="The name of the chromosome columns in both files "
        "<file 1 chr_name> <file 2 chr_name> (default: {0},{1}).".format(
            con.CHR_NAME, con.CHR_NAME
        )
    )
    parser.add_argument(
        '-s', '--start-pos', type=str, nargs=2,
        default=[con.START_POS, con.START_POS],
        help="The name of the start position columns in both files "
        "<file 1 start pos> <file 2 start pos> (default: {0},{1}).".format(
            con.START_POS, con.START_POS
        )
    )
    parser.add_argument(
        '-R', '--ref-allele', type=str, nargs=2,
        default=[con.REF_ALLELE, con.REF_ALLELE],
        help="The name of the effect allele column (default: {0},{1}).".format(
            con.REF_ALLELE, con.REF_ALLELE
        )
    )
    parser.add_argument(
        '-A', '--alt-allele', type=str, nargs=2,
        default=[con.ALT_ALLELE, con.ALT_ALLELE],
        help="The name of the other allele column  (default: {0},{1}).".format(
            con.ALT_ALLELE, con.ALT_ALLELE
        )
    )
    parser.add_argument(
        '--sort', type=int, nargs='+', choices=[1, 2], default=[],
        help="Should the files be sorted prior to overlaps (default: "
        "assume sorted)"
    )
    parser.add_argument(
        '--return-all', type=int, nargs='+', choices=[1, 2], default=[],
        help="Should all the non-overlaping sites be returned for either file"
        " (default: intersection)"
    )
    parser.add_argument(
        '-T', '--tmp-dir', type=str,
        help="The tmp directory to use (default: system temp)."
    )
    arguments.add_input_output(
        parser, infile=False, outfile_optional=True
    )
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '--no-alleles', action="store_true",
        help="Do not match on alleles."
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Parse all the paired arguments into separate arguments
    for a in ['delimiter', 'comment', 'encoding', 'chr_name', 'start_pos',
              'ref_allele', 'alt_allele']:
        for i in range(2):
            setattr(args, '{0}{1}'.format(a, i+1), getattr(args, a)[i])

    # Now the sort and return all args
    for a in ['sort', 'return_all']:
        setattr(args, '{0}1'.format(a), False)
        setattr(args, '{0}2'.format(a), False)

        for i in getattr(args, a):
            setattr(args, '{0}{1}'.format(a, i), True)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def site_overlaps(infile1, infile2, outfile=None, verbose=False, tmpdir=None,
                  chr_name1=con.CHR_NAME, chr_name2=con.CHR_NAME,
                  start_pos1=con.START_POS, start_pos2=con.START_POS,
                  ref_allele1=con.REF_ALLELE, ref_allele2=con.REF_ALLELE,
                  alt_allele1=con.ALT_ALLELE, alt_allele2=con.ALT_ALLELE,
                  sort1=False, sort2=False,
                  return_all1=False, return_all2=False,
                  delimiter1=con.DEFAULT_DELIMITER,
                  delimiter2=con.DEFAULT_DELIMITER,
                  encoding1=con.DEFAULT_ENCODING,
                  encoding2=con.DEFAULT_ENCODING,
                  comment1=con.DEFAULT_COMMENT,
                  comment2=con.DEFAULT_COMMENT,
                  no_alleles=False, chunksize=100000):
    """The main high level API function that takes an input file and generates
    and output file of matching entries.

    Parameters
    ----------
    infile1 : `str`
        The first input file.
    infile2 : `str`
        The second input file.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    tmpdir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    chr_name1 : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of file 1.
    chr_name2 : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of file 2.
    start_pos1 : `str`, optional: default: \
    `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of file 1.
    start_pos2 : `str`, optional: default: \
    `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of file 2.
    ref_allele1 : `str`, optional: default: \
    `bio_misc.common.constants.REF_ALLELE`
        The name of the reference allele column in the header of file 1.
    ref_allele2 : `str`, optional: default: \
    `bio_misc.common.constants.REF_ALLELE`
        The name of the reference allele column in the header of file 2.
    alt_allele1 : `str`, optional: default: \
    `bio_misc.common.constants.ALT_ALLELE`
        The name of the alternate allele column in the header of file 1
        (bi-allelic).
    alt_allele2 : `str`, optional: default: \
    `bio_misc.common.constants.ALT_ALLELE`
        The name of the alternate allele column in the header of file 2
        (bi-allelic).
    sort1 : `bool`, optional: default: `False`
        Should the first file be sorted prior to merging.
    sort2 : `bool`, optional: default: `False`
        Should the second file be sorted prior to merging.
    return_all1 : `bool`, optional: default: `False`
        Should all the rows of the first file be returned even if there are
        no overlaps. I am not 100% sure if this is fully implemented.
    return_all2 : `bool`, optional: default: `False`
        Should all the rows of the second file be returned even if there are
        no overlaps. I am not 100% sure if this is fully implemented.
    delimiter1 : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the first input (and output) file.
    delimiter2 : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the second input file.
    encoding1 : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the first input (and output) files.
    encoding2 : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the second input file.
    comment1 : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row in the first input file that will be
        skipped if they occur before the header.
    comment2 : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row in the second input file that will be
        skipped if they occur before the header.
    no_alleles : `bool`, optional, default: `False`
        Only match on chr_name and start_pos and not on alleles.
    chunksize : `int`, optional, default: `100000`
        The number of rows to hold in memory when sorting.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmpdir,
                       encoding=encoding1) as out:
        writer = csv.writer(out, delimiter=delimiter1,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_site_overlaps(infile1, infile2, tmpdir=tmpdir,
                                    chr_name1=chr_name1, chr_name2=chr_name2,
                                    start_pos1=start_pos1,
                                    start_pos2=start_pos2,
                                    ref_allele1=ref_allele1,
                                    ref_allele2=ref_allele2,
                                    alt_allele1=alt_allele1,
                                    alt_allele2=alt_allele2,
                                    sort1=sort1, sort2=sort2,
                                    return_all1=return_all1,
                                    return_all2=return_all2,
                                    delimiter1=delimiter1,
                                    delimiter2=delimiter2,
                                    encoding1=encoding1,
                                    encoding2=encoding2,
                                    comment1=comment1,
                                    comment2=comment2,
                                    yield_header=True,
                                    no_alleles=no_alleles,
                                    chunksize=chunksize),
                desc="[progress] get overlapping sites: ",
                unit=" returned overlaps",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_site_overlaps(infile1, infile2, tmpdir=None,
                        chr_name1=con.CHR_NAME, chr_name2=con.CHR_NAME,
                        start_pos1=con.START_POS, start_pos2=con.START_POS,
                        ref_allele1=con.REF_ALLELE, ref_allele2=con.REF_ALLELE,
                        alt_allele1=con.ALT_ALLELE, alt_allele2=con.ALT_ALLELE,
                        sort1=False, sort2=False,
                        return_all1=False, return_all2=False,
                        delimiter1=con.DEFAULT_DELIMITER,
                        delimiter2=con.DEFAULT_DELIMITER,
                        encoding1=con.DEFAULT_ENCODING,
                        encoding2=con.DEFAULT_ENCODING,
                        comment1=con.DEFAULT_COMMENT,
                        comment2=con.DEFAULT_COMMENT,
                        yield_header=True,
                        no_alleles=False,
                        chunksize=100000):
    """Yield rows of matching sites between the two files.

    Parameters
    ----------
    infile1 : `str`
        The first input file.
    infile2 : `str`
        The second input file.
    tmpdir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    chr_name1 : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of file 1.
    chr_name2 : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of file 2.
    start_pos1 : `str`, optional: default: \
    `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of file 1.
    start_pos2 : `str`, optional: default: \
    `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of file 2.
    ref_allele1 : `str`, optional: default: \
    `bio_misc.common.constants.REF_ALLELE`
        The name of the reference allele column in the header of file 1.
    ref_allele2 : `str`, optional: default: \
    `bio_misc.common.constants.REF_ALLELE`
        The name of the reference allele column in the header of file 2.
    alt_allele1 : `str`, optional: default: \
    `bio_misc.common.constants.ALT_ALLELE`
        The name of the alternate allele column in the header of file 1
        (bi-allelic).
    alt_allele2 : `str`, optional: default: \
    `bio_misc.common.constants.ALT_ALLELE`
        The name of the alternate allele column in the header of file 2
        (bi-allelic).
    sort1 : `bool`, optional: default: `False`
        Should the first file be sorted prior to merging.
    sort2 : `bool`, optional: default: `False`
        Should the second file be sorted prior to merging.
    return_all1 : `bool`, optional: default: `False`
        Should all the rows of the first file be returned even if there are
        no overlaps. I am not 100% sure if this is fully implemented.
    return_all2 : `bool`, optional: default: `False`
        Should all the rows of the second file be returned even if there are
        no overlaps. I am not 100% sure if this is fully implemented.
    delimiter1 : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the first input (and output) file.
    delimiter2 : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the second input file.
    encoding1 : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the first input (and output) files.
    encoding2 : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the second input file.
    comment1 : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row in the first input file that will be
        skipped if they occur before the header.
    comment2 : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row in the second input file that will be
        skipped if they occur before the header.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    no_alleles : `bool`, optional, default: `False`
        Only match on chr_name and start_pos and not on alleles.
    chunksize : `int`, optional, default: `100000`
        The number of rows to hold in memory when sorting.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    """
    # We initialise to no join condition which means that only intersecting
    # rows will be returned
    join_condition1 = join.NO_CONDITION
    join_condition2 = join.NO_CONDITION

    # No monitors, so both files will run to the end
    monitor1 = None
    monitor2 = None

    # We want to return all the rows in both files, irrespective of joins
    if return_all1 is True and return_all2 is True:
        join_condition1 = join.RETURN_ALL
        join_condition2 = join.RETURN_ALL
        monitor1 = join.AllIterMonitor()
        monitor2 = monitor1
    elif return_all1 is True:
        # Return all the rows in the first file
        join_condition1 = join.RETURN_ALL
        monitor1 = join.AllIterMonitor()
    elif return_all2 is True:
        # Return all the rows in the second
        join_condition2 = join.RETURN_ALL
        monitor2 = join.AllIterMonitor()

    # Initialise coordinates parsers, these will either be used as is or
    # dropped into a merge sort
    file1 = utils.CoordsParser(
        infile1, chr_name=chr_name1, start_pos=start_pos1, encoding=encoding1,
        comment=comment1, delimiter=delimiter1
    )
    file2 = utils.CoordsParser(
        infile2, chr_name=chr_name2, start_pos=start_pos2,
        encoding=encoding2, comment=comment2, delimiter=delimiter2
    )

    try:
        # Attempt to open the files, if this fails then close and raise
        file1.open()
        file2.open()
    except Exception:
        for i in [file1, file2]:
            try:
                i.close()
            except Exception:
                pass
        raise

    # Get the column numbers for chromosome and start positions in both files
    f1_chr_idx = file1.chr_name_idx
    f1_start_idx = file1.start_pos_idx
    f2_chr_idx = file2.chr_name_idx
    f2_start_idx = file2.start_pos_idx

    # Initialise the join keys
    f1_join_key = None
    f2_join_key = None
    if no_alleles is False:
        # If we are using alleles set up the joins keys for that
        f1_ref_idx = file1.get_col_idx(ref_allele1)
        f1_alt_idx = file1.get_col_idx(alt_allele1)
        f2_ref_idx = file2.get_col_idx(ref_allele2)
        f2_alt_idx = file2.get_col_idx(alt_allele2)
        f1_join_key = _get_sort_key_alleles(
            f1_chr_idx, f1_start_idx, f1_ref_idx, f1_alt_idx
        )
        f2_join_key = _get_sort_key_alleles(
            f2_chr_idx, f2_start_idx, f2_ref_idx, f2_alt_idx
        )
    else:
        # set up the join keys for using no alleles
        f1_join_key = _get_sort_key_no_alleles(f1_chr_idx, f1_start_idx)
        f2_join_key = _get_sort_key_no_alleles(f2_chr_idx, f2_start_idx)

    # Get the expected lengths of the rows in each file
    f1_line_len = len(file1.header)
    f2_line_len = len(file2.header)

    # If we are yielding the header, then do so
    if yield_header is True:
        yield file1.header + file1.get_unique_col(file2.header)

    # Sort file 1 if needed
    if sort1 is True:
        file1 = _init_sort(
            file1, chunksize=chunksize, tmpdir=tmpdir, delimiter=delimiter1,
            ref_allele_col=ref_allele1, alt_allele_col=alt_allele1
        )

    # Sort file 2 if needed
    if sort2 is True:
        file2 = _init_sort(
            file2, chunksize=chunksize, tmpdir=tmpdir, delimiter=delimiter2,
            ref_allele_col=ref_allele2, alt_allele_col=alt_allele2
        )

    # Setup the join iterators for the input files
    join1 = join.JoinIterator(
        file1, key=f1_join_key,
        monitor=monitor1, join_condition=join_condition1
    )
    join2 = join.JoinIterator(
        file2, key=f2_join_key,
        monitor=monitor2, join_condition=join_condition2
    )

    try:
        # Remove the headers
        joiner = join.Join(join1, join2)
        for rows in joiner:
            # rows[0] or rows[1] will be empty if we are returning all for
            # either of them
            if len(rows[0]) == 0:
                rows[0].append(([None] * f1_line_len))
            elif len(rows[1]) == 0:
                rows[1].append(([None] * f2_line_len))
            for f1 in rows[0]:
                for f2 in rows[1]:
                    yield f1 + f2
    finally:
        file1.close()
        file2.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_sort(parser, chunksize=100000, tmpdir=None, no_alleles=False,
               ref_allele_col=None, alt_allele_col=None, **csv_kwargs):
    """External merge sort the contents of the parser.

    Parameters
    ----------
    parser : `bio_misc.common.utils.CoordsParser`
        A parser attached tot he file that needs sorting.
    chunksize : `int`, optional, default: `100000`
        The number of rows that the sort will hold in memory.
    tmpdir : `str`, optional, default: `NoneType`
        The location of temp files generated during the sort, if not provided
         the system temp location is used.
    **csv_kwargs
        Any Python `csv` keyword arguments.

    Returns
    -------
    merger : `merge_sort.merge.CsvIterativeHeapqMerge`
        A merge objects that will iterate through the sorted rows. This is
        returned opened and will need closing when finished.
    """
    sort_key = None
    if no_alleles is True:
        sort_key = _get_sort_key_no_alleles(
            parser.chr_name_idx, parser.start_pos_idx
        )
    else:
        ref_allele_idx = parser.get_col_idx(ref_allele_col)
        alt_allele_idx = parser.get_col_idx(alt_allele_col)
        sort_key = _get_sort_key_alleles(
            parser.chr_name_idx, parser.start_pos_idx, ref_allele_idx,
            alt_allele_idx
        )
    try:
        with chunks.CsvExtendedChunks(
                chunk_dir=tmpdir,
                key=sort_key,
                chunksize=chunksize,
                delete_on_error=True,
                chunk_suffix=".chunk.gz",
                **csv_kwargs
        ) as chunker:
            for row in parser:
                chunker.add_row(row)
    finally:
        parser.close()

    # Setup the merge for the chunks
    merger = merge.CsvIterativeHeapqMerge(
        chunker.chunk_file_list,
        key=sort_key,
        tmpdir=tmpdir, header=False, delete=True, csv_kwargs=csv_kwargs
    )
    merger.open()
    return merger


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_sort_key_no_alleles(chr_name_idx, start_pos_idx):
    """Get a sort key not using any allele info.

    Parameters
    ----------
    chr_name_idx : `int`
        The chromosome name column index.
    start_pos_idx : `int`
        The start position column index.

    Returns
    -------
    sort_func : `function`
        A function to use to sort the file using no allele info, only
        chromosome, start pos
    """
    def _sort_key_no_alleles(row):
        """Sort function closure.

        Parameters
        ----------
        row : `list` of `str`
            The row to extract the keys from.

        Returns
        -------
        chr_name : `str`
            The chromosome name.
        start_pos : `int`
            The start position.
        """
        return (
            row[chr_name_idx],
            int(row[start_pos_idx])
        )
    return _sort_key_no_alleles


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_sort_key_alleles(chr_name_idx, start_pos_idx, ref_allele_idx,
                          alt_allele_idx):
    """Get a sort key not using allele info.

    Parameters
    ----------
    chr_name_idx : `int`
        The chromosome name column index.
    start_pos_idx : `int`
        The start position column index.
    ref_allele_idx : `int`
        The reference allele column index.
    alt_allele_idx : `int`
        The alternate allele column index.

    Returns
    -------
    sort_func : `function`
        A function to use to sort the file using chromosome, start pos and
        allele info.
    """
    def _sort_key_alleles(row):
        """Sort function closure.

        Parameters
        ----------
        row : `list` of `str`
            The row to extract the keys from.

        Returns
        -------
        chr_name : `str`
            The chromosome name.
        start_pos : `int`
            The start position.
        lowest_allele : `str`
            The allele which is the lowest in the sort order
        highest_allele : `str`
            The  which is the highest in the sort order
        """
        return (
            row[chr_name_idx],
            int(row[start_pos_idx]),
            *sorted([row[ref_allele_idx], row[alt_allele_idx]])
        )
    return _sort_key_alleles


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""Map somalogic seq_ids to uniprot IDs. This uses a pre-defined mapping file
within the package.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.example_data import examples
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from tqdm import tqdm
import argparse
import csv
import os
import sys
import re
# import pprint as pp


_SCRIPT_NAME = 'bm-map-seq-id'
"""The name of the script that is implemented in this module (`str`)
"""

HEADER = [
    ('uniprot_id', 'uniprot_accession'),
    ('platform', 'platform'),
    ('bad_seq_id', 'remove')
]
"""The columns added to the output file and their names in the mapping data.
The tuples are [0] output file header name (`str`) [1] mapping data name
(`str`) (`list` of `tuple`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_uniprot_ids(infile, mapping_dict, outfile=None, verbose=False,
                    seq_id='seq_id', tmp_dir=None, no_mapping=False,
                    delimiter=con.DEFAULT_DELIMITER,
                    encoding=con.DEFAULT_ENCODING,
                    comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings to somalogic seq-ids.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapping_dict : `dict`
        The keys of the `dict` are somalogic seq IDs and the values are lists
        of dictionaries that contain the uniprot IDs they map to and the status
        of the seq ID.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    seq_id : `str`, optional, default: `seq_id`
        The somalogic seq ID column that contains the seq IDs to map to
        uniprot.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_uniprot_ids(infile, mapping_dict, seq_id=seq_id,
                                  no_mapping=no_mapping, yield_header=True,
                                  encoding=encoding, delimiter=delimiter,
                                  comment=comment),
                desc="[progress] fetching uniprot IDs: ",
                unit=" returned IDs",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_uniprot_ids(infile, mapping_dict, seq_id='seq_id', yield_header=True,
                      delimiter=con.DEFAULT_DELIMITER, no_mapping=False,
                      encoding=con.DEFAULT_ENCODING,
                      comment=con.DEFAULT_COMMENT):
    """Yield uniprot mappings to somalogic seq-id identifiers.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    mapping_dict : `dict`
        The keys of the `dict` are somalogic seq IDs and the values are lists
        of dictionaries that contain the uniprot IDs they map to and the status
        of the seq ID.
    seq_id : `str`, optional, default: `seq_id`
        The somalogic seq ID column that contains the seq IDs to map to
        uniprot.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    with utils.BaseParser(infile, **kwargs) as parser:
        seq_id_idx = parser.get_col_idx(seq_id)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                ['uniprot_seq_id_mapping_input_idx'] + [i[0] for i in HEADER]
            )

        for idx, row in enumerate(parser, 1):
            # Make sure the seq_id is standardised, some inputs will have a
            # _ delimiter where as the mapping dict will have a - delimiter
            row[seq_id_idx] = re.sub(r'_', '-', row[seq_id_idx])
            try:
                for mapping in mapping_dict[row[seq_id_idx]]:
                    yield row + [idx] + [mapping[i[1]] for i in HEADER]
            except KeyError:
                if no_mapping is True:
                    yield row + [idx] + ([None] * len(HEADER))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        # Load up the mapping file
        mapping_dict = examples.get_data('somalogic_seq_ids')
        get_uniprot_ids(
            args.infile, mapping_dict, outfile=args.outfile,
            delimiter=args.delimiter, verbose=args.verbose,
            encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, seq_id=args.seq_id,
            no_mapping=args.no_map)
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Map somalogic seq-id identifiers to uniprot IDs."
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '--no-map', action="store_true",
        help="Include rows that have no mapping in the output file."
    )
    parser.add_argument(
        '--seq-id', type=str, default="seq_id",
        help="The name of the somalogic seq-id column in the input file."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""Map uniprot IDs to ensembl gene IDs
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from ensembl_rest_client import client
from requests.exceptions import HTTPError
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import pprint as pp


_SCRIPT_NAME = 'bm-map-ensembl-gene'
"""The name of the script that is implemented in this module (`str`)
"""
_SWISSPROT_NAME = "Uniprot/SWISSPROT"
"""The name of the UniProt/SwissProt database that Ensembl uses (`str`)
"""
_QUERY_CACHE = {}
"""A cache for the queries, this is useful if there are a lot of repeats in the
file (`dict`)
"""

HEADER = [
    ('ensembl_gene_id', 'gene_id')
]
"""The columns added to the output file and either names in the rest data.
The tuples are [0] output file header name (`str`) [1] rest data name (`str`)
(`list` of `tuple`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_ids(infile, rest_client, outfile=None, verbose=False,
                 species=con.DEFAULT_SPECIES, tmp_dir=None, cache=True,
                 uniprot_id=con.UNIPROT_PROT_ID, no_mapping=False,
                 delimiter=con.DEFAULT_DELIMITER,
                 encoding=con.DEFAULT_ENCODING,
                 comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client object to perform the queries.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    species : `str`, optional, default: `bio_misc.common.constants.DEFAULT_SPECIES`
        The default species to use for the rest queries.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    cache : `bool`, optional, default: `True`
        Cache the search results to reduce the number of rest queries. Will use
        more memory but will be faster.
    uniprot_id : `str`, optional, default: `bio_misc.common.constants.UNIPROT_PROT_ID`
        The uniprot ID column that contains the IDs to map to ensembl gene IDs.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_gene_ids(infile, rest_client, uniprot_id=uniprot_id,
                               species=species, no_mapping=no_mapping,
                               yield_header=True, encoding=encoding,
                               delimiter=delimiter, comment=comment,
                               cache=cache),
                desc="[progress] fetching gene IDs: ",
                unit=" returned IDs",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_gene_ids(infile, rest_client, uniprot_id=con.UNIPROT_PROT_ID,
                   species='homo_sapiens',
                   delimiter=con.DEFAULT_DELIMITER, no_mapping=False,
                   encoding=con.DEFAULT_ENCODING, yield_header=True,
                   comment=con.DEFAULT_COMMENT, cache=True):
    """Yield uniprot mappings to Ensembl gene identifiers.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client object to perform the queries.
    gene_id : `str`, optional, default: `con.ENSEMBL_GENE_ID`
        The Ensembl Gene ID column that contains the gene IDs to map to uniprot.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    include_trembl : `bool`, optional, default: `False`
        Include mappings to UniProt/TrEMBL in addition to UniProt/SwissProt.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    cache : `bool`, optional, default: `True`
        Cache the search results to reduce the number of rest queries. Will use
        more memory but will be faster.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    TypeError
        If any search terms are not `str` or `re.Pattern` objects.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    with utils.BaseParser(infile, **kwargs) as parser:
        uniprot_id_idx = parser.get_col_idx(uniprot_id)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                ['ensembl_gene_id_mapping_input_idx'] +
                [i[0] for i in HEADER] +
                ['bad_external_id']
            )

        for idx, row in enumerate(parser, 1):
            has_mapping = False
            flag = False

            try:
                for mapping in query_uniprot(rest_client, species,
                                             row[uniprot_id_idx],
                                             cache=cache):
                    has_mapping = True
                    yield row + [idx, mapping, int(flag)]
            except HTTPError as e:
                if e.response.status_code not in [400, 404]:
                    raise
                flag = True

            if has_mapping is False:
                yield row + [idx] + ([None] * len(HEADER)) + [int(flag)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_uniprot(rc, species, uniprot_id, cache=True):
    """
    """
    try:
        return _QUERY_CACHE[uniprot_id]
    except KeyError:
        pass

    ids = rc.get_xrefs_symbol(
        species, uniprot_id, db_type=None, external_db=_SWISSPROT_NAME,
        object_type='gene',
    )

    mappings = []

    for i in ids:
        # pp.pprint(i)
        mappings.append(i['id'])

    if cache is True:
        _QUERY_CACHE[uniprot_id] = mappings

    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        rest = client.Rest(url=args.rest)
        get_gene_ids(
            args.infile, rest, outfile=args.outfile, delimiter=args.delimiter,
            verbose=args.verbose, encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, uniprot_id=args.prot_id,
            species=args.species,
            no_mapping=args.no_map, cache=not args.no_cache
        )
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Map identifiers to ensembl gene IDs. Whilst this is"
        "intended for uniprot IDs it is not specific to them and should also"
        " work for other IDs."
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_rest_arguments(parser, species=True)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    parser.add_argument(
        '--no-map', action="store_true",
        help="Include rows that have no mapping in the output file."
    )
    parser.add_argument(
        '--no-cache', action="store_true",
        help="Turn off results caching. Will be slower but use less memory."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

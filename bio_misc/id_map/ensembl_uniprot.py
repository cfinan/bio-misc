"""Map ensembl gene identifiers to uniprot IDs
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from ensembl_rest_client import client
from requests.exceptions import HTTPError
from tqdm import tqdm
import argparse
import csv
import os
import sys
# import warnings
# import pprint as pp


_SCRIPT_NAME = 'bm-map-uniprot'
"""The name of the script that is implemented in this module (`str`)
"""
_SWISSPROT_NAME = "Uniprot/SWISSPROT"
"""The name of the UniProt/SwissProt database that Ensembl uses (`str`)
"""
_TREMBL_NAME = "Uniprot/SPTREMBL"
"""The name of the UniProt/Trembl database that Ensembl uses (`str`)
"""
_QUERY_CACHE = {}
"""A cache for the queries, this is useful if there are a lot of repeats in the
file (`dict`)
"""

HEADER = [
    ('uniprot_db_name', 'dbname'),
    ('uniprot_id', 'primary_id'),
    ('uniprot_syn_idx', 'uniprot_syn_idx'),
    ('uniprot_syn_id', 'uniprot_syn_id'),
    ('uniprot_display_id', 'display_id'),
    ('uniprot_desc', 'description'),
    ('uniprot_mapping', 'info_type'),
    ('uniprot_gene_start', 'ensembl_start'),
    ('uniprot_gene_end', 'ensembl_end'),
    ('uniprot_gene_identity', 'ensembl_identity'),
    ('uniprot_prot_start', 'xref_start'),
    ('uniprot_prot_end', 'xref_end'),
    ('uniprot_prot_identity', 'xref_identity'),
    ('uniprot_mapping_score', 'score'),
    ('uniprot_mapping_evalue', 'evalue'),
    ('uniprot_prot_version', 'version')
]
"""The columns added to the output file and either names in the rest data.
The tuples are [0] output file header name (`str`) [1] rest data name (`str`)
(`list` of `tuple`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_uniprot_ids(infile, rest_client, outfile=None, verbose=False,
                    gene_id=con.ENSEMBL_GENE_ID, tmp_dir=None,
                    delimiter=con.DEFAULT_DELIMITER, include_trembl=True,
                    encoding=con.DEFAULT_ENCODING, no_mapping=False,
                    comment=con.DEFAULT_COMMENT, cache=True):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client object to perform the queries.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    gene_id : `str`, optional, default: `con.ENSEMBL_GENE_ID`
        The Ensembl Gene ID column that contains the gene IDs to map to uniprot.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    include_trembl : `bool`, optional, default: `False`
        Include mappings to UniProt/TrEMBL in addition to UniProt/SwissProt.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    cache : `bool`, optional, default: `True`
        Cache the search results to reduce the number of rest queries. Will use
        more memory but will be faster.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_uniprot_ids(infile, rest_client, gene_id=gene_id,
                                  no_mapping=no_mapping, yield_header=True,
                                  encoding=encoding, delimiter=delimiter,
                                  comment=comment, cache=cache,
                                  include_trembl=include_trembl),
                desc="[progress] fetching uniprot IDs: ",
                unit=" returned IDs",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_uniprot_ids(infile, rest_client, gene_id=con.ENSEMBL_GENE_ID,
                      delimiter=con.DEFAULT_DELIMITER, no_mapping=False,
                      encoding=con.DEFAULT_ENCODING, yield_header=True,
                      include_trembl=True, comment=con.DEFAULT_COMMENT,
                      cache=True):
    """Yield uniprot mappings to Ensembl gene identifiers.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client object to perform the queries.
    gene_id : `str`, optional, default: `con.ENSEMBL_GENE_ID`
        The Ensembl Gene ID column that contains the gene IDs to map to uniprot.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    no_mapping : `bool`, optional, default: `False`
        Report all the entries that have no mappings in addition to those that
        do map.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    include_trembl : `bool`, optional, default: `False`
        Include mappings to UniProt/TrEMBL in addition to UniProt/SwissProt.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    cache : `bool`, optional, default: `True`
        Cache the search results to reduce the number of rest queries. Will use
        more memory but will be faster.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If any specified columns can not be found in the header.
    TypeError
        If any search terms are not `str` or `re.Pattern` objects.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    db_names = [_SWISSPROT_NAME]
    if include_trembl is True:
        db_names.append(_TREMBL_NAME)

    with utils.BaseParser(infile, **kwargs) as parser:
        gene_id_idx = parser.get_col_idx(gene_id)
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                ['uniprot_mapping_input_idx'] + [i[0] for i in HEADER] +
                ['bad_gene_id']
            )

        for idx, row in enumerate(parser, 1):
            has_mapping = False
            flag = False

            try:
                for mapping in query_uniprot(rest_client, db_names,
                                             row[gene_id_idx], cache=cache):
                    has_mapping = True
                    yield row + [idx] + mapping + [int(flag)]
            except HTTPError as e:
                if e.response.status_code != 400:
                    raise
                flag = True

            if has_mapping is False:
                yield row + [idx] + ([None] * len(HEADER)) + [int(flag)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_uniprot(rc, db_names, gene_id, cache=True):
    """
    """
    try:
        return _QUERY_CACHE[gene_id]
    except KeyError:
        pass

    ids = rc.get_xrefs_id(
        gene_id, all_levels=1
    )

    mappings = []

    for i in _process_id_matches(ids, db_names):
        mappings.append(i)

    if cache is True:
        _QUERY_CACHE[gene_id] = mappings

    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_id_matches(matches, dbs):
    """Process the uniprot mappings into an output row.
    """
    seen = set()

    for i in matches:
        if i['dbname'] in dbs:
            cur_ids = (i['primary_id'], i['dbname'])

            if cur_ids in seen:
                continue

            seen.add(cur_ids)
            i['synonyms'].insert(0, i['primary_id'])
            for idx, syn_id in enumerate(i['synonyms'], 1):
                i['uniprot_syn_idx'] = idx
                i['uniprot_syn_id'] = syn_id

                processed_row = []
                for header_col, rest_col in HEADER:
                    try:
                        processed_row.append(i[rest_col])
                    except KeyError:
                        processed_row.append(None)
                yield processed_row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        rest = client.Rest(url=args.rest)
        get_uniprot_ids(
            args.infile, rest, outfile=args.outfile, delimiter=args.delimiter,
            verbose=args.verbose, encoding=args.encoding, comment=args.comment,
            tmp_dir=args.tmp_dir, gene_id=args.gene_id, no_mapping=args.no_map,
            include_trembl=args.trembl, cache=not args.no_cache
        )
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Map ensembl gene identifiers to uniprot IDs."
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_rest_arguments(parser, species=False)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, uniprot_id=False)
    parser.add_argument(
        '--no-map', action="store_true",
        help="Include rows that have no mapping in the output file."
    )
    parser.add_argument(
        '--no-cache', action="store_true",
        help="Turn off results caching. Will be slower but use less memory."
    )
    parser.add_argument(
        '--trembl', action="store_true",
        help="Include UniProt/TrEMBL IDs, in addition to UniProt/SwissProt."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

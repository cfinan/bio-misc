"""Flip the effect direction and effect allele based either on allele sort
order or placing the effects in a positive or negative direction. The original
effect orientation is retained in s separate condensed column for a sanity
check.

If the effect allele/other allele columns are available then they are also
flipped if the effect direction is flipped.
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from tqdm import tqdm
import math
import argparse
import sys
import os
import csv
# import pprint as pp


_SCRIPT_NAME = 'bm-effect-flipper'
"""The name of the script that is output when verbose is True (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def effect_flip(infile, outfile=None, chr_name=con.CHR_NAME,
                start_pos=con.START_POS, effect_size=con.EFFECT_SIZE,
                effect_allele=con.EFFECT_ALLELE, other_allele=con.OTHER_ALLELE,
                effect_type='beta', flip_to='sort', verbose=False,
                delimiter=con.DEFAULT_DELIMITER, tmp_dir=None,
                encoding=con.DEFAULT_ENCODING, comment=con.DEFAULT_COMMENT):
    """The main high level API function that takes an input file and generates
    and output file of flipped alleles.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    effect_size : `str`, optional: default: `bio_misc.common.constants.EFFECT_SIZE`
        The name of the effect size column in the header.
    effect_allele : `str`, optional: default: `bio_misc.common.constants.EFFECT_ALLELE`
        The name of the effect allele in the header.
    other_allele : `str`, optional: default: `bio_misc.common.constants.OTHER_ALLELE`
        The name of the other (non effect allele) in the header.
    effect_type : `str`, optional: default: `beta`
        The effect type of the effect size in the file. Values should be
        ``beta``, ``log_or`` or ``or``.
    flip_to : `str`, optional, default: `sort`
        How to orientate the alleles or the effect direction. Possibly choices
        are ``sort`` (place the alleles into sort order with the lowest in the
        sort order being the effect allele), ``pos`` (place the alleles in an
        orientation that makes the effect size positive (or risk increasing for
        ``effect_type='or'``)), ``neg`` (place the alleles in an orientation
        that makes the effect size negative (or risk decreasing for
        ``effect_type='or'``))
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in tqdm(
                yield_effect_flip(infile, chr_name=chr_name,
                                  start_pos=start_pos, effect_size=effect_size,
                                  effect_type=effect_type, flip_to=flip_to,
                                  effect_allele=effect_allele,
                                  other_allele=other_allele,
                                  delimiter=delimiter,
                                  encoding=encoding, tmp_dir=tmp_dir,
                                  comment=comment,  yield_header=True),
                desc="[progress] flipping effects: ",
                unit=" flipped rows",
                disable=not verbose
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_effect_flip(infile, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                      effect_size=con.EFFECT_SIZE, effect_type='beta',
                      effect_allele=con.EFFECT_ALLELE, flip_to='sort',
                      other_allele=con.OTHER_ALLELE, tmp_dir=None,
                      delimiter=con.DEFAULT_DELIMITER, yield_header=True,
                      encoding=con.DEFAULT_ENCODING,
                      comment=con.DEFAULT_COMMENT):
    """Yield rows of flipped alleles.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header.
    effect_size : `str`, optional: default: `bio_misc.common.constants.EFFECT_SIZE`
        The name of the effect size column in the header.
    effect_type : `str`, optional: default: `beta`
        The effect type of the effect size in the file. Values should be
        ``beta``, ``log_or`` or ``or``.
    effect_allele : `str`, optional: default: `bio_misc.common.constants.EFFECT_ALLELE`
        The name of the effect allele in the header.
    flip_to : `str`, optional, default: `sort`
        How to orientate the alleles or the effect direction. Possibly choices
        are ``sort`` (place the alleles into sort order with the lowest in the
        sort order being the effect allele), ``pos`` (place the alleles in an
        orientation that makes the effect size positive (or risk increasing for
        ``effect_type='or'``)), ``neg`` (place the alleles in an orientation
        that makes the effect size negative (or risk decreasing for
        ``effect_type='or'``))
    other_allele : `str`, optional: default: `bio_misc.common.constants.OTHER_ALLELE`
        The name of the other (non effect allele) in the header.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If ``flip_to`` is not one of ``sort``, ``pos`` or ``neg`` or
        ``effect_type`` is not one of ``beta``, ``log_or`` or ``or``.
    """
    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        effect_allele=effect_allele,
        other_allele=other_allele,
        effect_type=effect_type,
        effect_size=effect_size,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    # Get the function calls that will transform the effect type
    transform_func, flip_func, sign_func = get_transform_func(
        effect_type, flip_to
    )

    with utils.GwasParser(infile, **kwargs) as parser:
        if yield_header is True:
            yield parser.header + parser.get_unique_col(
                ['old_alleles', 'has_flipped']
            )

        for idx, row in enumerate(parser, 1):
            chr_name, start_pos, end_pos, effect_allele, other_allele, \
                effect_size = parser.get_site(row)
            new_effect_size, new_effect_allele, new_other_allele, \
                has_flipped = transform_func(
                    effect_size, effect_allele, other_allele, sign_func,
                    flip_func
                )

            # Create a summary columns
            summary = "{0}/{1} ({2})".format(
                effect_allele, other_allele, effect_size
            )
            row[parser.effect_allele_idx] = new_effect_allele
            row[parser.other_allele_idx] = new_other_allele
            row[parser.effect_size_idx] = new_effect_size
            row.extend([summary, int(has_flipped)])
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_transform_func(effect_type, flip_to):
    """Get the transformation functions that will be applied to each row.

    Parameters
    ----------
    effect_type : `str`, optional: default: `beta`
        The effect type of the effect size in the file. Values should be
        ``beta``, ``log_or`` or ``or``.
    flip_to : `str`, optional, default: `sort`
        How to orientate the alleles or the effect direction. Possibly choices
        are ``sort`` (place the alleles into sort order with the lowest in the
        sort order being the effect allele), ``pos`` (place the alleles in an
        orientation that makes the effect size positive (or risk increasing for
        ``effect_type='or'``)), ``neg`` (place the alleles in an orientation
        that makes the effect size negative (or risk decreasing for
        ``effect_type='or'``))

    Returns
    -------
    transform_func : `function`
        A function to control the transformation of the flipping alleles, this
        will either be a function that applies an allele sort, or flips the
        effect direction to positive or negative.
    flip_func : `function`
        A function that will flip the effect direction according to the
        ``effect_type``, such that beta/log_or have a sign change and odds
        rations (or) have the reciprocal taken.
    sign_func : `function`
        A function that will return the sign of the ``effect_size``.
        ``beta``/``log_or`` will return the mathematical sign and odds rations
        (or) will return 1 for risk increasing direction and -1 for risk
        decreasing direction.

    Raises
    ------
    ValueError
        If ``flip_to`` is not one of ``sort``, ``pos`` or ``neg`` or
        ``effect_type`` is not one of ``beta``, ``log_or`` or ``or``.
    """
    transform_func = None
    if flip_to == 'sort':
        transform_func = flip_sort
    elif flip_to == 'pos':
        transform_func = flip_positive
    elif flip_to == 'neg':
        transform_func = flip_negative
    else:
        raise ValueError("unknown 'flip_to' value: '{0}'".format(flip_to))

    sign_func = None
    flip_func = None
    if effect_type in ['beta', 'log_or']:
        sign_func = beta_sign
        flip_func = flip_beta
    elif effect_type == 'or':
        sign_func = or_sign
        flip_func = flip_or
    else:
        raise ValueError(
            "unknown 'effect_type' value: '{0}'".format(effect_type)
        )
    return transform_func, flip_func, sign_func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def or_sign(effect_size):
    """Get the sign of an odds ratio effect

    Parameters
    ----------
    effect_size : `float`
        The effect size to determine the sign.

    Returns
    -------
    effect_sign : `int`
        1 for a risk increasing direction or -1 for a risk decreasing direction
    """
    if effect_size <= 0:
        raise ValueError("bad odds ratio: {0}".format(effect_size))

    if effect_size > 1:
        return 1
    return -1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def beta_sign(effect_size):
    """Get the sign of a beta/log odds ratio effect

    Parameters
    ----------
    effect_size : `float`
        The effect size to determine the sign.

    Returns
    -------
    effect_sign : `int`
        1 for positive direction or -1 for a negative direction
    """
    return math.copysign(1, effect_size)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flip_or(effect_size):
    """Flip an odds ratio effect size.

    Parameters
    ----------
    effect_size : `float`
        The effect size to flip.

    Returns
    -------
    effect_size : `float`
        The reciprocal of the input effect size.
    """
    return 1/effect_size


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flip_beta(effect_size):
    """Flip an odds ratio effect size.

    Parameters
    ----------
    effect_size : `float`
        The effect size to flip.

    Returns
    -------
    effect_size : `float`
        The reciprocal of the input effect size.
    """
    return -1 * effect_size


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flip_sort(effect_size, effect_allele, other_allele, sign_func, flip_func):
    """Flip the effect_allele and other_allele such that they are in sort
    order.

    Parameters
    ----------
    effect_size : `float`
        The effect size to flip (if needed).
    effect_allele : `str`
        The current effect allele.
    other_allele : `str`
        The current other (non-effect) allele.
    sign_func : `function`
        A function to determine the effect sign of the ``effect_size``.
    flip_func : `function`
        A function to flip the ``effect_size`` if needed.

    Notes
    -----
    That is that that the effect allele that is returned is guaranteed to be
    lower in the sort order than the other allele. If this required that the
    effect size is flipped that that will also happen based on the
    ``flip_func`` and the ``sign_func``.
    """
    alleles = [effect_allele, other_allele]
    ordered_alleles = sorted(alleles)
    if alleles != ordered_alleles:
        return (flip_func(effect_size), ordered_alleles[0],
                ordered_alleles[1], True)
    return effect_size, effect_allele, other_allele, False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flip_positive(effect_size, effect_allele, other_allele, sign_func,
                  flip_func):
    """Flip the effect_size such that it is in a positive direction.

    Parameters
    ----------
    effect_size : `float`
        The effect size to flip (if needed).
    effect_allele : `str`
        The current effect allele.
    other_allele : `str`
        The current other (non-effect) allele.
    sign_func : `function`
        A function to determine the effect sign of the ``effect_size``.
    flip_func : `function`
        A function to flip the ``effect_size`` if needed.

    Notes
    -----
    If the ``effect_type`` was an odds ratio then that will mean the
    ``sign_func`` is adjusted such that a risk increasing direction will be
    deemed positive. If the ``effect_size`` is flipped then the
    ``effect_allele`` and ``other_allele`` are flipped accordingly.
    """
    if sign_func(effect_size) == -1:
        return (flip_func(effect_size), other_allele,
                effect_allele, True)
    return effect_size, effect_allele, other_allele, False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def flip_negative(effect_size, effect_allele, other_allele, sign_func,
                  flip_func):
    """Flip the effect_size such that it is in a negative direction.

    Parameters
    ----------
    effect_size : `float`
        The effect size to flip (if needed).
    effect_allele : `str`
        The current effect allele.
    other_allele : `str`
        The current other (non-effect) allele.
    sign_func : `function`
        A function to determine the effect sign of the ``effect_size``.
    flip_func : `function`
        A function to flip the ``effect_size`` if needed.

    Notes
    -----
    If the ``effect_type`` was an odds ratio then that will mean the
    ``sign_func`` is adjusted such that a risk decreasing direction will be
    deemed negative. If the ``effect_size`` is flipped then the
    ``effect_allele`` and ``other_allele`` are flipped accordingly.
    """
    if sign_func(effect_size) == 1:
        return (flip_func(effect_size), other_allele,
                effect_allele, True)
    return effect_size, effect_allele, other_allele, False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    Notes
    -----
    I make this private so it does not show up in the API docs. Private
    functions in python are not enforced (as in java for example) but the
    convention is that anything prefixed with an _ should be considered
    "private" and should not be called by API users.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        effect_flip(args.infile, outfile=args.outfile, chr_name=args.chr_name,
                    start_pos=args.start_pos, effect_size=args.effect_size,
                    effect_allele=args.effect_allele, flip_to=args.flip_to,
                    other_allele=args.other_allele, tmp_dir=args.tmp_dir,
                    effect_type=args.effect_type,  verbose=args.verbose,
                    delimiter=args.delimiter, comment=args.comment,
                    encoding=args.encoding)
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_genomic_coords_arguments(parser, end_pos=False)
    arguments.add_allele_arguments(
        parser, ref=True, alt=True, use_effect_other=True
    )

    parser.add_argument(
        '-S', '--effect-size', type=str, default=con.EFFECT_SIZE,
        help="The name of the effect size column (default: {}).".format(
            con.EFFECT_SIZE
        )
    )
    parser.add_argument(
        '-t', '--effect-type', choices=['beta', 'log_or', 'or'],
        default='beta',
        help="The effect type of the effect size (default: {}).".format(
            'beta'
        )
    )
    parser.add_argument(
        '--flip-to', choices=['sort', 'pos', 'neg'], default='sort',
        help="The flipping strategy, either 'sort' (alleles in sort order),"
        " 'pos' (positive effect size), 'neg' (negative effect size) "
        "(default: {}).".format(
            'sort'
        )
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

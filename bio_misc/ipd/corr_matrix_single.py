"""Creating a correlation matrix using a single process and possibly a single
pairwise comparison or match comparisons
"""
from bio_misc.ipd import common
from tqdm import tqdm
import numpy as np
import numpy.ma as ma
import multiprocessing as mp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RunCorrMatrix(common._BaseRunCorrMatrix):
    """Generate pairwise correlations and numbers of observations for a
    specific set of variants for a single cohort.

    This implementation queries pairs of variants at a time and is very slow.
    So is not recommended. However, it is handy as a baseline implementation
    to compare against.

    Parameters
    ----------
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run_cohort(self, invars, cohort, verbose=False):
        """Generate a correlation matrix for a single cohort.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        cohort : `bio_misc.ipd.common.Cohort`
            Description of the cohort that will be used for as the source data
            for the correlation matrix.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Make sure the variants that have been supplied are unique
        invars = common.unique_variants(invars)

        corr_matrix, nobs_matrix = super().run_cohort(
            invars, cohort, verbose=verbose
        )
        cohort.open()

        try:
            pbar = tqdm(
                total=(len(invars) * len(invars)),
                desc=f"[info] running cohort {cohort.name}",
                unit=" correlations",
                disable=common.verbose_disable(verbose)
            )
            for sidx in range(len(invars)):
                index_var = invars[sidx]
                try:
                    index_dose, flipped = common.get_variant_dosage(
                        cohort.reader,
                        cohort.zero_func(index_var.chr_name),
                        index_var.start_pos,
                        index_var.effect_allele,
                        index_var.other_allele
                    )
                    pbar.update(1)
                except IndexError:
                    # No LD data
                    pbar.update(2*(len(invars) - sidx - 1))
                    continue

                for tidx in range(sidx, len(invars)):
                    test_var = invars[tidx]
                    pbar.update(2*bool(tidx - sidx))
                    try:
                        test_dose, flipped = common.get_variant_dosage(
                            cohort.reader,
                            cohort.zero_func(test_var.chr_name),
                            test_var.start_pos,
                            test_var.effect_allele,
                            test_var.other_allele
                        )
                    except IndexError:
                        # No LD data
                        continue

                    cc, nobs = common.block_corrcoef(
                        ma.masked_invalid([index_dose]),
                        ma.masked_invalid([test_dose])
                    )
                    # print(cc, nobs)
                    corr_matrix[sidx, tidx] = cc[0, 0]
                    corr_matrix[tidx, sidx] = cc[0, 0]
                    nobs_matrix[sidx, tidx] = nobs[0, 0]
                    nobs_matrix[tidx, sidx] = nobs[0, 0]
        finally:
            pbar.close()
            cohort.close()
        return corr_matrix, nobs_matrix


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RunBatchCorrMatrix(common._BaseRunCorrMatrix):
    """Generate pairwise correlations and numbers of observations for a
    specific set of variants.

    This implementation queries variants in batches in a single process.

    Parameters
    ----------
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run_cohort(self, invars, cohort, batch_size=50, verbose=False):
        """Generate a correlation matrix for a single cohort.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        cohort : `bio_misc.ipd.common.Cohort`
            Description of the cohort that will be used for as the source data
            for the correlation matrix.
        batch_size : `int`, optional, default: `50`
            The number of variant allele counts in each batch that will be
            stored in memory at one time. So the total number in each
            comparison will be twice this. This can be used to increased speed
            but at the expense of additional memory usage. The memory usage
            will also depend on the sample count in your reference population.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Make sure the variants that have been supplied are unique
        invars = common.unique_variants(invars)

        corr_matrix, nobs_matrix = super().run_cohort(
            invars, cohort, verbose=verbose
        )
        cohort.open()
        try:
            pbar = tqdm(
                total=(len(invars) * len(invars)),
                desc=f"[info] running cohort {cohort.name}",
                unit=" correlations",
                disable=common.verbose_disable(verbose)
            )
            for r in range(0, len(invars), batch_size):
                rend = r+batch_size
                row_dosage = common.get_variant_batch_dosage(
                    cohort, invars[r:rend]
                )
                for c in range(0, r+batch_size, batch_size):
                    cend = c+batch_size
                    col_dosage = common.get_variant_batch_dosage(
                        cohort, invars[c:cend]
                    )

                    cc, nobs = common.block_corrcoef(
                        row_dosage,
                        col_dosage
                    )
                    pbar.update((1+bool(c - r))*(cc.shape[0]*cc.shape[1]))
                    corr_matrix[r:rend, c:cend] = cc
                    corr_matrix[c:cend, r:rend] = cc.T
                    nobs_matrix[r:rend, c:cend] = nobs
                    nobs_matrix[c:cend, r:rend] = nobs.T
        finally:
            pbar.close()
            cohort.close()
        return corr_matrix, nobs_matrix


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RunParallelCorrMatrix(common._BaseRunCorrMatrix):
    """Generate pairwise correlations and numbers of observations for a
    specific set of variants.

    This implementation queries variants in batches and is designed to run in
    parallel. This increases performance substantially.

    Parameters
    ----------
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run_cohort(self, invars, cohort, batch_size=50, processes=2,
                   verbose=False):
        """Generate a correlation matrix for a single cohort.

        Parameters
        ----------
        invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
        cohort : `bio_misc.ipd.common.Cohort`
            Description of the cohort that will be used for as the source data
            for the correlation matrix.
        batch_size : `int`, optional, default: `1`
            The number of variant allele counts in each batch that will be
            stored in memory at one time. So the total number in each
            comparison will be twice this. This can be used to increased speed
            but at the expense of additional memory usage. The memory usage
            will also depend on the sample count in your reference population.
        verbose : `bool`, optional, default: `False`
            Report progress. True will enable progress
            monitoring.

        Returns
        -------
        corr_matrix : `numpy.array`
            The correlation matrix. The full span of the pairwise correlations,
            this will have the shape ``(len(invars)), len(invars))`` and
            missing data will have the value ``numpy.nan``. The row and column
        nobs_matrix : `numpy.array`
            The number of observations matrix. The full span of the pairwise
            number of observations, this will have the shape ``(len(invar)),
            len(invars))``. Missing data will have the value ``0``.
        """
        # Make sure the variants that have been supplied are unique
        invars = common.unique_variants(invars)
        queue = mp.JoinableQueue()

        corr_matrix, nobs_matrix = super().run_cohort(
            invars, cohort, verbose=verbose
        )
        process_combs = common.get_combs(len(invars), batch_size, processes)
        total = (len(invars) * len(invars))
        try:
            pbar = tqdm(
                total=total,
                desc=f"[info] running cohort {cohort.name}",
                unit=" correlations",
                disable=common.verbose_disable(verbose)
            )
            # Initialise the processes
            run_processes = []
            for i in range(len(process_combs)):
                p = mp.Process(
                    target=_run_batch_process,
                    args=(i, queue, invars, process_combs[i], cohort),
                    kwargs=dict(dtype=self.dtype)
                )
                p.start()
                run_processes.append(p)

            ncorrs = 0
            while ncorrs < total:
                r, c, cc, nobs = queue.get()
                rstart, rend = r
                cstart, cend = c
                corr_matrix[rstart:rend, cstart:cend] = cc
                corr_matrix[cstart:cend, rstart:rend] = cc.T
                nobs_matrix[rstart:rend, cstart:cend] = nobs
                nobs_matrix[cstart:cend, rstart:rend] = nobs.T
                updates = (1+bool(rstart - cstart))*(cc.shape[0]*cc.shape[1])
                pbar.update(updates)
                ncorrs += updates
                queue.task_done()
        finally:
            pbar.close()
            queue.join()

        return corr_matrix, nobs_matrix


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _run_batch_process(proc_idx, queue, invars, combs, cohort,
                       dtype=np.float64):
    """
    """
    # print(f"[info] {proc_idx} combs {len(combs)}")
    cohort.open()

    try:
        for r, c in combs:
            rstart, rend = r
            cstart, cend = c
            row_dosage = common.get_variant_batch_dosage(
                cohort, invars[rstart:rend]
            )
            col_dosage = common.get_variant_batch_dosage(
                cohort, invars[cstart:cend]
            )
            try:
                cc, nobs = common.block_corrcoef(row_dosage, col_dosage)
            except Exception as e:
                print(f"[info] exception {proc_idx} {e}!!!")
                raise

            queue.put((r, c, cc, nobs))
    finally:
        cohort.close()

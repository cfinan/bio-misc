""""Extract IPD allele counts (dosage) as delimited lines. The input file must
have a chromosome, start position, reference allele and alternate allele
columns that prove the data for the variants.
"""
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.ipd import common
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.format import uni_id
from tqdm import tqdm
from genomic_config import genomic_config
import argparse
import sys
import os
import csv
# import pprint as pp


_DESC = __doc__
"""The program description also used for argparse. (`str`)
"""
_SCRIPT_NAME = 'bm-ipd-allele-counts'
"""The name of the script that is output when verbose is True (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    For API usage see
    ``bio_misc.ipd.extract_allele_counts.extract_allele_counts`` and
    ``bio_misc.ipd.extract_allele_counts.yield_allele_counts``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        config = genomic_config.open(config_file=args.config)
        extract_allele_counts(args.infile, config, args.cohort, args.format,
                              assembly=args.assembly, outfile=args.outfile,
                              chr_name=args.chr_name, start_pos=args.start_pos,
                              ref_allele=args.ref_allele,
                              alt_allele=args.alt_allele, tmp_dir=args.tmp_dir,
                              verbose=args.verbose, delimiter=args.delimiter,
                              comment=args.comment, encoding=args.encoding)
        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")
    finally:
        config.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_allele_counts(infile, config, cohort, file_format, outfile=None,
                          chr_name=con.CHR_NAME, start_pos=con.START_POS,
                          ref_allele='ref_allele', alt_allele='alt_allele',
                          verbose=False, delimiter=con.DEFAULT_DELIMITER,
                          tmp_dir=None, encoding=con.DEFAULT_ENCODING,
                          comment=con.DEFAULT_COMMENT, assembly="GRCh38"):
    """The main high level API function.

    This takes an input file and generates and output file of dosages which may
    have been flipped to represent the input file reference allele.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohort : `str`
        The cohort name in the config file.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of ``infile``.
    ref_allele : `str`, optional: default: `ref_allele`
        The name of the reference allele column in the header of ``infile``.
    alt_allele : `str`, optional: default: `alt_allele`
        The name of the alternate allele column in the header of ``infile``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    assembly : `str`, optional: default: `GRCh38`
        The genome assembly in the config file.

    Yields
    ------
    allele_dosage_row : `list` of `str` and `float`
        A row of allele dosages. A row represents dosages for a single genomic
        variant. The first three columns are uni_id, ref_allele and is_flipped
        (a boolean indicator to indicate if the reference allele was flipped to
        match the input file). The remaining columns are allele dosages for
        each sample. The first row yielded will be the header row.
    """
    regions = []
    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    # The allele count extraction class takes variants
    with utils.CoordsParser(infile, **kwargs) as parser:
        ref_allele_idx = parser.get_col_idx(ref_allele)
        alt_allele_idx = parser.get_col_idx(alt_allele)

        for idx, row in enumerate(parser, 1):
            # if row[parser.chr_name_idx] in ['12', '17', '20']:
            uid = uni_id.make_uni_id(
                row[parser.chr_name_idx], row[parser.start_pos_idx],
                row[ref_allele_idx], row[alt_allele_idx]
            )
            regions.append(
                (row[parser.chr_name_idx], row[parser.start_pos_idx],
                 row[ref_allele_idx], row[alt_allele_idx], uid)
            )

    regions.sort(key=lambda x: (x[0], x[1]))

    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        idx = 0
        for row in tqdm(
                yield_allele_counts(regions, config, cohort, file_format,
                                    chr_name=chr_name, start_pos=start_pos,
                                    ref_allele=ref_allele,
                                    alt_allele=alt_allele, delimiter=delimiter,
                                    encoding=encoding, tmp_dir=tmp_dir,
                                    comment=comment,  yield_header=True,
                                    assembly=assembly),
                desc="[progress] extracting allele counts: ",
                unit=" variants",
                disable=not verbose,
                total=len(regions)
        ):
            chr_name, start_pos, ref_allele, alt_allele, is_flipped, dosage = \
                row

            try:
                uid = uni_id.make_uni_id(
                    chr_name, start_pos, ref_allele, alt_allele
                )
            except TypeError:
                if idx > 0:
                    raise
                writer.writerow(
                    ['uni_id', 'ref_allele', 'is_flipped'] + dosage.tolist()
                )
                continue

            writer.writerow(
                [uid, ref_allele, int(is_flipped)] + dosage.tolist()
            )
            idx += 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_allele_counts(regions, config, cohort, file_format,
                        chr_name=con.CHR_NAME, start_pos=con.START_POS,
                        ref_allele='ref_allele', alt_allele='alt_allele',
                        tmp_dir=None, delimiter=con.DEFAULT_DELIMITER,
                        yield_header=True, encoding=con.DEFAULT_ENCODING,
                        comment=con.DEFAULT_COMMENT, assembly="GRCh38"):
    """The slightly lower level API function.

    This takes an input regions and generates and yields rows of allele
    dosages which may have been flipped to represent the input file reference
    allele.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    config : `genomic_config.ini_config.IniConfig`
        The configuration file object that will provide the files based on
        genomic location.
    cohort : `str`
        The cohort name in the config file.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of ``infile``.
    ref_allele : `str`, optional: default: `ref_allele`
        The name of the reference allele column in the header of ``infile``.
    alt_allele : `str`, optional: default: `alt_allele`
        The name of the alternate allele column in the header of ``infile``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    assembly : `str`, optional: default: `GRCh38`
        The genome assembly in the config file.

    Yields
    ------
    allele_dosage_row : `list` of `str` and `float`
        A row of allele dosages. A row represents dosages for a single genomic
        variant. The first three columns are uni_id, ref_allele and is_flipped
        (a boolean indicator to indicate if the reference allele was flipped to
        match the input file). The remaining columns are allele dosages for
        each sample. The first row yielded will be the header row.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    ValueError
        If ``flip_to`` is not one of ``sort``, ``pos`` or ``neg`` or
        ``effect_type`` is not one of ``beta``, ``log_or`` or ``or``.
    """
    if file_format not in common.FILE_FORMATS:
        raise ValueError("unknown file format: '{0}'".format(file_format))

    read_func = common.read_vcf
    if file_format == 'bgen':
        read_func = common.read_bgen

    provider = config.get_cohort_region_provider(
        common.SPECIES, assembly, cohort, file_format
    )

    for row in read_func(provider, regions):
        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser = common.init_cmd_args(parser)
    arguments.add_allele_arguments(
        parser, ref=True, alt=True, use_effect_other=False
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""An LD clumping program. The input GWAS file should have a
chromosome, start position, effect allele, other allele columns. The
values for these can also be supplied from the command line. This is low
memory and only rows to be clumped will be held in memory at one time.

This will only clump within a chromosome and not across chromosomes. It
also relies on genomic-config package being present along with a valid
config file.
"""
# from bio_misc import __version__, __name__ as pkg_name
from bio_misc.ipd import common
from bio_misc.transforms.log_pvalue import log_transform
from bio_misc.common import (
    utils,
    # constants as con,
    # arguments,
    # log
)
# from bio_misc.format import uni_id
from tqdm import tqdm
# from genomic_config import genomic_config
from multi_join import join
# import multiprocessing as mp
# import numpy.ma as ma
import sqlite3
import stdopen
# import numpy as np
# import argparse
# import sys
import os
# import math
import csv
import gzip
# import warnings
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseClumpHandler(object):
    """Base class to provide batches of index variant/clump candidates to an
    LD clumping algorithm, do not use directly.

    Parameters
    ----------
    infile : `str`
        The input file with the required columns.
    *args
        Ignored.
    pvalue_filter : `float`, optional: default: `5E-08`
        Only variants below this cut point will be considered for clumping
        Please note, even if ``pvalue_logged`` is True, supply this value
        untransformed.
    pvalue_logged : `bool`, optional: default: `False`
        Is the pvalue data in the ``pvalue`` column -log10 transformed.
    delimiter : `str`, optional, default `\t`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `utf-8`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `#`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    flank : `int`, optional, default: `1000000`
        The flanking region around an index variant that defines the flanking
        clump boundaries
    verbose : `bool`, optional, default: `False`
        Report progress.
    chr_name : `str`, optional: default: `chr_name`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `start_pos`
        The name of the start position column in the header of ``infile``.
    effect_allele : `str`, optional: default: `effect_allele`
        The name of the effect allele column in the header of ``infile``.
    other_allele : `str`, optional: default: `other_allele`
        The name of the other allele column in the header of ``infile``.
    pvalue : `str`, optional: default: `pvalue`
        The name of the pvalue column in the header of ``infile``.
    tmpdir : `str`, optional, default: `NoneType`
        The tmp location. The tmp directory is only used if input is from
        STDIN, there fore a copy of the input file will need to be kept. A
        ``NoneType`` value means that the system temp location is used.
    **kwargs
        Ignored.
    """
    VAR_ID_COL = 'variant_idx'
    IS_INDEX_COL = 'is_index'
    IS_CLUMPED_COL = 'is_clumped'
    IS_UNKNOWN_COL = 'is_unknown'
    CLUMP_COLS = [VAR_ID_COL, IS_INDEX_COL, IS_CLUMPED_COL, IS_UNKNOWN_COL]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, *args, pvalue_filter=1.0, pvalue_logged=False,
                 delimiter="\t", encoding="utf-8", comment="#", verbose=False,
                 chr_name='chr_name', start_pos='start_pos', clump_flank=1E06,
                 effect_allele='effect_allele', other_allele='other_allele',
                 pvalue='pvalue', tmpdir=None, **kwargs):
        self.infile = infile
        self.delimiter = delimiter
        self.encoding = encoding
        self.comment = comment
        self.verbose = verbose
        self.pvalue_logged = pvalue_logged
        self.flank = clump_flank
        self.pvalue_filter = pvalue_filter
        self.tmpdir = tmpdir

        # This is the value of the variant.is_unknown_bits that indicates
        # that it is missing in all of the cohorts. This allows the handler
        # to skip these variants so they will not be repeatedly queried.
        # This default is set so that the handler will not skip any potentially
        # missing variants
        self.missing_value = -1

        self.log_pvalue_filter = log_transform(self.pvalue_filter)
        if self.log_pvalue_filter is None or self.log_pvalue_filter < 0:
            raise ValueError(
                "bad pvalue cutoff: -log10(pvalue)={self.log_pvalue_filter}"
            )

        self.chr_name = chr_name
        self.start_pos = start_pos
        self.effect_allele = effect_allele
        self.other_allele = other_allele
        self.pvalue = pvalue

        self.is_open = False
        self.nerrors = 0
        self.nexcludes = 0
        self.load_idx = 0
        self._updated = False
        self._tmp_infile = None
        self.inheader = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def is_updated(self):
        """Return the state of the handler, has it been updated or is it in the
        original state.
        """
        return self._updated

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def filtered_input(self):
        """Return the path to a file containing all the rows filtered from the
        input file. This is a gzip compressed file.
        """
        return self._tmp_infile

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_missing_value(self, cohorts):
        """Set the missing value to the cohorts that we have. The default is
        value that the object is initialised with will not skip any unknowns.

        Parameters
        ----------
        cohort : `list` of `bio_misc.ipd.common.Cohort`
            The cohorts to use to set the missing value.
        """
        if len(cohorts) == 0:
            raise ValueError("no cohorts")

        self.missing_value = 0
        for i in cohorts:
            self.missing_value |= i.bits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the handler, this will load infile. This is called on
        __enter__.
        """
        if self.is_open is True:
            raise RuntimeError("already open")
        self.is_open = True
        self.load_data()
        self.is_loaded = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the handler, this is called on __exit__.
        """
        if self._tmp_infile is not None:
            os.unlink(self._tmp_infile)

        self.is_loaded = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_data(self):
        """Filter the input file for variants that are below the cut off point.

        This filters all it for all the variants that are more significant or
        equal to the significance cut filter ``pvalue_filter``.

        Raises
        ------
        IOError
            If the handler is not open.

        Notes
        -----
        You should not need to call this directly as it is called by
        ``open()``.
        """
        if self.is_open is False:
            raise IOError("handler not open")

        pval_trans_func = float
        if self.pvalue_logged is False:
            pval_trans_func = log_transform

        open_method = open
        if self.infile is not None and self.infile != '-' and \
           utils.is_gzip(self.infile):
            open_method = gzip.open

        self._tmp_infile = utils.get_tmp_file(dir=self.tmpdir)
        try:
            tmpout = gzip.open(self._tmp_infile, 'wt')
            with stdopen.open(
                    self.infile, method=open_method, encoding=self.encoding
            ) as inobj:
                row = self.comment
                while row.startswith(self.comment):
                    row = next(inobj)

                self.inheader = row.strip().split(self.delimiter)
                reader = csv.DictReader(
                    inobj, fieldnames=self.inheader, delimiter=self.delimiter
                )
                writer = csv.DictWriter(
                    tmpout, fieldnames=[self.VAR_ID_COL] + self.inheader,
                    delimiter=self.delimiter
                )
                writer.writeheader()

                for idx, row in enumerate(
                        tqdm(reader,
                             desc="[info] filtering input file...",
                             unit=" rows",
                             disable=not self.verbose)
                ):
                    self.load_idx = idx
                    row[self.pvalue] = pval_trans_func(row[self.pvalue])
                    if row[self.pvalue] is None:
                        self.nerrors += 1
                        continue
                    if row[self.pvalue] < self.log_pvalue_filter:
                        self.nexcludes += 1
                        continue
                    row[self.start_pos] = int(row[self.start_pos])
                    self.append_row(row)
                    row[self.VAR_ID_COL] = idx
                    writer.writerow(row)
            tmpout.close()
        except Exception:
            tmpout.close()
            os.unlink(self._tmp_infile)
            self._tmp_infile = None
            raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def append_row(self, row):
        """Append a row to the handler's internal data

        Parameters
        ----------
        row : `Any`
            The row to append to the handler data source. Technically could be
            anything but in most cases will be a ``dict``
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_index(self):
        """Get the next index variant in the dataset.

        Returns
        -------
        index_variant : `bio_misc.ipd.common.ClumpVariant`
            An index variant to be processed.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_clump_batch(self, index_variant):
        """Get the next batch of variants to be clumped.

        Yields
        ------
        variant : `list` of `bio_misc.ipd.common.ClumpVariant`
            Variants to be processed, the index variant is not included.
            one.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update(self, variants):
        """Update the status of the variants prior to the next index being
        called.

        Parameters
        ----------
        variants : `list` of `bio_misc.ipd.common.ClumpVariant`
            A variants to be updated.
        """
        self._updated = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset(self):
        """Reset the handler to the unclumped state.
        """
        self._updated = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_infile_join(self):
        """Provide a join iterator for the input file data.

        Returns
        -------
        infile_join : `multi_join.join.JoinIterator`
            An iterator to use in the join with the clump data.
        """
        # Use a private generator function that is wrapped in an iterator
        # this will provide the variant id at element [0] in a list
        def _infile():
            with gzip.open(self._tmp_infile, 'rt') as infile:
                reader = csv.DictReader(infile, delimiter=self.delimiter)
                for row in reader:
                    row[self.start_pos] = int(row[self.start_pos])
                    row[self.VAR_ID_COL] = int(row[self.VAR_ID_COL])
                    yield [row[self.VAR_ID_COL], row]
        return join.JoinIterator(iter(_infile()), key=(0,))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_clump_join(self):
        """Provide a join iterator for the clumped data. This should be
        overridden in child classes.

        Raises
        ------
        NotImplementedError
            Need to override this.

        Notes
        -----
        The join iterator should be of type `multi_join.join.JoinIterator` and
        provide the variant_idx data to join on.
        """
        raise NotImplementedError("need to override")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def output(self):
        """Output the clumped data along side the input file data.

        Yields
        ------
        clump_data : `dict`
            The keys of the dict are the input file columns and additionally
            keys for the clumped data.
        """
        file_join = self.get_infile_join()
        clump_join = self.get_clump_join()
        joiner = join.Join(file_join, clump_join)
        for j in joiner:
            j = join.flatten_join_block(j)
            if len(j) > 2:
                raise ValueError("unexpected join block this is a bug")
            outrow = j[0][1][1]
            clump_row = j[1][1][1]
            outrow['is_index'] = clump_row['is_index']
            outrow['is_clumped'] = clump_row['is_clumped']
            outrow['is_unknown'] = clump_row['is_unknown']
            yield outrow


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FileClumpHandler(_BaseClumpHandler):
    """Provides batches of index variant/clump candidates to an LD
    clumping algorithm.

    This loads all filtered variants into memory, so is suitable for small
    jobs.

    Parameters
    ----------
    *args
        Any arguments to ``bio_misc.ipd.ld_clump._BaseClumpHandler``.
    **kwargs
        Any keyword arguments to ``bio_misc.ipd.ld_clump._BaseClumpHandler``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._input_variants = []
        self.chromosomes = dict()
        self._index_var_idx = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the handler and delete the variants.
        """
        self._input_variants = []
        super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_data(self, **kwargs):
        """Filter the input file for variants that are below the cut off point.

        This filters all it for all the variants that are more significant or
        equal to the significance cut filter ``pvalue_filter``.

        Raises
        ------
        IOError
            If the handler is not open.

        Notes
        -----
        You should not need to call this directly as it is called by
        ``open()``.
        """
        super().load_data(**kwargs)

        # sort the way you want them, chromsomes, pvalue (desc),
        # start_pos (asc)
        self._input_variants.sort(key=lambda x: x.start_pos)
        self._input_variants.sort(key=lambda x: x.pvalue, reverse=True)
        self._input_variants.sort(key=lambda x: x.chr_name)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def append_row(self, row):
        """Append a row to the handler's internal data

        Parameters
        ----------
        row : `Any`
            The row to append to the handler data source. Technically could be
            anything but in most cases will be a ``dict``
        """
        v = common.ClumpVariant(
            row[self.chr_name],
            row[self.start_pos],
            row[self.effect_allele],
            row[self.other_allele],
            pvalue=row[self.pvalue],
            pvalue_logged=True
        )
        try:
            self.chromosomes[v.chr_name] += 1
        except KeyError:
            self.chromosomes[v.chr_name] = 1

        self._input_variants.append(v)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_index(self):
        """Get the next index variant in the dataset.

        Returns
        -------
        index_variant : `bio_misc.ipd.common.ClumpVariant`
            An index variant to be processed.

        Raises
        ------
        IndexError
            When there are no more index variants, essentially everything is
            finished as far as the handler is concerned.
        """
        idx = self._input_variants[self._index_var_idx]
        self._index_var_idx += 1

        while (idx.is_clumped | idx.is_index |
               (idx.is_unknown_bits == self.missing_value)) is True:
            idx = self._input_variants[self._index_var_idx]
            self._index_var_idx += 1

        return idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_clump_batch(self, index_variant):
        """Get the next batch of variants to be clumped.

        Returns
        -------
        clump_batch : `list` of `bio_misc.ipd.common.ClumpVariant`
            Variants to be processed, the index variant is not included.
        """
        clump_batch = []
        start = index_variant.start_pos - self.flank
        end = index_variant.start_pos + self.flank

        for i in range(self._index_var_idx+1, len(self._input_variants), 1):
            v = self._input_variants[i]
            if (v.is_clumped | v.is_index |
                (v.is_unknown_bits == self.missing_value)) is True:
                continue

            if v.chr_name == index_variant.chr_name and v.start_pos >= start \
               and v.start_pos <= end:
                clump_batch.append(v)
            elif v.chr_name != index_variant.chr_name:
                # No point looping any more as we have passed through the
                # chromosome we are interested in (remember we are sorted on
                # chr first)
                break
        return clump_batch

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update(self, variants):
        """Update the status of the variants prior to the next index being
        called.

        Parameters
        ----------
        variants : `list` of `bio_misc.ipd.common.ClumpVariant`
            A variants to be updated.
        """
        super().update(variants)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chromosomes(self):
        """Get a unique set of chromosomes present in the dataset.

        Returns
        -------
        uniq_chr_names : `list` of `str`
            A unique set of chromosome names.
        """
        return list(self.chromosomes.keys())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_count(self):
        """Get the total count of variants to be clumped.

        Returns
        -------
        total_variants : `int`
            A count of all filtered variants.
        """
        return sum([i for i in self.chromosomes.values()])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset(self):
        """Reset the handler to it's default unclumped state.
        """
        if self._is_updated is True:
            self._index_var_idx = 0
            for i in self._input_variants:
                i.is_clumped, i.is_index, i.is_unknown = False, False, False


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DbClumpHandler(_BaseClumpHandler):
    """Provides batches of index variant/clump candidates to an LD
    clumping algorithm.

    This loads all filtered variants into an SQLite database, the database
    could be disk based or memory based. So is potentially suitable for larger
    jobs.

    Parameters
    ----------
    infile : `str`
        The input file with the required columns.
    database_path : `str`
        The path to the output database file, must not exist. If it is
        ``:memory:``, then an in-memory database will be used.
    insert_every : `int`, optional, default: `10000`
        When loading commit to the database every ``insert_every`` rows.
    overwrite : `bool`, optional, default: `False`
        If the database file already exists then the default action is to raise
        an error. If this is set to ``True`` then the existing file is deleted
        and re-created.
    **kwargs
        Any keyword arguments to ``bio_misc.ipd.ld_clump._BaseClumpHandler``.
    """
    CREATE_TABLE_SQL = """
    CREATE TABLE indata (
    variant_idx INTEGER PRIMARY KEY,
    chr_name VARCHAR(50) NOT NULL,
    start_pos NOT NULL,
    end_pos NOT NULL,
    effect_allele VARCHAR(50) NOT NULL,
    other_allele VARCHAR(50) NOT NULL,
    pvalue FLOAT NOT NULL,
    is_index INT NOT NULL DEFAULT 0,
    is_unknown INT NOT NULL DEFAULT 0,
    is_clumped INT NOT NULL DEFAULT 0
    )
    """

    INSERT_SQL = """
    INSERT INTO indata
    (variant_idx, chr_name, start_pos, end_pos,
    effect_allele, other_allele, pvalue)
    VALUES
    (?, ?, ?, ?, ?, ?, ?)
    """

    INDEX_VARIANT_SQL = """
    SELECT variant_idx, chr_name, start_pos, effect_allele, other_allele,
           pvalue, is_unknown
    FROM indata
    WHERE pvalue = (
    SELECT MAX(pvalue)
    FROM indata
    WHERE chr_name=?
    AND is_index=0
    AND is_clumped=0
    AND is_unknown != ?
    )
    AND chr_name = ?
    AND is_index=0
    AND is_clumped=0
    AND is_unknown != ?
    """

    UPDATE_VARIANT_SQL = """
    UPDATE indata
    SET is_index = ?, is_unknown = ?, is_clumped = ?
    WHERE variant_idx = ?
    """

    CLUMP_BATCH_SQL = """
    SELECT variant_idx, chr_name, start_pos, effect_allele, other_allele,
           is_unknown
    FROM indata
    WHERE chr_name = ?
    AND start_pos >= ?
    AND start_pos <= ?
    AND is_index=0
    AND is_clumped=0
    AND is_unknown != ?
    AND variant_idx != ?
    """

    POS_INDEX_SQL = "CREATE INDEX pos_idx ON indata (chr_name, start_pos)"
    PVAL_INDEX_SQL = "CREATE INDEX pval_idx ON indata (pvalue)"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, database_path, insert_every=10000,
                 overwrite=False, **kwargs):
        super().__init__(infile, **kwargs)
        self._database_path = database_path
        self._overwrite = overwrite
        self.insert_every = insert_every

        self._in_memory = False
        self.conn = None
        self.cur = None
        self.is_init = False
        self._data_buffer = list()
        self._chr_order = list()
        self._cur_chr_name = None
        self._proc_chr_name = list()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the handler and delete the variants.
        """
        self.cur.close()
        self.conn.close()
        super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_data(self):
        """Filter the input file for variants that are below the cut off point.

        This filters all it for all the variants that are more significant or
        equal to the significance cut filter ``pvalue_filter``.

        Raises
        ------
        IOError
            If the handler is not open.

        Notes
        -----
        You should not need to call this directly as it is called by
        ``open()``.
        """
        self.init_db()
        super().load_data()
        self.flush_data_buffer()
        self.cur.execute(self.POS_INDEX_SQL)
        self.cur.execute(self.PVAL_INDEX_SQL)
        # Initialise a starting chromosome to process
        self.next_chromosome()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def flush_data_buffer(self):
        """Flush the database buffer into the database (load a batch of rows).
        """
        if len(self._data_buffer) > 0:
            self.cur.executemany(self.INSERT_SQL, self._data_buffer)
            self._data_buffer = list()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_chromosome_order(self):
        """Get the chromosomes from the database and order for processing. This
        determines what the chromosome is for index processing.

        Note that all current and previous chromosomes are removed from this
        list, so it just contains anything that is in the database that has not
        been processed.
        """
        # Get all the chromosomes in the database
        chr_names = self.get_chromosomes()

        # Remove any current/previously processed chromosomes, so what is left
        # is stuff that has not been processed
        for i in [self._cur_chr_name] + self._proc_chr_name:
            try:
                # Attempt to remove
                chr_names.pop(chr_names.index(i))
            except ValueError:
                # Not in the list - not sure why this would get raised
                pass
        # Set what is left
        self._chr_order = chr_names

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_chromosome(self):
        """Get the next chromosome in the processing sequence.

        Returns
        -------
        next_chr : `str`
            The next chromosome to process (also stored internally).

        Raises
        ------
        IndexError
            If there are no more chromosomes to process.
        """
        # No chromosomes, might not be initialised
        if len(self._chr_order) == 0:
            self.set_chromosome_order()

        self._proc_chr_name.append(self._cur_chr_name)
        # There are some chromosomes, so pop
        if len(self._chr_order) > 0:
            self._cur_chr_name = self._chr_order.pop(0)
            return self._cur_chr_name
        # Nothing more to do
        raise IndexError("no more chromosomes")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def init_db(self):
        """Initialise the database file.
        """
        if self.is_init is True:
            raise RuntimeError("database already initialised")

        if self._database_path != ":memory:":
            self._in_memory = False
            try:
                # Make sure the database file does not already exist as this
                # would screw things up
                open(self._database_path).close()

                if self._overwrite is True:
                    os.unlink(self._database_path)
                else:
                    raise FileExistsError(
                        f"database already exists: {self._database_path}"
                    )
            except FileNotFoundError:
                pass
        else:
            self._in_memory = True

        self.conn = sqlite3.connect(self._database_path)
        self.cur = self.conn.cursor()

        # Create table
        self.cur.execute(
            self.CREATE_TABLE_SQL
        )
        self.is_init = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def append_row(self, row):
        """Append a row to the handler's internal data.

        Parameters
        ----------
        row : `Any`
            The row to append to the handler data source. Technically could be
            anything but in most cases will be a ``dict``
        """
        self._data_buffer.append(
            (
                self.load_idx,
                row[self.chr_name],
                row[self.start_pos],
                row[self.start_pos] + len(row[self.effect_allele]) - 1,
                row[self.effect_allele],
                row[self.other_allele],
                row[self.pvalue]
            )
        )

        if len(self._data_buffer) == self.insert_every:
            self.flush_data_buffer()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_index(self):
        """Get the next index variant in the dataset.

        Returns
        -------
        index_variant : `bio_misc.ipd.common.ClumpVariant`
            An index variant to be processed.
        """
        while True:
            self.cur.execute(
                self.INDEX_VARIANT_SQL,
                (
                    self._cur_chr_name, self.missing_value,
                    self._cur_chr_name, self.missing_value
                )
            )
            index_var = self.cur.fetchall()
            if index_var is None or len(index_var) == 0:
                self.next_chromosome()
            else:
                index_var = index_var[0]
                index_var_obj = common.ClumpVariant(
                    index_var[1], index_var[2],
                    index_var[3], index_var[4],
                    pvalue=index_var[5]
                )
                index_var_obj.variant_idx = index_var[0]
                index_var_obj.set_unknown(index_var[6])
                return index_var_obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_clump_batch(self, index_variant):
        """Get the next batch of variants to be clumped.

        Returns
        -------
        clump_batch : `list` of `bio_misc.ipd.common.ClumpVariant`
            Variants to be processed, the index variant is not included.
        """
        clump_batch = []
        self.cur.execute(
            self.CLUMP_BATCH_SQL,
            (
                index_variant.chr_name,
                index_variant.start_pos - self.flank,
                index_variant.start_pos + self.flank,
                self.missing_value,
                index_variant.variant_idx
            )
        )
        for i in self.cur:
            test_var_obj = common.ClumpVariant(
                i[1], i[2], i[3], i[4]
            )
            test_var_obj.variant_idx = i[0]
            test_var_obj.set_unknown(i[5])
            clump_batch.append(test_var_obj)
        return clump_batch

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update(self, variants):
        """Update the status of the variants prior to the next index being
        called.

        Parameters
        ----------
        variants : `list` of `bio_misc.ipd.common.ClumpVariant`
            A variants to be updated.
        """
        super().update(variants)
        self.cur.executemany(
            self.UPDATE_VARIANT_SQL,
            [(int(i.is_index), int(i.is_unknown_bits), int(i.is_clumped),
              i.variant_idx) for i in variants]
        )
        self.conn.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chromosomes(self):
        """Get a unique set of chromosomes present in the dataset.

        Returns
        -------
        uniq_chr_names : `list` of `str`
            A unique set of chromosome names.
        """
        sql = """
        SELECT chr_name FROM indata GROUP BY chr_name
        """
        self.cur.execute(sql)
        return [i[0] for i in self.cur.fetchall()]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_count(self):
        """Get the total count of variants to be clumped.

        Returns
        -------
        total_variants : `int`
            A count of all filtered variants.
        """
        sql = """
        SELECT count(*) FROM indata
        """
        self.cur.execute(sql)
        return self.cur.fetchone()[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset(self):
        """Reset the handler to it's default unclumped state.
        """
        if self._is_updated is True:
            raise NotImplementedError("unable to reset")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_clump_join(self):
        def _clump_out():
            cols = [
                "variant_idx",
                "chr_name",
                "start_pos",
                "end_pos",
                "effect_allele",
                "other_allele",
                "pvalue",
                "is_index",
                "is_unknown",
                "is_clumped"
            ]

            sql = "SELECT * FROM indata ORDER BY variant_idx"
            self.cur.execute(sql)
            for i in self.cur:
                yield i[0], dict([(k, v) for k,v in zip(cols, i)])
        return join.JoinIterator(iter(_clump_out()), (0,))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FileSplitHandler(_BaseClumpHandler):
    """Provides batches of index variant/clump candidates to an LD
    clumping algorithm.

    This loads all filtered variants into memory, so is suitable for small
    jobs.

    Parameters
    ----------
    *args
        Any arguments to ``bio_misc.ipd.ld_clump._BaseClumpHandler``.
    **kwargs
        Any keyword arguments to ``bio_misc.ipd.ld_clump._BaseClumpHandler``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chromosomes = dict()
        self._save_files = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the handler and delete the variants.
        """
        super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def delete(self):
        for writer, fobj, file_name in self._save_files.values():
            os.unlink(file_name)
        super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def files(self):
        return self._save_files

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_data(self, **kwargs):
        """Filter the input file for variants that are below the cut off point.

        This filters all it for all the variants that are more significant or
        equal to the significance cut filter ``pvalue_filter``.

        Raises
        ------
        IOError
            If the handler is not open.

        Notes
        -----
        You should not need to call this directly as it is called by
        ``open()``.
        """
        try:
            super().load_data(**kwargs)
        finally:
            for writer, fobj, file_name in self._save_files.values():
                fobj.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def append_row(self, row):
        """Append a row to the handler's internal data

        Parameters
        ----------
        row : `Any`
            The row to append to the handler data source. Technically could be
            anything but in most cases will be a ``dict``
        """
        writer = self.get_file(row[self.chr_name])
        writer.writerow(row)

        try:
            self.chromosomes[row[self.chr_name]] += 1
        except KeyError:
            self.chromosomes[row[self.chr_name]] = 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_file(self, chr_name):
        """
        """
        try:
            return self._save_files[chr_name][0]
        except KeyError:
            # create a file
            self._save_files[chr_name] = self.create_file()
            return self._save_files[chr_name][0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def create_file(self):
        """
        """
        file_name = utils.get_tmp_file(dir=self.tmpdir)
        fobj = gzip.open(file_name, 'wt')
        writer = csv.DictWriter(fobj, self.inheader, delimiter=self.delimiter)
        writer.writeheader()
        return writer, fobj, file_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chromosomes(self):
        """Get a unique set of chromosomes present in the dataset.

        Returns
        -------
        uniq_chr_names : `list` of `str`
            A unique set of chromosome names.
        """
        return list(self.chromosomes.keys())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_count(self):
        """Get the total count of variants to be clumped.

        Returns
        -------
        total_variants : `int`
            A count of all filtered variants.
        """
        return sum([i for i in self.chromosomes.values()])

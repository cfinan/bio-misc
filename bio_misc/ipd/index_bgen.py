"""Open a bgen file with bgen-reader to ensure an index is created
"""
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    log
)
from bgen_reader import open_bgen
import os
import sys
import argparse


_DESC = __doc__
"""The program description also used for argparse. (`str`)
"""
_PROG_NAME = 'bm-bgen-reader-index'
"""The name of the script that is output when verbose is True (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    For API usage see ``bio_misc.ipd.index_bgen.run_create_index``.
    """
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=bool(args.verbose))
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        run_create_index(args.infile, allow_complex=args.allow_complex,
                         verbose=args.verbose)
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        # Missing files
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'infile', type=str,
        help="The input bgen file"
    )
    parser.add_argument(
        '--allow-complex', action="store_true",
        help="Open the bgen and allow complex variants, this creates a "
        "different kind of index"
    )
    parser.add_argument(
        '-v', '--verbose', action="store_true",
        help="Output progress"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_create_index(infile, allow_complex=False, verbose=False):
    """Open the bgen file to create the index
    """
    bgen = open_bgen(infile, allow_complex=allow_complex, verbose=verbose)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

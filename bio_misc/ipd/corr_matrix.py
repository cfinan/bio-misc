"""Output a correlation matrix for all variants in the input file.
The input file should have a chromosome, start position, effect allele,
other allele columns. The values for these can also be supplied from the
command line. This is low memory only holding data for a couple of
variants at time.

This can accept > 1 population to gather allele count data. These are
treated hierarchically, so if the variant is not available in the first
population, then the next one is checked, missing correlations will be
output as NaN.

The number of observations for each correlation is also given.
"""
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.ipd import (
    common,
    corr_matrix_zarr,
    corr_matrix_single
)
from bio_misc.common import (
    utils,
    constants as con,
    arguments,
    log
)
from tqdm import tqdm
from genomic_config import genomic_config
import numpy as np
import argparse
import sys
import os
import warnings
import multiprocessing
# import pprint as pp

# queue = multiprocessing.Queue()
queue = multiprocessing.JoinableQueue()

_DESC = __doc__
"""The program description also used for argparse. (`str`)
"""
_PROG_NAME = 'bm-corr-matrix'
"""The name of the script that is output when verbose is True (`str`)
"""
_MULTIPROC_BACKEND = "multiproc"
"""The name of the multiprocessing backend (`str`)
"""
_ZARR_BACKEND = "zarr"
"""The name of the zarr backend (`str`)
"""
_BACKENDS = [_MULTIPROC_BACKEND, _ZARR_BACKEND]
"""All the available backends (`list` of `str`)
"""
_PRECISION_MAP = {16: np.float16, 32: np.float32, 64: np.float64}
"""Mappings between integer data type arguments and numy datatypes
(`dict`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.

    For API usage see ``bio_misc.ipd.corr_matrix.get_corr_matrix``.
    """
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Read the config file, if not present will go to the defaults
    config = genomic_config.open(config_file=args.config)

    # Parse the cohort data from the command line arguments, the parsed data is
    # stored in args.cohorts
    common.parse_cohort_data(args, config)

    logger = log.init_logger(_PROG_NAME, verbose=bool(args.verbose))
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    print(args.cohorts)
    try:
        # Extract all the input variants from the input file
        invars = read_infile(
            args.infile, chr_name=args.chr_name, start_pos=args.start_pos,
            effect_allele=args.effect_allele, other_allele=args.other_allele,
            verbose=args.verbose, delimiter=args.delimiter,
            comment=args.comment, encoding=args.encoding
        )
        # pp.pprint(invars)
        # Get the correlation matrix and the number of observations used in
        # each comparison
        corr_matrix, nobs_matrix = get_corr_matrix(
            invars, args.cohorts, verbose=args.verbose,
            batch_size=args.batch_size, processes=args.processes,
            backend=args.backend, array_file_prefix=args.keep_array,
            dtype=_PRECISION_MAP[args.precision]
        )

        # Write everything out
        corr_out = "{0}.corr.txt.gz".format(args.outfile)
        nobs_out = "{0}.nobs.txt.gz".format(args.outfile)
        # print("writing")
        corr_matrix.to_csv(corr_out, sep="\t", header=True, index=True)
        nobs_matrix.to_csv(nobs_out, sep="\t", header=True, index=True)
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        # Missing files
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        config.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_corr_matrix(invars, cohorts, verbose=False, batch_size=50, processes=1,
                    backend=_MULTIPROC_BACKEND, chunk_rows=1000,
                    array_file_prefix=None, tmpdir=None, dtype=np.float64,
                    load_size=100):
    """The main high level API function for generating a correlation matrix.

    Parameters
    ----------
    invars : `list` of `bio_misc.ipd.common.Variant`
            The input genetic variants.
    cohorts : `list` of `bio_misc.ipd.common.Cohort`
        Descriptions of the cohorts that will be used for as the source data
        for the correlation matrix.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress. True will log messaged and > 1 will enable progress
        monitoring.
    batch_size : `int`, optional, default: `50`
        The number of variant allele counts in each batch that will be stored
        in memory at one time. So the total number in each comparison will be
        twice this. This can be increased to speed up but at the expense of
        additional memory usage. The memory usage will also depend on the
        sample count in your reference population.
    dtype : `numpy.float`, optional, default: `numpy.float64`
        The data type for the resulting correlation matrix. The accepted
        choices are `numpy.float64`, `numpy.float32` and `numpy.float16`,
        set according to the size of the matrix, available memory and the
        required precision. Lower values will save a lot of memory if
        memory is tight.

    Returns
    -------
    corr_matrix : `pandas.DataFrame`
        The master correlation matrix. The full span of the pairwise
        correlations, this will have the shape ``(len(invar)), len(invars))``
        and missing data will have the value ``numpy.nan``. The row and column
        indexes will be ``uni_id``, that is the chromosome and start position
        and alleles in sort order all concatinated with ``_``.
    nobs_matrix : `pandas.DataFrame`
        The master number of observations matrix. The full span of the pairwise
        number of observations, this will have the shape
        ``(len(invar)), len(invars))``. Missing data will have the value
        ``0``. The row and column indexes will be ``uni_id``, that is the
        chromosome and start position and alleles in sort order all
        concatinated with ``_``.
    """
    # If the batch size is single then run the slow implementation
    if batch_size == 1:
        warnings.warn(
            "If possible avoid batch sizes of 1 as these will have very "
            "poor performance, batch sizes of 50 do not use appreciably more"
            " memory"
        )
        cm = corr_matrix_single.RunCorrMatrix(
            cohorts,
            dtype=dtype
        )
        corr_matrix, nobs_matrix, invars = cm.run(
            invars, verbose=verbose
        )
    elif batch_size > 1 and processes == 1 and backend == _MULTIPROC_BACKEND:
        # If we are batching up to increase performance
        cm = corr_matrix_single.RunBatchCorrMatrix(
            cohorts,
            dtype=dtype
        )
        # pp.pprint(invars)
        corr_matrix, nobs_matrix, invars = cm.run(
            invars, batch_size=batch_size, verbose=verbose
        )
    elif processes > 1 and backend == _MULTIPROC_BACKEND:
        # Multiprocessing with direct querying
        cm = corr_matrix_single.RunParallelCorrMatrix(
            cohorts,
            dtype=dtype
        )
        corr_matrix, nobs_matrix, invars = cm.run(
            invars, batch_size=batch_size, processes=processes,
            verbose=verbose
        )
    elif backend == _ZARR_BACKEND:
        # Multiprocessing with direct querying
        cm = corr_matrix_zarr.RunZarrCorrMatrix(
            cohorts,
            dtype=dtype
        )
        corr_matrix, nobs_matrix, invars = cm.run(
            invars, batch_size=batch_size, processes=processes,
            verbose=verbose, chunk_rows=chunk_rows,
            tmpdir=tmpdir, file_prefix=array_file_prefix,
            load_size=load_size
        )
    else:
        raise ValueError(
            f"unknown combination: {backend} and {processes}"
        )
    return common.get_data_frame(corr_matrix, invars), \
        common.get_data_frame(nobs_matrix, invars)


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def do_correlations_idx(variants, got, is_ok):
#     """Perform the correlations and update the correlation matrices.

#     Parameters
#     ----------
#     corr_matrix : `np.array`
#         The master correlation matrix. A 2D array that represents the full span
#         of the pairwise correlations, of which ``variants`` may be a subset.
#     nobs_matrix : `np.array`
#         The master number of observations matrix. A 2D array that represents
#         the full span of the pairwise comparisons, of which ``variants`` may be
#         a subset.
#     variants : `list` of `np.MaskedArray`
#         The extracted allele counts/dosages.
#     got : `list` of `dict`
#         The genetic variants extracted using the reader. Each dict has
#         the keys and value types ``chr_name``, ``start_pos``,
#         ``effect_allele``, ``other_allele``, ``is_unknown``, ``idx``.
#     is_ok : `list` of `np.array`
#         Each sub array is a boolean array indicating if a individual has a
#         count or not.

#     Returns
#     -------
#     corr_matrix : `np.array`
#         The master correlation matrix. A 2D array that represents the full span
#         of the pairwise correlations. This is updated in place to the return is
#         not strictly necessary.
#     nobs_matrix : `np.array`
#         The master number of observations matrix. A 2D array that represents
#         the full span of the pairwise comparisons. This is updated in place so
#         the return is not strictly necessary.
#     """
#     cc = ma.corrcoef(variants)
#     r, c = get_matrix_indexes(got)

#     try:
#         cc_diag = cc[np.triu_indices(len(got))]
#         cc_diag[cc_diag.mask] = np.nan
#         cc_diag[cc_diag.mask] = np.nan
#     except TypeError:
#         # This is an edge case where the variant batch only contains a single
#         # variant, so cc is an integer of 1. In this case we do nothing as no
#         # correlations have actually taken place
#         if not isinstance(cc, int) and np.isnan(cc) is False:
#             raise
#         return r, c, np.array([np.nan]), np.array([0])

#     nobs = common.get_batch_nobs(is_ok)
#     nobs_diag = nobs[np.triu_indices(len(got))]
#     return r, c, cc_diag, nobs_diag


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def do_correlations(corr_matrix, nobs_matrix, variants, got, is_ok):
#     """Perform the correlations and update the correlation matrices.

#     Parameters
#     ----------
#     corr_matrix : `np.array`
#         The master correlation matrix. A 2D array that represents the full span
#         of the pairwise correlations, of which ``variants`` may be a subset.
#     nobs_matrix : `np.array`
#         The master number of observations matrix. A 2D array that represents
#         the full span of the pairwise comparisons, of which ``variants`` may be
#         a subset.
#     variants : `list` of `np.MaskedArray`
#         The extracted allele counts/dosages.
#     got : `list` of `dict`
#         The genetic variants extracted using the reader. Each dict has
#         the keys and value types ``chr_name``, ``start_pos``,
#         ``effect_allele``, ``other_allele``, ``is_unknown``, ``idx``.
#     is_ok : `list` of `np.array`
#         Each sub array is a boolean array indicating if a individual has a
#         count or not.

#     Returns
#     -------
#     corr_matrix : `np.array`
#         The master correlation matrix. A 2D array that represents the full span
#         of the pairwise correlations. This is updated in place to the return is
#         not strictly necessary.
#     nobs_matrix : `np.array`
#         The master number of observations matrix. A 2D array that represents
#         the full span of the pairwise comparisons. This is updated in place so
#         the return is not strictly necessary.
#     """
#     cc = ma.corrcoef(variants)
#     r, c = get_matrix_indexes(got)

#     try:
#         diag = cc[np.triu_indices(len(got))]
#         diag[diag.mask] = np.nan
#         diag[diag.mask] = np.nan
#     except TypeError:
#         # This is an edge case where the variant batch only contains a single
#         # variant, so cc is an integer of 1. In this case we do nothing as no
#         # correlations have actually taken place
#         if not isinstance(cc, int) and np.isnan(cc) is False:
#             raise
#         return corr_matrix, nobs_matrix
#     corr_matrix[r, c] = diag
#     corr_matrix[c, r] = diag
#     nobs = common.get_batch_nobs(is_ok)
#     diag = nobs[np.triu_indices(len(got))]
#     nobs_matrix[r, c] = diag
#     nobs_matrix[c, r] = diag
#     return corr_matrix, nobs_matrix


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def is_square(m):
#     """Test is a matrix is square.

#     Parameters
#     ----------
#     m : `numpy.array`
#         A 2D numpy array.

#     Returns
#     -------
#     square : `bool`
#         ``True`` if the matrix is square ``False`` if not.
#     """
#     return m.shape[0] == m.shape[1]


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_batch_nobs(has_count):
#     """Get a matrix of number of observations used in each pairwise correlation.

#     Parameters
#     ----------
#     has_count : `list` of `np.array`
#         The list will have length equal to the number of variants that have
#         been compared in the batch, with each sub numpy array representing a
#         variant and being a boolean array indicating if an individual had an
#         allele count for the variant.

#     Returns
#     -------
#     nobs : `numpy.array`
#         A 2D array with shape ``(len(has_count), len(has_count))`` with the
#         number of observations for each pairwise variant comparison.
#     """
#     nobs = np.array([np.sum(i & j) for i in has_count for j in has_count])
#     nobs.shape = (len(has_count), len(has_count))
#     return nobs


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_matrix_indexes(variants):
#     """Get the index positions of all the variants for the row, colums of the
#     triangle

#     Parameters
#     ----------
#     variants : `dict`
#         The input genetic variants. This must have the key ``idx`` (which
#         should not be repeated).

#     Returns
#     -------
#     rows : `np.array`
#         The index positions of the variant rows in a triangular matrix.
#     columns : `np.array`
#         The index positions of the variant columns in a triangular matrix.
#     """
#     vl = len(variants)
#     columns = [
#         variants[j][common._IDX_COL] for idx, i in enumerate(range(vl, 0, -1))
#         for j in range(0+idx, vl)
#     ]
#     rows = [
#         j for idx, i in enumerate(range(vl, 0, -1))
#         for j in it.repeat(variants[idx][common._IDX_COL], i)
#     ]
#     return rows, columns


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_infile(infile, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                effect_allele='effect_allele', other_allele='other_allele',
                verbose=False, delimiter=con.DEFAULT_DELIMITER,
                encoding=con.DEFAULT_ENCODING, comment=con.DEFAULT_COMMENT):
    """Filter the input file for variants that are below the cut off point.

    This takes an input file and filters all it for all the variants that are
    more significant or equal to the significance cut off.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chr_name : `str`, optional: default: `bio_misc.common.constants.CHR_NAME`
        The name of the chromosome column in the header of ``infile``.
    start_pos : `str`, optional: default: `bio_misc.common.constants.START_POS`
        The name of the start position column in the header of ``infile``.
    effect_allele : `str`, optional: default: `effect_allele`
        The name of the effect allele column in the header of ``infile``.
    other_allele : `str`, optional: default: `other_allele`
        The name of the other allele column in the header of ``infile``.
    verbose : `bool`, optional, default: `False`
        Report progress.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.

    Yields
    ------
    data : `dict`
        The data row. The ``dict`` represents an input variant that is more
        significant than the cut off value. The keys are columns and should
        match the names for ``chr_name``, ``start_pos``, ``effect_allele``,
        ``other_allele`` and ``pvalue``.
    """
    logger = log.retrive_logger(_PROG_NAME, verbose=verbose)

    kwargs = dict(
        chr_name=chr_name,
        start_pos=start_pos,
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    logger.info("reading variants...")
    errors = 0
    input_variants = []
    # The allele count extraction class takes variants
    with utils.CoordsParser(infile, **kwargs) as parser:
        effect_allele_idx = parser.get_col_idx(effect_allele)
        other_allele_idx = parser.get_col_idx(other_allele)

        for idx, row in tqdm(enumerate(parser, 1),
                             desc="[info] filtering input file...",
                             unit=" rows",
                             disable=common.verbose_disable(verbose)):
            try:
                input_variants.append(
                    common.Variant(
                        row[parser.chr_name_idx],
                        row[parser.start_pos_idx],
                        row[effect_allele_idx],
                        row[other_allele_idx]
                    )
                )
            except TypeError:
                continue
                errors += 1

    logger.info(f"there are {idx} input variants...")
    logger.info(f"there are {errors} error variants...")
    return input_variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'cohorts', type=str, nargs="+",
        help="The name for the cohort(s) you want to extract from"
    )
    arguments.add_input_output(
        parser,
        infile_optional=True,
        outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_genomic_coords_arguments(parser, end_pos=True)
    parser.add_argument(
        '--config', type=str,
        help="The path to the genomic-config file, that lists all the"
        " reference populations to use for LD, if not supplied will try to"
        " ID it in ``GENOMIC_CONFIG`` environment variable  or "
        "~/.genomic_data.ini"
    )
    parser.add_argument(
        '--samples', type=str, nargs="+", default=['-'],
        help="The path any sample ID file (one sample listed per line), of a"
        " sample name to use in the config file. These will be used as a "
        "reference for LD and if not supplied will extract all the samples. If"
        " you only want samples from a single cohort then you should use a "
        "dash to represent any other cohorts (i.e. no file) so if you have "
        "three cohorts and you only want samples for cohort to then you would"
        " use --samples - /path/to/cohort2/samples -"
    )
    parser.add_argument(
        '--format', type=str, nargs="+", choices=common.FILE_FORMATS,
        default=[common.FILE_FORMATS[0]],
        help="The file formats for the reference genome files to extract "
        "from. There should be one for each cohort"
    )
    parser.add_argument(
        '--assembly', type=str, default="GRCh38",
        help="The assembly for the reference cohort"
    )
    parser.add_argument(
        '-v', '--verbose', action="count",
        help="Output progress, use -vv for progress monitors"
    )
    arguments.add_allele_arguments(
        parser, ref=True, alt=True, use_effect_other=True
    )
    parser.add_argument(
        '--zero', type=str, nargs="+", default=[],
        help="The names of the cohorts that you want to add a leading zero to"
        " the chromosome name that have a length < 2 this is to deal with "
        "some UK BioBank bgen implementations."
    )
    parser.add_argument(
        '--batch-size', type=int, default=50,
        help="The batch size for the number of allele dosages that will be "
        "held in memory at one time."
    )
    parser.add_argument(
        '--processes', type=int, default=1,
        help="The number of processes to use."
    )
    parser.add_argument(
        '--backend', type=str,
        default=_MULTIPROC_BACKEND,
        choices=_BACKENDS,
        help="The backend to use for multiprocessing"
    )
    parser.add_argument(
        '--precision', type=str,
        default=32,
        choices=[i for i in _PRECISION_MAP.keys()],
        help="The precision to use for all the calculations"
    )
    parser.add_argument(
        '--keep-array', type=str, default=None,
        help="If using the zarr backend then pass a directory here to write"
        " to if you want to keep persistent storage of allele dosages"
    )
    parser.add_argument(
        '--species', type=str, default=common.SPECIES,
        help="The species for the reference cohort"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

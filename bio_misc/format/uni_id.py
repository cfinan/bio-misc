"""Use the chromosome/position, reference allele/alternate allele, to create
a universal identifier that is chr_pos_allele1_allele2 where allele1 and
allele2 are the reference and alternate alleles in sort order
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.format import common as cpcom
import argparse
import sys
import os
import csv
# import pprint as pp


_SCRIPT_NAME = 'bm-uni-id'
"""The name of the script that is output when verbose is True (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_uni_id(infile, outfile=None, chr_name=con.CHR_NAME,
               start_pos=con.START_POS, ref_allele=con.REF_ALLELE,
               alt_allele=con.ALT_ALLELE, delimiter=con.DEFAULT_DELIMITER,
               verbose=False, encoding=con.DEFAULT_ENCODING, tmp_dir=None,
               comment=con.DEFAULT_COMMENT):
    """Take and input file, split the chromosome position column and write to
    an output file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_name : `str`, optional, default: `con.CHR_POS`
        The name of the chromosome column in the input file.
    start_pos : `str`, optional, default: `con.CHR_POS`
        The name of the start position column in the input file.
    ref_allele : `str`, optional, default: `con.CHR_POS`
        The name of the reference allele  column in the input file.
    alt_allele : `str`, optional, default: `con.CHR_POS`
        The name of the alternate allele column in the input file.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in yield_uni_id(infile, chr_name=chr_name, start_pos=start_pos,
                                ref_allele=ref_allele, alt_allele=alt_allele,
                                encoding=encoding, delimiter=delimiter,
                                comment=comment):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_uni_id(infile, chr_name=con.CHR_NAME, start_pos=con.START_POS,
                 ref_allele=con.REF_ALLELE, alt_allele=con.ALT_ALLELE,
                 yield_header=True, encoding=con.DEFAULT_ENCODING,
                 comment=con.DEFAULT_COMMENT, delimiter=con.DEFAULT_DELIMITER):
    """Yield rows of matching entries.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chr_name : `str`, optional, default: `con.CHR_POS`
        The name of the chromosome column in the input file.
    start_pos : `str`, optional, default: `con.CHR_POS`
        The name of the start position column in the input file.
    ref_allele : `str`, optional, default: `con.CHR_POS`
        The name of the reference allele  column in the input file.
    alt_allele : `str`, optional, default: `con.CHR_POS`
        The name of the alternate allele column in the input file.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    AttributeError
        If any of the chrpos data in the column can't be parsed.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment,
        chr_name=chr_name,
        start_pos=start_pos,
        end_pos=start_pos
    )

    with utils.CoordsParser(infile, **kwargs) as parser:
        ref_allele_idx = parser.get_col_idx(ref_allele)
        alt_allele_idx = parser.get_col_idx(alt_allele)

        if yield_header is True:
            yield parser.header + parser.get_unique_col(['uni_id'])

        for row in parser:
            try:
                chr_name_val, start_pos_val, end_pos_val = parser.get_coords(
                    row
                )
                uni_id = make_uni_id(
                    chr_name_val, start_pos_val, row[ref_allele_idx],
                    row[alt_allele_idx]
                )
            except AttributeError:
                # can't parse
                uni_id = None

            row.append(uni_id)
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_uni_id(chr_name, start_pos, ref_allele, alt_allele):
    """Generate a universal ID

    Parameters
    ----------
    chr_name : `str`
        The chromosome value.
    start_pos : `int`
        The start position.
    ref_allele : `str`
        The reference allele.
    alt_allele : `str`
        The alternate allele.

    Returns
    -------
    uni_id : `str`
        The universal ID, which is the
        <chr_name>_<start_pos>_<allele1>_<allele2> where allele1 and allele2
        are the ref/alt alleles in sort order.
    """
    return "{0}_{1}_{2}_{3}".format(
        chr_name, start_pos, *(sorted([ref_allele, alt_allele]))
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        get_uni_id(
            args.infile, outfile=args.outfile, chr_name=args.chr_name,
            start_pos=args.start_pos, ref_allele=args.ref_allele,
            alt_allele=args.alt_allele, delimiter=args.delimiter,
            verbose=args.verbose, comment=args.comment,
            encoding=args.encoding, tmp_dir=args.tmp_dir
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Generate a universal ID from chr_pos_allele1_allele2 "
        "where allele1/allele2 are the ref/alt alleles in sort order"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_genomic_coords_arguments(parser, end_pos=False)
    arguments.add_allele_arguments(parser)
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

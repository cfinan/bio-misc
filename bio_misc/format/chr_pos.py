"""Split a chr:pos column into its component parts
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import (
    utils,
    constants as con,
    arguments
)
from bio_misc.format import common as cpcom
import argparse
import sys
import os
import csv
# import pprint as pp


_SCRIPT_NAME = 'bm-split-chr-pos'
"""The name of the script that is output when verbose is True (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def split_chr_pos(infile, outfile=None, chr_pos=con.CHR_POS,
                  spec=cpcom.DEFAULT_CHR_POS_SPEC,
                  delimiter=con.DEFAULT_DELIMITER, verbose=False,
                  encoding=con.DEFAULT_ENCODING, tmp_dir=None,
                  comment=con.DEFAULT_COMMENT):
    """Take and input file, split the chromosome position column and write to
    an output file.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file. If ``NoneType`` then output will be to ``STDOUT``.
    chr_pos : `str`, optional, default: `con.CHR_POS`
        The name of the chrpos column in the input file.
    spec : `list` of `str`, optional, default: `DEFAULT_CHR_POS_SPEC`
        The specification of the chrpos column (i.e. what information it
        contains). The list should be structured in the same order that
        the values appear in the chrpos column. The allowed values (and
        defaults) are chr_name, start_pos, end_pos, effect_allele,
        other_allele. So, if the structure of a chromosome position column
        is `chr_name:start_pos:other_allele`, the spec would be defined as:
        `chrpos_spec=['chr_name', 'start_pos', 'other_allele']`
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.
    verbose : `bool`, optional, default: `False`
        Report progress.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    tmp_dir : `str` or `NoneType`, optional, default: `NoneType`
        If writing to a file, a temp file is used and copied over to the final
        location when finished. Use this to specify a different temp location.
        If `NoneType` then the system default temp location is used.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    """
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        for row in yield_split_chr_pos(infile, chr_pos=chr_pos, spec=spec,
                                       encoding=encoding, delimiter=delimiter,
                                       comment=comment):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_split_chr_pos(infile, chr_pos=con.CHR_POS, yield_header=True,
                        spec=cpcom.DEFAULT_CHR_POS_SPEC, start_anchor=False,
                        end_anchor=False, encoding=con.DEFAULT_ENCODING,
                        comment=con.DEFAULT_COMMENT,
                        delimiter=con.DEFAULT_DELIMITER):
    """Yield rows of the input file with chrpos split.

    Parameters
    ----------
    infile : `str`
        The input file with the ``gene_id`` column.
    chr_pos : `str`, optional, default: `con.CHR_POS`
        The name of the chrpos column in the input file.
    yield_header : `bool`, optional, default: `True`
        When processing the input file the first row yielded should be a header
        row.
    spec : `list` of `str`, optional, default: `DEFAULT_CHR_POS_SPEC`
        The specification of the chrpos column (i.e. what information it
        contains). The list should be structured in the same order that
        the values appear in the chrpos column. The allowed values (and
        defaults) are chr_name, start_pos, end_pos, effect_allele,
        other_allele. So, if the structure of a chromosome position column
        is `chr_name:start_pos:other_allele`, the spec would be defined as:
        `chrpos_spec=['chr_name', 'start_pos', 'other_allele']`
    start_anchor : `bool`, optional, default: `False`
        Should the chrpos spec be anchored to the start of the text in the
        column, i.e. like ``^`` in a regexp.
    end_anchor : `bool`, optional, default: `False`
        Should the chrpos spec be anchored to the end of the text in the
        column, i.e. like ``$`` in a regexp.
    encoding : `str`, optional, default: `con.DEFAULT_ENCODING`
        The default encoding of the input (and output) files.
    comment : `str`, optional, default: `con.DEFAULT_COMMENT`
        The symbol for a comment row that will be skipped if they occur before
        the header.
    delimiter : `str`, optional, default `con.DEFAULT_DELIMITER`
        The delimiter of the input (and output) files.

    Yields
    ------
    row : `list`
        A row of data. If ``yield_header=True`` then the first row will be the
        header row.

    Raises
    ------
    AttributeError
        If any of the chrpos data in the column can't be parsed.
    """
    kwargs = dict(
        delimiter=delimiter,
        encoding=encoding,
        comment=comment
    )

    chr_pos_parser = cpcom.ChrPosParser(spec=spec, start_anchor=start_anchor,
                                        end_anchor=end_anchor)

    with utils.BaseParser(infile, **kwargs) as parser:
        chr_pos_idx = parser.get_col_idx(chr_pos)

        if yield_header is True:
            yield parser.header + parser.get_unique_col(spec)
        for row in parser:
            try:
                parsed_chr_pos = chr_pos_parser.parse(row[chr_pos_idx])
            except AttributeError:
                # can't parse
                parsed_chr_pos = [None] * len(spec)
                print(row[chr_pos_idx])
                print(chr_pos_parser.regexp)
                raise
            row.extend(parsed_chr_pos)
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    try:
        split_chr_pos(
            args.infile, outfile=args.outfile, chr_pos=args.chr_pos,
            spec=args.spec, delimiter=args.delimiter, verbose=args.verbose,
            comment=args.comment, encoding=args.encoding, tmp_dir=args.tmp_dir
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
    finally:
        m.msg("*** END ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Split a chromosome-position column into it's components "
        "to make it computationally tractable"
    )
    arguments.add_input_output(
        parser, infile_optional=True, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    parser.add_argument(
        '-c', '--chr-pos', type=str, default=con.CHR_POS,
        help="The name of the chromosome position column "
        "(default: {}).".format(con.CHR_POS)
    )
    parser.add_argument(
        '--spec', type=str, nargs='+', default=['chr_name', 'start_pos'],
        help="The spec of the column (the order of sub columns, can be"
        " anything from chr_name, start_pos, end_pos, ref_allele, "
        "alt_allele) (default: {}).".format(
            ','.join([con.CHR_NAME, con.START_POS])
        )
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

"""Common components for chromosome:position parsing
"""
from bio_misc.common import (
    constants as con
)
import collections
import re
# import pprint as pp


CHRPOS_COMPONENTS = {
    con.CHR_NAME:
        r'(?:c(?:hr)?)?(?P<{chr_name}>(?:[1-9]|1[0-9]|2[0-5])|m(?:t)?|x|y)',
    con.START_POS: r'(?P<{start_pos}>[1-9]\d*)',
    con.END_POS: r'(?P<{end_pos}>[1-9]\d*)',
    con.REF_ALLELE: r'(?P<{ref_allele}>(?:[ATCGDIRSVN]+|-))',
    con.ALT_ALLELE: r'(?P<{alt_allele}>(?:[ATCGDIRSVN-]+[/,]?)+)'
}
"""Components of an overall regex of chrpos extractions (`dict`)

Notes
-----
The keys of the dict are the standard merit names i.e. ``chr_name``,
``start_pos``, ``end_pos``, ``effect_allele``, ``other_allele``. The values
are regular expression raw strings to recognise them. Note the chromosome
regexp is exclusively the standard human chromosomes 1-22, x, y, mt. The use
of this data stricture is designed so that the components can be combined
easily in different orders (separated by delimiter).

The various components tagged by the regexp can be accessed in a final
`re.search` by named parameter access i.e. ``match.group("chr_name")``.
"""

# The default flags that will be used with the chrpos regexps
CHRPOS_FLAGS = re.VERBOSE | re.IGNORECASE
"""The flags applied to `re.compile` when generating the chr:pos regexp (`int`)
"""

# The column mappings
DEFAULT_CHRPOS_MAPPINGS = {con.CHR_NAME: con.CHR_NAME,
                           con.START_POS: con.START_POS,
                           con.END_POS: con.END_POS,
                           con.REF_ALLELE: con.REF_ALLELE,
                           con.ALT_ALLELE: con.ALT_ALLELE}
"""Used to extract regexp named groups (`dict`)
"""

# If the user does not change the chrpos spec, then this is the spec that will
# be used for chrpos parsing
DEFAULT_CHR_POS_SPEC = (
    con.CHR_NAME,
    con.START_POS,
    con.END_POS,
    con.REF_ALLELE,
    con.ALT_ALLELE
)
"""The default chr:pos spec aims to extract everything
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChrPosParser(object):
    """Handle the parsing of ``chrpos`` columns in GWAS datasets

    The parsing of chromosome position columns this was rolled into a class to
    make it easier for the user to spec out the structure of the chrpos column.

    Parameters
    ----------
    spec : `list` of `str`
        The specification of the chrpos column (i.e. what information it
        contains). The list should be structured in the same order that
        the values appear in the chrpos column. The allowed values (and
        defaults) are chr_name, start_pos, end_pos, effect_allele,
        other_allele. So, if the structure of a chromosome position column
        is `chr_name:start_pos:other_allele`, the spec would be defined as:
        `chrpos_spec=['chr_name', 'start_pos', 'other_allele']`
    chrpos_flags : `int`, optional, default: re.VERBOSE | re.IGNORECASE
        Flags to the regular expression engine. The defaults are:
        re.VERBOSE | re.IGNORECASE
    delimiters : `str`, optional, default: `:_/,-`
        The characters that you might expect to see between the various
        components of the chrpos_spec
    start_anchor : `bool`, optional, default: `True`
        Should the chrpos regexp be anchored to the start of the string,
        acts like `^` in a regexp
    end_anchor : `bool`, optional, default: `True`
        Should the chrpos regexp be anchored to the end of the string,
        acts like `$` in a regexp

    Notes
    -----
    Many datasets annotate the variant coordinates and sometimes alleles and
    other information in a single column. In the MeRIT code and documentation
    these are referred to as `chrpos` columns. This makes the data
    computationally intractable in it's native form. An added complexity, is
    that the delimiters between the data components may also vary.

    To deal with these complexities, the `data_tools.ChrPosParser` class can
    be
    used. This enables the user to define the types of data that exist in the
    chrpos column and a variety of delimiters that can exist between the data
    components. This class, has two methods that can parse out the data:

    * `data_tools.ChrPosParser.extract_chrpos`: Extracts the chromosome
      position and allele information into a completely new DataFrame.

    * `data_tools.ChrPosParser.assign_chrpos`: Extracts the chromsome position
      information into user defined column names in the existing DataFrame,

    The data components encoded in the ``chrpos`` column are defined by setting
    the ``chrpos_spec`` parameter when a ``ChrPosParser`` object is
    instantiated. ``chrpos_spec`` should be a list of merit column names where
    the order of the list reflects the order of the data components in the
    ``chrpos`` column. The default ``chrpos_spec`` is ``['chr_name',
    'start_pos', 'end_pos', 'effect_allele', 'other_allele']``. These are the
    full complement of columns that are allowed in the ``chrpos_spec``, if any
    other names appear in the ``chrpos_spec`` then it is an error. It is also
    an error if any duplicated names appear in the ``chrpos_spec``. So if the
    ``chrpos`` column has the following structure ``11:2564322`` (``chr:pos``)
    then the ``chrpos_spec`` would be ``['chr_name', 'start_pos']``. If it is
    ``11:2564322:A_AGGC`` (``chr:pos:effect_other``) then the ``chrpos_spec``
    would be ``['chr_name', 'start_pos', 'effect_allele', 'other_allele']``.
    Alternatively, it could be ``11:2564322:AGGC_A`` (``chr:pos:effect_other``)
    making the ``chrpos_spec`` ``['chr_name', 'start_pos', 'other_allele',
    'effect_allele']``. You will notice that the ``chrpos_spec`` does not
    contain any information about delimiters. These are given using the
    ``delimiters`` parameter. This defines a pool of single character
    delimiters any of which can appear between the data components of the
    ``chrpos_spec``. Finally, the ``chrpos_spec`` can be made more specific
    using the ``start_anchor`` and/or ``end_anchor`` parameters. These act like
    the ``^`` and ``$`` respectively in a regular expression.

    See the ipython notebook in examples ``examples/chrpos_parsing.ipynb`` for
    some illustrations of using the ``data_tools.ChrPosParser``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def __init__(self, spec=DEFAULT_CHR_POS_SPEC, chrpos_flags=CHRPOS_FLAGS,
                 delimiters=r':_/,-', start_anchor=True, end_anchor=True):
        self._spec = spec
        self._chrpos_flags = chrpos_flags
        self._delimiters = delimiters
        self._start_anchor = start_anchor
        self._end_anchor = end_anchor

        # First make sure the spec is valid
        self.check_spec(self._spec)

        # Now we build a regexp according to the spec
        self._build_regex()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def check_spec(cls, spec):
        """
        Check the structure of a chrpos spec to make sure it is ok, it can't
        be empty or have any other values that are not the in the defaults

        Parameters
        ----------
        spec :list of str
            The order of the column data to be extracted from the chrpos column
            allowed column names are chr_name, start_pos, end_pos,
            effect_allele and other_allele. These must also be unique in the
            spec

        Raises
        ------
        ValueError
            if the spec is empty [] or `NoneType`. If unknown column names are
            in the spec or if any of the column names in the spec are repeated
        """
        try:
            # noinspection PyTypeChecker
            if len(spec) == 0:
                raise ValueError("no items in the spec")
        except TypeError:
            # if it is NoneType
            raise ValueError("spec is the incorrect format")

        # Loop through the spec and make sure all the items in it are valid
        # expected column names
        for i in spec:
            if i not in DEFAULT_CHR_POS_SPEC:
                raise ValueError("unknown item in spec '{0}'".format(i))

        # if we get to here then we test for duplicated values in the spec
        # as they could cock things up
        spec_count = [arg for arg, count in collections.Counter(spec).items()
                      if count > 1]
        if len(spec_count) > 0:
            raise ValueError("'{0}' repeated in the chrpos spec".format(
                ','.join(spec_count)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def regexp(self):
        """
        return the regexp
        """
        return self._regexp

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _build_regex(self):
        """
        Build the regexp according to the spec and the start/end anchor
        arguments
        """
        # Will hold individual bits of the regular chrpos expression, these
        # will eventually be joined together with the delimiters
        regexp = []

        # The regexp named capture groups can be dynamically named, however,
        # at present we are just naming them with the merit column names. This
        # holds the renaming arguments for the spec
        spec_subs = {}

        for i in self._spec:
            regexp.append(CHRPOS_COMPONENTS[i])
            spec_subs[i] = i

        # join the regexp components into a single regexp string
        self._regexp = r'[{0}]'.format(self._delimiters).join(regexp)

        # If we are anchoring the start or the end then update the regexp
        # TODO: Test moving these after the renaming as I am not sure if the
        #  named capture groups will interfere here
        if self._start_anchor is True:
            self._regexp = r'^{0}'.format(self._regexp)

        if self._end_anchor is True:
            self._regexp = r'{0}$'.format(self._regexp)

        # These will be used as default mappings if no mappings are supplied
        # to assign_chr_pos
        self._mappings = spec_subs

        # Now format the named capture groups to the merit column names
        self._regexp = self._regexp.format(**spec_subs)
        self._regexp_compiled = re.compile(self._regexp, self._chrpos_flags)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self, chrpos):
        """Parse a chromosome position value into it's component parts based on
        the spec defined in the class.

        Parameters
        ----------
        chrpos : `str`
            The chromosome position string to parse

        Returns
        -------
        parsed : `list` of `str`
            The parsed chromosome position string.

        Notes
        -----
        Note that all components are string and no casting of any positional
        data is attempted. The order of the elements in the list are as is
        given in the spec.
        """
        parsed = []
        match = self._regexp_compiled.search(chrpos)
        for i in self._spec:
            parsed.append(match.group(i))
        return parsed

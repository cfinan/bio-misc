name	species	assembly	file_format	corr_matrix	variants	test_file	tabix_index
pop_r11_c10	human	b37	vcf	pop_r11_c10_corr.txt.gz	pop_r11_c10_variants.txt.gz	pop_r11_c10.vcf.gz	pop_r11_c10.vcf.gz.tbi
pop_r117_c10	human	b37	vcf	pop_r117_c10_corr.txt.gz	pop_r117_c10_variants.txt.gz	pop_r117_c10.vcf.gz	pop_r117_c10.vcf.gz.tbi
pop_r1117_c10	human	b37	vcf	pop_r1117_c10_corr.txt.gz	pop_r1117_c10_variants.txt.gz	pop_r1117_c10.vcf.gz	pop_r1117_c10.vcf.gz.tbi

"""Determine the interaction path distances between proteins within drug
targets and a set of input proteins
"""
# Importing the version number (in __init__.py) and the package name
from bio_misc import __version__, __name__ as pkg_name
from bio_misc.common import utils, constants as con, arguments
from bio_misc.reactome import common as rcommon
from bio_misc.drug_lookups import chembl_queries as cq
from tqdm import tqdm
from sqlalchemy import text
import argparse
import getpass
import csv
import os
# import sys
# import re
# import pprint as pp


_SCRIPT_NAME = 'bm-drug-target-interact'
"""The name of the script that is implemented in this module (`str`)
"""
# _SWISSPROT_NAME = "Uniprot/SWISSPROT"
# """The name of the UniProt/SwissProt database that Ensembl uses (`str`)
# """
# _TREMBL_NAME = "Uniprot/SPTREMBL"
# """The name of the UniProt/Trembl database that Ensembl uses (`str`)
# """

# HEADER = [
#     ('uniprot_db_name', 'dbname'),
#     ('uniprot_id', 'primary_id'),
#     ('uniprot_syn_idx', 'uniprot_syn_idx'),
#     ('uniprot_syn_id', 'uniprot_syn_id'),
#     ('uniprot_display_id', 'display_id'),
#     ('uniprot_desc', 'description'),
#     ('uniprot_mapping', 'info_type'),
#     ('uniprot_gene_start', 'ensembl_start'),
#     ('uniprot_gene_end', 'ensembl_end'),
#     ('uniprot_gene_identity', 'ensembl_identity'),
#     ('uniprot_prot_start', 'xref_start'),
#     ('uniprot_prot_end', 'xref_end'),
#     ('uniprot_prot_identity', 'xref_identity'),
#     ('uniprot_mapping_score', 'score'),
#     ('uniprot_mapping_evalue', 'evalue'),
#     ('uniprot_prot_version', 'version')
# ]
# """The columns added to the output file and either names in the rest data.
# The tuples are [0] output file header name (`str`) [1] rest data name (`str`)
# (`list` of `tuple`).
# """


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_drug_target_interactions(
        infile, chembl_conn, reactome_user, reactome_pass, reactome_host,
        reactome_port, outfile=None, verbose=False, processes=1,
        prot_id=con.UNIPROT_PROT_ID, tmp_dir=None, comment=con.DEFAULT_COMMENT,
        delimiter=con.DEFAULT_DELIMITER, encoding=con.DEFAULT_ENCODING):
    """The main high level API function that takes an input file and generates
    and output file of uniprot mappings.
    """
    unique_prot = get_unique_proteins(
        infile, prot_id, comment=comment,
        delimiter=delimiter, encoding=encoding
    )
    # unique_prot = unique_prot[:2]
    target_map = get_all_targets(chembl_conn)
    targets = list(target_map.keys())
    ncomparisons = len(unique_prot) * len(targets)
    with utils.stdopen(outfile, mode='wt', use_tmp=True, tmp_dir=tmp_dir,
                       encoding=encoding) as out:
        writer = csv.writer(out, delimiter=delimiter,
                            lineterminator=os.linesep)
        writer.writerow(
            ["source_uniprot_id", "target_uniprot_id", "path_length"]
        )
        for row in tqdm(
                rcommon.yield_path_dist(
                    reactome_user, reactome_pass, reactome_host,
                    reactome_port, unique_prot, targets,
                    processes=processes
                ),
                desc="[progress] fetching path lengths: ",
                unit=" comparisons",
                disable=not verbose,
                total=ncomparisons
        ):
            writer.writerow(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_unique_proteins(infile, prot_id_col, **kwargs):
    """
    """
    unique_prot = []
    with utils.BaseParser(infile, **kwargs) as parser:
        prot_id_idx = parser.get_col_idx(prot_id_col)

        for row in parser:
            prot_id = row[prot_id_idx]

            if prot_id is not None and prot_id not in unique_prot:
                unique_prot.append(prot_id)

    return unique_prot


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_targets(chembl_conn, organism="Homo sapiens",
                    component_type="PROTEIN"):
    session = chembl_conn()

    sql = text(
        """
        SELECT td.chembl_id as target_chembl_id,
               td.pref_name as target_pref_name,
               cs.component_type,
               cs.accession as target_accession,
               td.target_type,
               td.organism
        FROM drug_mechanism dm
        JOIN target_dictionary td on dm.tid = td.tid
        JOIN target_components tc on dm.tid = tc.tid
        JOIN component_sequences cs on tc.component_id = cs.component_id
        WHERE td.organism = :ORGANISM AND component_type = :COMPONENT_TYPE
        GROUP BY target_chembl_id, target_accession
        """
    )

    query = session.execute(
        sql, dict(ORGANISM=organism, COMPONENT_TYPE=component_type)
    )

    protein_target_map = {}
    for i in query:
        protein_target_map[i.target_accession] = dict(i)
    session.close()
    return protein_target_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Make sure the password does not appear on screen, (a bit of a hack)
    neo_password = args.neo_pass
    args.neo_pass = '*****'

    m = utils.Msg(prefix=con.MSG_INFO, file=con.MSG_LOC, verbose=args.verbose)
    m.msg_prog(_SCRIPT_NAME, pkg_name, __version__)
    m.msg_args(args)

    # Connect to the databases, both of them do not seem to need closing
    # Connect to reactome
    # reactome_conn = rcommon.connect(
    #     args.neo_host, args.neo_port, args.neo_user, neo_password
    # )

    # Session maker
    chembl_conn = cq.chembl_sqlite_connect(args.chembl)

    get_drug_target_interactions(
        args.infile, chembl_conn, args.neo_user, neo_password, args.neo_host,
        args.neo_port, outfile=args.outfile, delimiter=args.delimiter,
        verbose=args.verbose, encoding=args.encoding, processes=args.processes,
        comment=args.comment, tmp_dir=args.tmp_dir, prot_id=args.prot_id
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Get the P-P interaction path distance between a set of "
        "proteins and known drug targets."
    )
    arguments.add_input_output(
        parser, infile_optional=False, outfile_optional=True
    )
    arguments.add_file_arguments(parser)
    arguments.add_verbose_arguments(parser)
    arguments.add_id_column_arguments(parser, ensembl_id=False)
    arguments.add_neo4j_arguments(parser)
    arguments.add_drug_data_arguments(parser, bnf=False)
    parser.add_argument('--processes', type=int, default=1,
                        help="The number of processes to use")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it

    Returns
    -------
    args : `Namespace`
        The parsed command line argments
    """
    # Now parse the arguments
    args = parser.parse_args()

    if args.neo_pass is None:
        args.neo_pass = getpass.getpass(prompt='Input Neo4j password > ')
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

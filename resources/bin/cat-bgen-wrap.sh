#!/bin/bash
set -euo pipefail
list_file="$1"
outfile="$2"

echo "*** cat-bgen-wrap.sh ***"
echo "[info] list file: $list_file"
echo "[info] outfile: $outfile"

echo "[info] getting files..."
INFILES=($(cat "$list_file"))

echo "[info] merging chunks"
cat-bgen -g ${INFILES[@]} -og "$outfile"

echo "[info] create an index: $outfile"
bgenix -g "$outfile" -index

bm-bgen-reader-index -v "$outfile"
bm-bgen-reader-index -v --allow-complex "$outfile"

echo "*** END ***"

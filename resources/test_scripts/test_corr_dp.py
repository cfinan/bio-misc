#!/usr/bin/env python
from numba import jit
import pandas as pd
import numpy as np
import numpy.ma as ma
import math
import warnings

# number of rows in one chunk
SPLITROWS = 2
nrows = 10
ncols = 10
# the big table, which is usually bigger
bigdata = np.random.random((nrows, ncols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_nan(bigdata, prop=0.1):
    nulls = np.random.randint(
        0, bigdata.size - 1, math.ceil(bigdata.size * prop)
    )
    bigdata.flat[nulls] = np.nan
    mask = np.isnan(bigdata)
    ma.array(bigdata, mask=mask)
    return ma.array(bigdata, mask=mask)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_full(bigdata):
    numrows = bigdata.shape[0]

    # subtract means form the input data
    bigdata -= np.mean(bigdata, axis=1)[:, None]

    # normalize the data
    bigdata /= np.sqrt(np.sum(bigdata*bigdata, axis=1))[:, None]

    # reserve the resulting table onto HDD
    res = np.full((numrows, numrows), np.nan)

    for r in range(0, numrows, SPLITROWS):
        for c in range(0, numrows, SPLITROWS):
            r1 = r + SPLITROWS
            c1 = c + SPLITROWS
            chunk1 = bigdata[r:r1]
            chunk2 = bigdata[c:c1]
            print(chunk1.shape)
            print(chunk2.shape)
            x = np.dot(chunk1, chunk2.T)
            print(x.shape)
            print(x)
            res[r:r1, c:c1] = np.dot(chunk1, chunk2.T)
    return res


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_part(bigdata):
    numrows = bigdata.shape[0]

    # reserve the resulting table onto HDD
    res = np.full((numrows, numrows), np.nan)

    for r in range(0, numrows, SPLITROWS):
        for c in range(0, numrows, SPLITROWS):
            r1 = r + SPLITROWS
            c1 = c + SPLITROWS
            chunk1 = bigdata[r:r1]
            chunk2 = bigdata[c:c1]
            c_all = np.vstack((chunk1, chunk2))
            # subtract means form the input data
            c_all -= np.mean(c_all, axis=1)[:, None]

            # normalize the data
            c_all /= np.sqrt(np.sum(c_all*c_all, axis=1))[:, None]
            res[r:r1, c:c1] = np.dot(c_all[0:SPLITROWS], c_all[SPLITROWS:].T)
    return res


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_nan(bigdata):
    numrows = bigdata.shape[0]

    # reserve the resulting table onto HDD
    res = np.full((numrows, numrows), np.nan)

    for r in range(0, numrows, SPLITROWS):
        for c in range(0, numrows, SPLITROWS):
            r1 = r + SPLITROWS
            c1 = c + SPLITROWS
            chunk1 = bigdata[r:r1]
            chunk2 = bigdata[c:c1]
            c_all = ma.vstack((chunk1, chunk2))
            # subtract means form the input data
            # print(ma.mean(c_all, axis=1))
            # print("")
            # print(ma.mean(c_all, axis=1))
            # print(c_all)
            c_all -= ma.mean(c_all, axis=1)[:, None]

            # normalize the data
            c_all /= ma.sqrt(ma.sum(c_all*c_all, axis=1))[:, None]
            res[r:r1, c:c1] = ma.dot(c_all[0:SPLITROWS], c_all[SPLITROWS:].T)
    return res


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _covhelper(x, y=None, rowvar=True, allow_masked=True):
    """
    Private function for the computation of covariance and correlation
    coefficients.
    """
    x = ma.array(x, ndmin=2, copy=True, dtype=float)
    xmask = ma.getmaskarray(x)
    # Quick exit if we can't process masked data
    if not allow_masked and xmask.any():
        raise ValueError("Cannot process masked data.")
    #
    if x.shape[0] == 1:
        rowvar = True
    # Make sure that rowvar is either 0 or 1
    rowvar = int(bool(rowvar))
    axis = 1 - rowvar
    if rowvar:
        tup = (slice(None), None)
    else:
        tup = (None, slice(None))
    #
    if y is None:
        xnotmask = np.logical_not(xmask).astype(int)
    else:
        y = ma.array(y, copy=False, ndmin=2, dtype=float)
        ymask = ma.getmaskarray(y)
        if not allow_masked and ymask.any():
            raise ValueError("Cannot process masked data.")
        if xmask.any() or ymask.any():
            if y.shape == x.shape:
                # Define some common mask
                common_mask = np.logical_or(xmask, ymask)
                if common_mask is not ma.nomask:
                    xmask = x._mask = y._mask = ymask = common_mask
                    x._sharedmask = False
                    y._sharedmask = False
        x = ma.concatenate((x, y), axis)
        xnotmask = np.logical_not(np.concatenate((xmask, ymask), axis)).astype(int)
    x -= x.mean(axis=rowvar)[tup]
    return (x, xnotmask, rowvar)


def corrcoef(x, y=None, rowvar=True, bias=np._NoValue, allow_masked=True,
             ddof=np._NoValue):
    """
    Return Pearson product-moment correlation coefficients.
    Except for the handling of missing data this function does the same as
    `numpy.corrcoef`. For more details and examples, see `numpy.corrcoef`.
    Parameters
    ----------
    x : array_like
        A 1-D or 2-D array containing multiple variables and observations.
        Each row of `x` represents a variable, and each column a single
        observation of all those variables. Also see `rowvar` below.
    y : array_like, optional
        An additional set of variables and observations. `y` has the same
        shape as `x`.
    rowvar : bool, optional
        If `rowvar` is True (default), then each row represents a
        variable, with observations in the columns. Otherwise, the relationship
        is transposed: each column represents a variable, while the rows
        contain observations.
    bias : _NoValue, optional
        Has no effect, do not use.
        .. deprecated:: 1.10.0
    allow_masked : bool, optional
        If True, masked values are propagated pair-wise: if a value is masked
        in `x`, the corresponding value is masked in `y`.
        If False, raises an exception.  Because `bias` is deprecated, this
        argument needs to be treated as keyword only to avoid a warning.
    ddof : _NoValue, optional
        Has no effect, do not use.
        .. deprecated:: 1.10.0
    See Also
    --------
    numpy.corrcoef : Equivalent function in top-level NumPy module.
    cov : Estimate the covariance matrix.
    Notes
    -----
    This function accepts but discards arguments `bias` and `ddof`.  This is
    for backwards compatibility with previous versions of this function.  These
    arguments had no effect on the return values of the function and can be
    safely ignored in this and previous versions of numpy.
    """
    msg = 'bias and ddof have no effect and are deprecated'
    if bias is not np._NoValue or ddof is not np._NoValue:
        # 2015-03-15, 1.10
        warnings.warn(msg, DeprecationWarning, stacklevel=2)
    # Get the data
    (x, xnotmask, rowvar) = _covhelper(x, y, rowvar, allow_masked)
    # Compute the covariance matrix
    if not rowvar:
        fact = np.dot(xnotmask.T, xnotmask) * 1.
        c = (ma.dot(x.T, x.conj(), strict=False) / fact).squeeze()
    else:
        fact = np.dot(xnotmask, xnotmask.T) * 1.
        c = (ma.dot(x, x.T.conj(), strict=False) / fact).squeeze()
    cols = ['A', 'B', 'C', 'D']
    rows = cols
    print(pd.DataFrame(c, columns=cols, index=rows))
    # Check whether we have a scalar
    try:
        diag = ma.diagonal(c)
    except ValueError:
        return 1
    #
    if xnotmask.all():
        _denom = ma.sqrt(ma.multiply.outer(diag, diag))
    else:
        _denom = ma.diagflat(diag)
        _denom._sharedmask = False  # We know return is always a copy
        n = x.shape[1 - rowvar]
        if rowvar:
            for i in range(n - 1):
                for j in range(i + 1, n):
                    _x = ma.mask_cols(ma.vstack((x[i], x[j]))).var(axis=1)
                    print(f"***** {i},{j} *****")
                    print(_x)
                    _denom[i, j] = _denom[j, i] = ma.sqrt(ma.multiply.reduce(_x))
        else:
            for i in range(n - 1):
                for j in range(i + 1, n):
                    _x = ma.mask_cols(
                            ma.vstack((x[:, i], x[:, j]))).var(axis=1)
                    _denom[i, j] = _denom[j, i] = ma.sqrt(ma.multiply.reduce(_x))
    print(pd.DataFrame(_denom, columns=cols, index=rows))
    return c / _denom

# def nb_cov(a, b):
#     return [ma.cov(x)[0,1] for x in ma.stack((a, b), axis=1)]

# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def row_cc(a, b):
#     covar = row_cov(a, b)
#     mask = np.logical_not((a.mask | b.mask))
#     avar = np.array([a[idx][mask[idx]].var() for idx in range(mask.shape[0])])
#     bvar = np.array([b[idx][mask[idx]].var() for idx in range(mask.shape[0])])
#     stdev = ma.sqrt(avar * bvar)
#     res = covar/stdev
#     return res


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def row_cc(a, b):
#     mask = np.logical_not((a.mask | b.mask))
#     am = a - a.mean(axis=1)[:, None]
#     bm = b - b.mean(axis=1)[:, None]
#     covar = np.sum(am * bm, axis=1)/np.sum(mask, axis=1)

#     var_prod = []
#     for idx in range(mask.shape[0]):
#         if not np.all(a[idx].mask) and not np.all(b[idx].mask):
#             avar = a[idx][mask[idx]].var()
#             bvar = b[idx][mask[idx]].var()
#             var_prod.append(avar * bvar)
#         else:
#             var_prod.append(np.nan)
#     return covar/np.sqrt(var_prod), np.sum(mask, axis=1)


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @jit(nopython=True)
# def row_cc(a, b):
#     mask = np.logical_not(np.isnan(a) | np.isnan(b))
#     am = a - np.mean(a, 1)[:, None]
#     bm = b - np.mean(b, 1)[:, None]
#     denom = np.sum(mask, axis=1)
#     covar = np.sum(am * bm, axis=1)/denom
#     avar = np.array([a[idx][mask[idx]].var() for idx in range(mask.shape[0])])
#     bvar = np.array([b[idx][mask[idx]].var() for idx in range(mask.shape[0])])
#     stdev = ma.sqrt(avar * bvar)
#     res = covar/stdev
#     return res


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def pair_covar(x):
#     rowvar = True
#     allow_masked = True
#     # Get the data
#     (x, xnotmask, rowvar) = _covhelper(x, None, rowvar, allow_masked)
#     # Compute the covariance matrix
#     fact = np.dot(xnotmask, xnotmask.T) * 1.
#     c_dot = ma.dot(x, x.T.conj(), strict=False)
#     c_fact = c_dot / fact
#     c = c_fact.squeeze()
#     return (ma.dot(x, x.T.conj(), strict=False) / fact).squeeze()

# print("full")
# res = test_full(bigdata.copy())
# print(res)
#
# print("part")
# res = test_part(bigdata.copy())
# print(res)
#
# bignan = add_nan(bigdata.copy())
# print("full nan")
# res = test_full(bignan.copy())
# print(res)
#
# print("part nan")
# res = test_part(bignan.copy())
# print(res)
#
# print("masked cc")
# masked_cc = ma.corrcoef(bignan.copy())
# print(masked_cc)
#
#
# print("masked part")
# masked_dot = test_nan(bignan.copy())
# print(masked_dot)

a = ma.array([ma.masked_invalid([1, 2, 5, 6, np.nan, np.nan, 10, 11])])
b = ma.array([ma.masked_invalid([np.nan, 1, 7, 9, 11, np.nan, 4, 6])])
c = ma.array([ma.masked_invalid([4, 1, 7, 9, 11, 1, 4, np.nan])])
d = ma.array([ma.masked_invalid([np.nan, 1, 7, 9, 11, 2, 9, 4])])
e = ma.array([ma.masked_invalid([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])])
f = ma.array([ma.masked_invalid([np.nan, 1, 7, 9, 11, 2, 9, 4])])
g = ma.array([ma.masked_invalid([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])])
h = ma.array([ma.masked_invalid([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])])
# # # print(nb_cov(a, b))
# # print(ma.cov(x))
# print("A")
# print(a)
# print("B")
# print(b)
# print("X")
# x = ma.array(
#     [a[0], b[0], c[0], d[0], e[0], f[0], g[0], h[0]]
# )
# print(x)

# # print("A mask")
# # print(a.mask)
# # print("B mask")
# # print(b.mask)
# # print("X mask")
# # print(x.mask)

# # print("MY covar")
# # cvar = row_cov(a, b)
# # print(cvar)

# # print("MA covar(a,b)")
# # print(ma.cov(a, b, bias=True))
# # print("MA covar(x)")
# # print(ma.cov(x, bias=True))

# # print("MA.CC covar(x)")
# # print(pair_covar(x))


# # print("MA.CC(a,b)")
# # print(ma.corrcoef(a[0], b[0]))

# print("MA.CC(x)")
# print(ma.corrcoef(x))

# print("MY.CC(a,b)")
# cc, nobs = row_cc(a, b)
# print("RESULT N1")
# print(cc)
# print(nobs)
# cc, nobs = row_cc(
#     ma.array([a[0], c[0], e[0], g[0]]),
#     ma.array([b[0], d[0], f[0], h[0]])
# )
# print("RESULT N2")
# print(cc)
# print(nobs)

# # A = np.sqrt(np.arange(12).reshape(3, 4))   # some 3 by 4 array
# # print(A)
# # b = np.array([[2], [4], [5]])              # some 3 by 1 vector
# # print(b)
# # cov = np.dot(b.T - b.mean(), A - A.mean(axis=0)) / (b.shape[0]-1)
# # print(cov)


# # x, xnotmask, rowvar = _covhelper(x)
# # cc = corrcoef(x)
# #print(cc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def row_cov(a, b):
    mask = np.logical_not((a.mask | b.mask))
    am = a - a.mean(axis=1)[:, None]
    bm = b - b.mean(axis=1)[:, None]
    denom = np.sum(mask, axis=1)
    return np.sum(am * bm, axis=1)/denom


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def block_cov(a, b):
    # mask = np.logical_not((a.mask | b.mask))
    aobs = np.logical_not(a.mask).astype(int)
    bobs = np.logical_not(b.mask).astype(int)
    am = a - a.mean(axis=1)[:, None]
    bm = b - b.mean(axis=1)[:, None]
    denom = np.dot(aobs, bobs.T) * 1.
    return ma.dot(am, bm.T)/denom


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ma_cc_cov(x, y=None):
    rowvar = True
    allow_masked = True
    # Get the data
    (x, xnotmask, rowvar) = _covhelper(x, y, rowvar, allow_masked)
    # Compute the covariance matrix
    fact = np.dot(xnotmask, xnotmask.T) * 1.
    # c_dot = ma.dot(x, x.T.conj(), strict=False)
    # c_fact = c_dot / fact
    # c = c_fact.squeeze()
    cx = (ma.dot(x, x.T.conj(), strict=False) / fact).squeeze()
    return cx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def block_corrcoef(a, b):
    covar = block_cov(a, b)
    # print(covar)
    n = a.shape[0]
    _denom = np.full((n, n), np.nan)
    for i in range(n):
        for j in range(n):
            _x = ma.mask_cols(ma.vstack((a[i], b[j]))).var(axis=1)
            print(f"***** {i},{j} *****")
            print(_x)
            _denom[i, j] = ma.sqrt(ma.multiply.reduce(_x))
    rows = ['A', 'B']
    cols = ['C', 'D']
    print(pd.DataFrame(_denom, columns=cols, index=rows))
    return covar/_denom


# # a = ma.array([a[0], c[0], e[0], g[0]])
# # b = ma.array([b[0], d[0], f[0], h[0]])

# # print("ROW_COVAR")
# # rcov = row_cov(a, b)
# # print(rcov)

# # print("BLOCK_COVAR")
# # bcov = block_cov(a, b)
# # print(bcov)

# # print("MA_COVAR")
# # mcov = ma.cov(a, y=b, bias=True)
# # print(mcov)


# # print("MA_CC_COVAR(X, Y)")
# # mcov = ma_cc_cov(a, y=b)
# # print(mcov)


x = ma.array(
    [a[0], b[0], c[0], d[0]]
)
a = ma.array([a[0], b[0]])
b = ma.array([c[0], d[0]])
x = ma.array(
    [a[0], b[0], c[0], d[0], e[0], f[0], g[0], h[0]]
)

print("MA_CC_COVAR(x)")
mcov = ma_cc_cov(x)
cols = ['A', 'B', 'C', 'D']
rows = cols
print(pd.DataFrame(mcov, columns=cols, index=rows))

print("BLOCK_COVAR")
rows = ['A', 'B']
cols = ['C', 'D']
bcov = block_cov(a, b)
print(pd.DataFrame(bcov, columns=cols, index=rows))


print("MA_CC(x)")
cols = ['A', 'B', 'C', 'D']
rows = cols
mcorr = ma.corrcoef(x)
print(pd.DataFrame(mcorr, columns=cols, index=rows))


print("BLOCK_CC(a, b)")
rows = ['A', 'B']
cols = ['C', 'D']
mcorr = block_corrcoef(a, b)
print(pd.DataFrame(mcorr, columns=cols, index=rows))


# from bio_misc.ipd import common
# print(common.get_process_params(20, 3, 5))

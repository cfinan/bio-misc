#!/bin/bash
# A task for run merge of BGEN liftover chunks. This will also create index
# files for bgenix and bgen-reader (simple and complex). This expects the job
# array file to have the following columns IN THIS ORDER:

# 1. rowidx - sequential row index number, required by every job array file
# 2. infile - This is a merge file. one file/line (in genomic order)
# 3. outfile - The output bgen file.

# This task will accept the following parameters
# 1. job array file (required)
# 2. tmpdir to use (required)
# 3. step size (required)
# 4. rowidx (optional), if not set then the SGETASKID is used

# The task requires cat-bgen-wrap.sh to be in the path

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Pull the columns from the current row, rowidx is pulled by task_init.sh
    infile="${PROCESS_LINE[1]}"
    outfile="${PROCESS_LINE[2]}"
    info_msg "infile=$infile"
    info_msg "outfile=$outfile"

    # The temp dir to place all the output files in
    temp_dir=$(mktemp -d -p "$WORKING_DIR")
    outbase=$(basename $outfile)
    outdir=$(dirname $outfile)
    temp_out="$temp_dir"/"$outbase"
    info_msg "tmpout_dir=$temp_dir"
    info_msg "outbase=$outbase"
    info_msg "outdir=$outdir"
    info_msg "temp_out=$temp_out"

    cat-bgen-wrap.sh "$infile" "$temp_out"
    mv "$temp_dir"/* "$outdir"
}

# This actually initialises the running of the job
. task_init.sh

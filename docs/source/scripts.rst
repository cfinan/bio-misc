======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with the bio-misc. Many of these are mentioned in their respective context throughout the documentation but they are listed here all in a single place.

.. toctree::
   :maxdepth: 2
   :caption: Python scripts:

   scripts/drug_lookups
   scripts/overlaps
   scripts/mapping
   scripts/annotation
   scripts/transforms
   scripts/format
   scripts/ipd
   scripts/pubmed
   scripts/liftover
   scripts/reactome
   scripts/umls

.. toctree::
   :maxdepth: 2
   :caption: Bash scripts:

   scripts/bash_scripts
   scripts/task_scripts

# Getting Started with bio-misc

__version__: `0.1.2a0`

The bio-misc package is essentially a collection of scripts to do various "biological" tasks, the sort of stuff that is useful post GWAS. It is not meant to be an API but I will try to write things to be generalise and with an API in mind. The scripts are of varying quality.

If it looks like anything is getting too big, it will be rolled into it's own package and I will attach a deprecation warning to them.

There is [online](https://cfinan.gitlab.io/bio-misc/) documentation for bio-misc.

## Installation instructions
At present, bio-misc is undergoing development and no packages exist yet on PyPy or in Conda. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/bio-misc.git
cd bio-misc
```

### Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./resources/conda/env`. A new conda environment called `bio_misc_py39` can be built using the command:

```
# From the root of the bio-misc repository
conda env create --file ./resources/conda/env/py39/conda_create.yml
```

To add to an existing environment use:
```
# From the root of the bio-misc repository
conda env update --file ./resources/conda/env/py39/conda_update.yml
```

There are also Conda environments for Python v3.7, v3.8 and and v3.10. Then to install paper_scrapper you can either do:
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the bio-misc repository. The difference with this is that you can just to a `git pull` to update paper_scrapper, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install paper_scrapper as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then to install paper_scrapper you can either do:
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the bio-misc repository. The difference with this is that you can just to a `git pull` to update paper_scrapper, or switch branches without re-installing:
```
python -m pip install -e .
```

## Installing the webdrivers
Some of the scripts use Selanium. This requires a [browser driver](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/) to be installed (webdriver). For Firefox this is referred to as [`geckodriver`](https://github.com/mozilla/geckodriver/releases). The driver's location will need to be in your PATH in your `~/.bashrc` or `~/.bash_profile` and should be matched to your browser version.

## Next steps...
After installation you will may to run the tests. If you have cloned the repository you can run the command below from the root of the repository. Please note, that there are currently no pytests. I hope to implement some when I can get headless mode running:

1. Run the tests using ``pytest ./tests``

## Command endpoints
bio_misc contains a lot of command line endpoints that are installed along with the package. Documentation for these can be found [here](https://cfinan.gitlab.io/bio-misc/scripts.html).

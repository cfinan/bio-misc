=============================
Defining overlapping features
=============================

This sub-package has some scripts to calculate some simple overlaps between features. This is certainly not any replacement for `bedtools <https://bedtools.readthedocs.io/en/latest/>`_, rather, they perform very defined tasks.

``bm-genes-within-flank``
-------------------------

.. argparse::
   :module: bio_misc.overlaps.genes_within
   :func: _init_cmd_args
   :prog: bm-genes-within-flank


Output columns
~~~~~~~~~~~~~~

In addition to the columns of the input file. These additional columns will be added to the output file. Note that there will be some pseudo-replication of input file data if the region has >1 gene overlapping the flanking region.

.. include:: ../data_dict/genes-within-flank.rst

``bm-trans-within-flank``
-------------------------

.. argparse::
   :module: bio_misc.overlaps.trans_within
   :func: _init_cmd_args
   :prog: bm-trans-within-flank


Output columns
~~~~~~~~~~~~~~

In addition to the columns of the input file. These additional columns will be added to the output file. Note that there will be some pseudo-replication of input file data if the region has >1 gene overlapping the flanking region.

.. include:: ../data_dict/trans-within-flank.rst

``bm-nearest-genes``
--------------------

.. argparse::
   :module: bio_misc.overlaps.nearest_genes
   :func: _init_cmd_args
   :prog: bm-nearest-genes

``bm-site-overlap``
-------------------

.. argparse::
   :module: bio_misc.overlaps.site_overlap
   :func: _init_cmd_args
   :prog: bm-site-overlap

``bm-trans-qtl-link``
---------------------

.. argparse::
   :module: bio_misc.overlaps.trans_qtl_link
   :func: _init_cmd_args
   :prog: bm-trans-qtl-link

========================
Mapping against the UMLS
========================

Scripts to map against the UMLS.

``bm-mesh-mapping``
-------------------

.. argparse::
   :module: bio_misc.umls.mesh_mapping
   :func: _init_cmd_args
   :prog: bm-mesh-mapping

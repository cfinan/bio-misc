===================
Mapping between IDs
===================

Scripts to map between different identifiers.

``bm-map-ensembl-gene``
-----------------------

.. argparse::
   :module: bio_misc.id_map.ensembl_gene
   :func: _init_cmd_args
   :prog: bm-map-ensembl-gene


``bm-map-uniprot``
------------------

.. argparse::
   :module: bio_misc.id_map.ensembl_uniprot
   :func: _init_cmd_args
   :prog: bm-map-uniprot


``bm-map-ensembl-gene-offline``
-------------------------------

.. argparse::
   :module: bio_misc.id_map.ensembl_human_uniprot_gene
   :func: _init_cmd_args
   :prog: bm-map-ensembl-gene-offline


``bm-map-uniprot-offline``
--------------------------

.. argparse::
   :module: bio_misc.id_map.ensembl_human_gene_uniprot
   :func: _init_cmd_args
   :prog: bm-map-uniprot-offline


``bm-map-seq-id``
-----------------

.. argparse::
   :module: bio_misc.id_map.somalogic_seq_ids
   :func: _init_cmd_args
   :prog: bm-map-seq-id

=============================
``./resources/tasks`` scripts
=============================

The pyaddons repository also contains some task scripts in the ``./resources/bin`` `directory <https://gitlab.com/cfinan/bio-misc/resources/task>`_. Unlike the Python scripts these are not installed into the PATH and the ``./resources/task`` must be added manually to your PATH in your ``~/.bashrc``.

Task scripts are designed to be used to run parallel tasks on an HPC using the hpc-tools `package <https://gitlab.com/cfinan/cluster>`_. Task scripts require the hpc-tools `task initialisation <https://gitlab.com/cfinan/cluster/resources/bin/task_init.sh>` script to be in your path.

All task scripts have the same interface:

.. code-block::

   my-task-script.sh <job_array_file> <tmpdir> <step_size> <rowidx>

Where:

1. job array file (required) - A tab delimited file with a header row that details the jobs, the first column must be named ``rowidx`` and contain a sequential counter, the other columns will be dependent on the task script.
2. tmpdir (required) - The location of the temp directory to use
3. step size (required) - The step size, i..e how many sequential rows (tasks) in the job array should the script process.
4. rowidx (optional) - The row number in the job array file to process. row 1 is the first row after the job array file header. If not set then the SGETASKID which will be supplied by GridEngine is used.

``liftover-bgen.task.sh``
~~~~~~~~~~~~~~~~~~~~~~~~~

A task script that runs the liftover of bgen files, this uses the ``liftover-bgen.sh`` :ref:`script <liftover_bgen>`. This accepts a job array file with the following columns:

1.  ``rowidx`` - A sequential row counter that starts at 1
2.  ``region_idx`` - A sequential row counter for the bgen chunk that starts at 1, will probably be the same as ``rowidx``.
3.  ``chr_name`` - The chromosome name of the bgen chunk for the task.
4.  ``start_pos`` - The start position of the bgen chunk for the task.
5.  ``end_pos`` - The start position of the bgen chunk for the task.
6.  ``nsites`` - The number of variant sites in the bgen chunk for the task.
7.  ``infile`` - The input bgen file where the chunk will be extracted from.
8.  ``outfile`` - The output chunk file name.
9.  ``source_assembly`` - The source assembly of the input bgen file.
10. ``target_assembly`` - The target assembly for the output chunk file.
11. ``species`` - The species for both the input and output chunk file.
12. ``refgen_name`` - The name of the reference genome file to use in the genomic config file.
13. ``mapper_name`` - The name of the variant mapper file to use in the genomic config file.
14. ``samples`` - A file with a subset of sample IDs to extract from the bgen file, i.e if you want to subset and liftover at the same time. If not used a ``-`` should be placed here.

``cat-bgen.task.sh``
~~~~~~~~~~~~~~~~~~~~

A task script that runs bgen concatenations jobs via the ``cat-bgen-wrap.sh`` :ref:`bash script <cat_bgen_wrap>`. This accepts a job array file with the following columns:

1. ``rowidx`` - A sequential row counter that starts at 1
2. ``infile`` - The path to the input list file containing one full path to each bgen file to be concatenated per row.
3. ``outfile`` - The full path to the output bgen file (and index files)

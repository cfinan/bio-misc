=====================
Individual level data
=====================

These are programs that handle individual level data in some way. Man will use `genomic-config <https://gitlab.com/cfinan/genomic-config>`_.

``bm-ld-clumper``
-----------------

.. argparse::
   :module: bio_misc.ipd.ld_clump
   :func: _init_cmd_args
   :prog: bm-ld-clumper

Please note, I was having some issues with the LD clumping tests. I test two cohorts, one seems to fail and the other is ok. I am looking into it but I think it is down to rounding and nothing too serious. However, if there are any issues please let me know and please scrutinise the output from this script.

The output file from ``bm-ld-clumper`` will contain the LD independent variants as defined by the input parameters. Variants that are not present in the LD reference population will be output along side the index variants. The structure of the output file will resemble the input file structure except an ``is_unknown`` column will be added as the final column. This will be ``0`` if the variant is known or ``1`` if unknown (i.e. not present in the LD reference panel.

``bm-corr-matrix``
------------------

.. argparse::
   :module: bio_misc.ipd.corr_matrix
   :func: _init_cmd_args
   :prog: bm-corr-matrix


``bm-ipd-allele-counts``
------------------------

.. argparse::
   :module: bio_misc.ipd.extract_allele_counts
   :func: _init_cmd_args
   :prog: bm-ipd-allele-counts


``bm-bgen-reader-index``
------------------------

.. argparse::
   :module: bio_misc.ipd.index_bgen
   :func: _init_cmd_args
   :prog: bm-bgen-reader-index

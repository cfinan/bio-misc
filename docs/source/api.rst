================
``bio-misc`` API
================

bio-misc is not really designed as an API. Nevertheless, everything is documented and conforms to the standard pep-8 way of doing things. There are functions within the vast majority of the scripts that could be used as API entry points. So I have placed API sections in here, also linking out to the relevant script documentation (where it has been written).

.. toctree::
   :maxdepth: 4

   api/annotation_sub_package
   api/drug_lookups_sub_package
   api/example_data_sub_package
   api/format_sub_package
   api/id_map_sub_package
   api/liftover_sub_package
   api/overlaps_sub_package
   api/reactome_sub_package
   api/transforms_sub_package
   api/umls_sub_package
   api/ipd_sub_package
   api/pubmed_sub_package

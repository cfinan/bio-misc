===============================
``bio_misc.id_map`` sub-package
===============================

``bio_misc.id_map.ensembl_gene``
--------------------------------

.. currentmodule:: bio_misc.id_map.ensembl_gene
.. autofunction:: get_gene_ids
.. autofunction:: yield_gene_ids

``bio_misc.id_map.ensembl_human_gene_uniprot``
----------------------------------------------

.. currentmodule:: bio_misc.id_map.ensembl_human_gene_uniprot
.. autofunction:: get_uniprot_ids
.. autofunction:: yield_uniprot_ids

``bio_misc.id_map.ensembl_human_uniprot_gene``
----------------------------------------------

.. currentmodule:: bio_misc.id_map.ensembl_human_uniprot_gene
.. autofunction:: get_gene_ids
.. autofunction:: yield_gene_ids

``bio_misc.id_map.ensembl_uniprot``
-----------------------------------

.. currentmodule:: bio_misc.id_map.ensembl_uniprot
.. autofunction:: get_uniprot_ids
.. autofunction:: yield_uniprot_ids

``bio_misc.id_map.somalogic_seq_ids``
-------------------------------------

.. currentmodule:: bio_misc.id_map.somalogic_seq_ids
.. autofunction:: get_uniprot_ids
.. autofunction:: yield_uniprot_ids

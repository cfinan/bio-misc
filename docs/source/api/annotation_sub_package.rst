===================================
``bio_misc.annotation`` sub-package
===================================

``bio_misc.annotation.boolean_matrix``
--------------------------------------

.. currentmodule:: bio_misc.annotation.boolean_matrix
.. autofunction:: get_annotation_matrices

``bio_misc.annotation.genecards``
---------------------------------

.. currentmodule:: bio_misc.annotation.genecards
.. autofunction:: initialise_chrome
.. autofunction:: get_gene_cards
.. autofunction:: yield_gene_cards

``bio_misc.annotation.vcf_id_mapper``
-------------------------------------

.. currentmodule:: bio_misc.annotation.vcf_id_mapper
.. autofunction:: run_var_id_mapper

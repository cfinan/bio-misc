=====================================
``bio_misc.drug_lookups`` sub-package
=====================================

``bio_misc.drug_lookups.target_effects``
----------------------------------------

.. currentmodule:: bio_misc.drug_lookups.target_effects
.. autofunction:: get_drug_target_effects
.. autofunction:: yield_drug_target_effects

``bio_misc.drug_lookups.target_summary``
----------------------------------------

.. currentmodule:: bio_misc.drug_lookups.target_summary
.. autofunction:: get_drug_target_summary
.. autofunction:: yield_drug_target_summary

``bio_misc.drug_lookups.search_effects``
----------------------------------------

.. currentmodule:: bio_misc.drug_lookups.search_effects
.. autofunction:: search_drug_effects

``bio_misc.drug_lookups.therapeutic_direction``
-----------------------------------------------

.. currentmodule:: bio_misc.drug_lookups.therapeutic_direction
.. autofunction:: get_therapeutic_direction
.. autofunction:: yield_therapeutic_direction

=============================
``bio_misc.umls`` sub-package
=============================

``bio_misc.umls.mesh_mapping``
------------------------------

.. currentmodule:: bio_misc.umls.mesh_mapping
.. autofunction:: get_mesh_mapping
.. autofunction:: yield_mesh_mapping

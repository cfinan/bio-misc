=====================================
``bio_misc.example_data`` sub-package
=====================================

``bio_misc.example_data.examples``
----------------------------------

.. automodule:: bio_misc.example_data.examples
   :members:
   :undoc-members:
   :show-inheritance:

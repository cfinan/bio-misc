============================
``bio_misc.ipd`` sub-package
============================

``bio_misc.ipd.corr_matrix``
----------------------------

.. currentmodule:: bio_misc.ipd.corr_matrix
.. autofunction:: get_corr_matrix

``bio_misc.ipd.corr_matrix_single``
-----------------------------------

.. automodule:: bio_misc.ipd.corr_matrix_single
   :members:
   :undoc-members:
   :show-inheritance:

``bio_misc.ipd.corr_matrix_zarr``
---------------------------------

.. automodule:: bio_misc.ipd.corr_matrix_zarr
   :members:
   :undoc-members:
   :show-inheritance:

``bio_misc.ipd.dosage_zarr``
----------------------------

.. currentmodule:: bio_misc.ipd.dosage_zarr
.. autofunction:: get_dosage_array

``bio_misc.ipd.extract_allele_counts``
--------------------------------------

.. currentmodule:: bio_misc.ipd.extract_allele_counts
.. autofunction:: extract_allele_counts
.. autofunction:: yield_allele_counts

``bio_misc.ipd.index_bgen``
---------------------------

.. currentmodule:: bio_misc.ipd.index_bgen
.. autofunction:: run_create_index

``bio_misc.ipd.ld_clump``
-------------------------

.. currentmodule:: bio_misc.ipd.ld_clump
.. autofunction:: do_ld_clump

``bio_misc.ipd.readers``
------------------------

.. automodule:: bio_misc.ipd.readers
   :members:
   :undoc-members:
   :show-inheritance:

=================================
``bio_misc.liftover`` sub-package
=================================

``bio_misc.liftover.quick_lift``
--------------------------------

.. currentmodule:: bio_misc.liftover.quick_lift
.. autofunction:: get_liftover
.. autofunction:: yield_liftover
